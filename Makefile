# $Id: Makefile,v 1.1.1.1 2010/08/18 16:44:15 brian Exp $
include ${FSLCONFDIR}/default.mk

PROJNAME = graph_tools


INC_BOOST=/usr/local/include
LIB_BOOST=/usr/local/lib/boost

CXXFLAGS+= -std=c++11

USRINCFLAGS = -I/usr/local/extras/include -I ../ -I${INC_NEWMAT} -I${INC_ZLIB} -I${INC_PROB} -I${INC_BOOST}
USRLDFLAGS = -L ../fslsurface -L${LIB_BOOST}  -L${LIB_NEWMAT} -L${LIB_PROB}  -L${LIB_ZLIB} -L./

SULIBS=-lmisc_utils
LIBS+=-lfslsurface -lfirst_lib -lfslvtkio -lgiftiio -lexpat -lmeshclass -lnewimage -lmiscmaths -lprob -lfslio -lniftiio -lznz -lutils -lnewmat   -lutils -lz
LIBSNOSURF=-lfirst_lib -lfslvtkio -lgiftiio -lexpat -lmeshclass -lnewimage -lmiscmaths -lprob -lfslio -lniftiio -lznz -lutils -lnewmat   -lutils -lz


XFILES=etkin_graph correlation_norm etkin_matrix_mask etkin_matrix_setdiag etkin_matrix_normalize etkin_graph_surfaces etkin_graph_conn_2_circos etkin_graph_cohend etkin_graph_roi_dist etkin_roi_overlap etkin_matrix_mirror

CXX+=-std=c++11


all:  ${XFILES}

network_metrics: network_metrics.o
	${AR} -r libnetwork_metrics.a network_metrics.o

etkin_graph: etkin_graph.o network_metrics.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ etkin_graph.o network_metrics.o  ${LIBS}


etkin_graph_cohend: etkin_graph_cohend.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ etkin_graph_cohend.o ${LIBS}

etkin_graph_conn_2_circos: etkin_graph_conn_2_circos.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ etkin_graph__onn_2_circos.o   ${LIBS}


etkin_graph_roi_dist: etkin_graph_roi_dist.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ etkin_graph_roi_dist.o   ${LIBS} -lboost_filesystem -lboost_system

etkin_roi_overlap: etkin_roi_overlap.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ etkin_roi_overlap.o ${LIBS} -lboost_filesystem -lboost_system

etkin_graph_surfaces: etkin_graph_surfaces.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ etkin_graph_surfaces.o ${LIBS}

etkin_matrix_setdiag: etkin_matrix_setdiag.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ etkin_matrix_setdiag.o ${LIBS}

etkin_matrix_mirror: etkin_matrix_mirror.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ etkin_matrix_mirror.o ${LIBSNOSURF}

etkin_matrix_normalize: etkin_matrix_normalize.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ etkin_matrix_normalize.o ${LIBS}

circos_conf_create: circos_conf_create.o
	${CXX}  ${CXXFLAGS} ${LDFLAGS} -o $@ circos_conf_create.o ${LIBS} -lboost_filesystem -lboost_system

etkin_matrix_mask: etkin_matrix_mask.o network_metrics.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ etkin_matrix_mask.o ${LIBS}

correlation_norm: correlation_norm.o
	${CXX} ${CXXFLAGS} ${LDFLAGS} -o $@ correlation_norm.o ${LIBS}
