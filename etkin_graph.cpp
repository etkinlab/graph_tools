//
//  main.cpp
//  interpret_motion_parameters
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include <string>
#include <set>
//#include <unordered_map>
//#include <unordered_set>

//FSL INCLUDE
#include "newimage/newimageall.h"
#include "utils/options.h"

//BOOST INCLUDES (MOSTLY BGL)
//#include <boost/graph/properties.hpp>
//#include <boost/graph/graphviz.hpp>

//#include <boost/config.hpp>
//#include  <boost/filesystem.hpp>
#include "etkin_graph_typedefs.h"

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
//#include <boost/property_map/property_map.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
//#include <boost/graph/betweenness_centrality.hpp>


using namespace NEWIMAGE;
using namespace Utilities;

using namespace std;
using namespace boost;
#include "etkin_graphio.hpp"
#include "network_metrics.h"
using namespace etkin_graph;


string title="etkin_graph Stanford University (Brian Patenaude)";
string examples="etkin_graph -i <connection_matrix_graph.nii.gz> -o <output_base>";




Option<bool> verbose(string("-v,--verbose"), false,
		string("switch on diagnostic messages"),
		false, no_argument);
Option<bool> help(string("-h,--help"), false,
		string("display this message"),
		false, no_argument);
Option<bool> doKnockOut(string("--knockOut,--knockOut"), false,
		string("Calculate network metrics with each node being knocked out."),
		false, no_argument);
Option<bool> doBinarize_in(string("--Unweighted,--Unweighted"), false,
		string("Calculate Unweighted graph metrics. Binaries edge weights."),
		false, no_argument);
Option<bool> directional(string("--directional,--directional"), false,
		string("Calculate directional-weighted graph metrics. "),
		false, no_argument);



//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
		string("Filename of input image correlation matrix"),
		true, requires_argument);
Option<string> maskname(string("-m,--mask"), string(""),
		string("Filename of input mask - used to specify networks"),
		false, requires_argument);
Option<string> labelname(string("-l,--labels"), string(""),				  string("Filename of vertex labels"),				  false, requires_argument);
Option<string> clustername(string("-c,--clusters"), string(""),				  string("Filename of cluster integer labels"),				  false, requires_argument);
Option<string> clust_name_map(string("-n,--cl_names"), string(""),				  string("Filename of cluster names"),				  false, requires_argument);
Option<string> subjectname(string("-s,--subjects"), string(""),				  string("Filename of subjects names"),				  false, requires_argument);
Option<string> groupsname(string("-j,--groups"), string(""),				  string("Filename of subject's group"),				  false, requires_argument);
Option<int> swap_index_in(string("-u,--swapindex"), -1,
		string("Index of ROI to swap out"),
		false, requires_argument);
Option<int> permutations(string("-p,--permutations"), 0,
		string("Number of Permutations"),
		false, requires_argument);

Option<float> thresh(string("-t,--thresh"), 0,
		string("Thresh for connection weights (inverse of correlation)"),
		false, requires_argument);
Option<int> gmeth(string("-g,--graphMethod"), -1,
		  string("Method for graph construction (ONLY AVAILABLE FOR UNDIRECTED GRAPHS).") \
		+ string( " \n \t\t 0: MST + any edge greater less than min wegiht.")\
		+ string("\n \t\t 1: Absolute Threshold.") \
		+ string("\n \t\t 2: MST_W_ABS_THRESH") \
		+ string("\n \t\t 3: MST") \
		+ string("\n \t\t 4: MST connections to meet  Percentile requirement")\
		+ string("\n \t\t 5: Percentile") \
		+ string("\n \t\t 6: MST Union Percentile (Still has bug)") \
		+ string("\n \t\t 7: Complete Graph"), \
		false, requires_argument);
Option<string> outname(string("-o,--output"), string(""),
		string("Output name"),
		true, requires_argument);
int nonoptarg;

enum g_thresh_method  {  MST_MIN_W, ABS_THRESH, MST_W_ABS_THRESH, MST, MST_W_PERC,  PERC , MST_U_PERC, COMPLETE };


void usage(){

	cout<<"\n Usage : "<<endl;
	cout<<"etkin_graph_theory <correlations.nii.gz> \n"<<endl;

}


//some useful templated functions
template< class T >
float sum( const vector<T> & vec )
{
	float sum=0;
	for ( typename vector<T>::const_iterator i = vec.begin(); i != vec.end(); ++i)
		sum+=*i;
	return sum;
}



string int2str( const int & val)
{
	stringstream ss;
	ss<<val;
	string sval;
	ss >> sval;
	return sval;

}
int string2int( const string & val)
{
	stringstream ss;
	ss<<val;
	int ival;
	ss >> ival;
	return ival;

}
//-------------graph functions


template<class T>
void print_graphEdges(  T & g )
{
	cout<<"Graph Edges"<<endl;
	typename property_map < T, edge_weight_t >::type weight = get(edge_weight, g);

	typename graph_traits < T >::edge_iterator ei, eend;
	for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
		cout<<"edge "<<source(*ei, g)<<"<-->"<<target(*ei, g)<<" ---> "<<weight[*ei]<<endl;
	}

}


//removes all edge from either directed or undirected graph
template<class T>
void clearEdges( T & g ){
	typename graph_traits < T >::edge_iterator vi, vend, next;

	//loop through edges and remove each one
	boost::tie(vi, vend) = edges(g);
	for (next= vi; vi != vend; vi=next) {
		++next;
		remove_edge(*vi,g);
	}

}

//set all the edges for directed and undirected graph
//preserves designed weights and names from the graph g
//theres an assumption that edges already have a weight and/or name in g
template<class T>
void setEdges( vector < Edge > & edges  , T & g ){
	//creates a new graph object than swap in data
	T gnew;
	typename property_map<T, edge_weight_t>::type weightmap = get(edge_weight, g);
	typename property_map<T, edge_weight_t>::type weightmap_new = get(edge_weight, gnew);

	//add each edge to empty graph
	//also create new weights based on existing
	for (vector< Edge > ::iterator i_e = edges.begin(); i_e != edges.end(); ++i_e)
	{
		Edge e; bool inserted;
		tie(e, inserted) = add_edge(source(*i_e,g),target(*i_e,g),gnew);
		weightmap_new[e]=weightmap[*i_e];
	}

	//as with the edge weights lets preserve vertex names
	typename property_map<T, vertex_name_t>::type vnamemap = get(vertex_name, g);
	typename property_map<T, vertex_name_t>::type vnamemap_new = get(vertex_name, gnew);

	typename  graph_traits<T>::vertex_iterator vi, vi_end;
	int index = 0 ;
	for (boost::tie(vi, vi_end) = vertices(gnew); vi != vi_end; ++vi,++index)
	{
		vnamemap_new[*vi] = vnamemap[*vi];
	}

	g=gnew;

}
//---A structure that 	associate an index and a x,y coordinate
struct ind_coord{
	ind_coord(int index_in, int xin,int yin){
		x=xin;
		y = yin ;
		index = index_in;
	}
	ind_coord(){
		x=y=index=0;
	}
	int x,y,index;
};


//a compare function implemented for sorting or weight,ind_coord pair
bool compare_weight (const std::pair<float, ind_coord> & first, const std::pair<float, ind_coord>& second)
{
	return (first.first < second.first);
}




//const float W_SCALE_FACTOR=100000;
//used to not know how to specify edge_weight as float


//this function searches through a mask to find all unique labels
set<int> getUniqueLabels(const volume<int> & mask)
		{
	set<int> labels;

	const int* m_ptr = mask.fbegin();
	for ( unsigned long int i =0 ; i < mask.nvoxels(); ++i)
	{
		if (*m_ptr != 0 )
			labels.insert(*m_ptr);
	}

	return labels;
		}

void calculateGraphMetrics( const DirectedGraph & g , network_metrics & nmets , const  std::vector<int> & clusters, const std::vector<int> excl_ind)
{
	getNumberOfNodes(g,  nmets  );
	getNumberOfEdges(g ,  nmets );
	getCostBin(g,nmets);
	degree_und(g,  nmets);
	degree_in(g,  nmets);
	degree_out(g,  nmets);

	degree_intra_inter(g,clusters,nmets);
	//    avgconn_intra_inter(g,clusters,nmets);
	//
	//    powerLawGamma(g,nmets,5);
	//
	//    systemSegregation( g,  clusters,  nmets );
	//    modularity(g,  clusters, nmets  );
	//    vector<float> eii,ai;
	//    modularity(g,  clusters, nmets  );
	//
	//
	//    within_module_degree_z( g, clusters,  nmets) ;
	//    closeness_centrality( g ,  excl_ind , nmets );//for connected graphs
	efficiency_wd( g , excl_ind, nmets );
	characteristicPathLength_wd( g, excl_ind,  nmets );
	//
	//    participation_coefficient( g, clusters, nmets );
	//    //    intra_inter_connectivity( g, clusters, nmets );
	//
	//    assortativity( g, nmets );
	//    cout<<"done metrics"<<endl;
}
//calculates the various graph metrics and stores in sturuct network metrics
//Graph Can be directed or Undirected Graph

void calculateGraphMetrics( const UndirectedGraph & g , network_metrics & nmets , const  std::vector<int> & clusters, const std::vector<int> excl_ind)
{
	getNumberOfNodes(g,  nmets  );
	getNumberOfEdges(g ,  nmets );
	getCostBin(g,nmets);
	degree_und(g,  nmets);
	degree_in(g,  nmets);
	degree_out(g,  nmets);
	degree_intra_inter(g,clusters,nmets);
	avgconn_intra_inter(g,clusters,nmets);

	powerLawGamma(g,nmets,5);

	systemSegregation( g,  clusters,  nmets );
	modularity(g,  clusters, nmets  );
	vector<float> eii,ai;
	modularity(g,  clusters, nmets  );


	within_module_degree_z( g, clusters,  nmets) ;
	closeness_centrality( g ,  excl_ind , nmets );//for connected graphs
	efficiency_wd( g , excl_ind, nmets );
	characteristicPathLength_wd( g, excl_ind,  nmets );

	participation_coefficient( g, clusters, nmets );
	//    intra_inter_connectivity( g, clusters, nmets );

	assortativity( g, nmets );

}


void calculateGraphMetrics( const UndirectedGraph & g , network_metrics & nmets , const  std::vector<int> & clusters)
{
	getNumberOfNodes(g,  nmets  );
	getNumberOfEdges(g ,  nmets );
	getCostBin(g,nmets);
	degree_und(g,  nmets);
	degree_intra_inter(g,clusters,nmets);
	avgconn_intra_inter(g,clusters,nmets);
	systemSegregation( g,  clusters,  nmets );
	modularity(g,  clusters, nmets  );
	within_module_degree_z( g, clusters,  nmets) ;
	std::vector<int> excl_ind;
	characteristicPathLength_wd( g ,excl_ind ,  nmets );
	closeness_centrality( g ,  excl_ind , nmets );//for connected graphs

	efficiency_wd( g , excl_ind, nmets );

	participation_coefficient( g, clusters, nmets );
	assortativity( g, nmets );


}


//swaps the in connectivity data for ROI swap_index
//used for permuting data
void swapin_roi_connectivity( volume<float> & imswap, const volume<float> & imsrc, int swap_index)
{
	int N = imswap.xsize();//matrix needs to be square
	for (int i =0 ; i < N ; ++i)
	{
		imswap.value(swap_index,i,0) = imsrc.value(swap_index,i,0);
		imswap.value(i,swap_index,0) = imsrc.value(i,swap_index,0);

	}
}

//zero's out the connectivity for node
//used for knock-out analysis
void zero_node(volume<float> & im_conn, const int & node)
{
	int Nnodes=im_conn.xsize();//square
	for (int i = 0 ; i < Nnodes; ++i) //+ exlude self connection
	{
		im_conn.value(node, i,0) = im_conn.value(i, node,0) = 0;

	}

}

//permute a node for connectivity matrices
//must be a square matrix
void permute(volume<float> & im_conn, const int & node)
{
	//TODO replace the list with an unordered_map;
	//TODO make compatible with directiornal graphs
	//TODO implement efficient shuffle to reduce memory complexity

	//Number of nodes or feature;
	int Nnodes=im_conn.xsize();//square

	//loaded with indices that are not N/
	//will then randomize the indices which are used to index
	//the random permutation
	list<int> all_inds;
	vector<float> data_orig(Nnodes, 0);
	//loop over all nodes
	//loads indices and store data
	for (int i = 0 ; i < Nnodes; ++i) //+ exlude self connection
	{
		//exclude the node for which it is swapping into
		if (i != node) {
			all_inds.push_back(i);
		}
		data_orig[i]=im_conn.value(node,i,0);//undirected

	}

	//sample existing indices
	list<int>::iterator l_i;
	int index=0;
	//randomly sample
	while (!all_inds.empty()) {
		if (index != node)
		{
			//randomly index the list of available nodes
			int ind = rand() % all_inds.size();
			//access the value to swap out
			l_i = all_inds.begin();
			advance(l_i,ind);

			//swap in data, above and below diagonal
			im_conn.value(node, index,0) = im_conn.value(index, node,0) = data_orig[*l_i];
			all_inds.erase(l_i);
		}
		++index;
	}

}


//resample matrix given a mask
//used when network matrix is a superset of desired matrix
volume4D<float> resampleMatrixAndSortByCluster( const volume4D<float> & conn_matrix , const volume<int> & mask , volume<int> & resamp_maskout,vector<string> & labels, vector<int> & v_clusters , vector< volume4D<float> > & sub_nets , vector<int> & sub_nets_labels )
		{
	//conn_matrix is a 4D volume 
	//mask is the desired resample mask, it also encodes group member ship
	//gives a new reduced dimensionality mask
	//labels: feature labels
	//v_clusters: cluster membership
	//sub_nets 
	//sub_nets_labels

	//find the number of nodes that remains
	int Nt = conn_matrix.tsize();
	vector<string> labels_out;
	vector<int> v_clusters_out;
	if (! sub_nets.empty())
		sub_nets.clear();

	if (sub_nets_labels.empty())
		sub_nets_labels.clear();


	//maps features to indices and back
	//indices will be changing as we resample
	//indices are the index where a particular group is stored
	//map contains all unique indices
	//TODO document the mask format and why I don't just search the diagonal

	map<int,int> features_2_index,index_2_features;
	int N = mask.xsize();
	for (int x = 0 ; x < N ; ++x )
		for (int y = 0 ; y < N ; ++y )
		{
			int val =  mask.value(x,y,0);
			if ( val != 0 )
			{
				//insert element at val if val does not exist
				//val indicates group as defined by mask
				features_2_index.insert(pair<int,int> (val, features_2_index.size()));
			}
		}

	//create the reverse mapping
	//index_2_features give you group at a given index
	for ( auto i_m : features_2_index )
	{
		index_2_features[i_m.second]=i_m.first;
	}

	//stores what groups were found in the data
	//and stores what feature indices are associated with each group
	//TODO why am I looping over all entries and not just the diagonal
	//TODO replace with a multimap
	vector< set<int> > group_inds(features_2_index.size());
	//populate the group indices
	for (int x = 0 ; x < N ; ++x )
		for (int y = 0 ; y < N ; ++y )
		{
			//value of mask at a given edge
			int val =  mask.value(x,y,0);
			//exclude 0 values
			if ( val != 0 )
			{
				//groups_inds is index by the mapping of feature to index
				//for each group it is listing features
				group_inds[ features_2_index[val] ].insert(x);
				group_inds[ features_2_index[val] ].insert(y);

			}
		}

	//This hash table maps old indices to new ones
	//going to populate this structure
	map<int,int> index_2_index;
	//totalN counts the total number of features
	int totalN=0;
	{
		//two running indices, one faster, one slower
		int index=0;
		int index_new=0;
		//loops over groups, index keeps track of group
		for (vector< set<int> >::iterator i = group_inds.begin(); i != group_inds.end(); ++i,++index)
		{
			//add in number of features from the group at *i
			totalN+=i->size();
			//loop over features with group
			//index_new is counting feature
			//this loop is effectively resorting by group
			//TODO confiorm this statement
			for (set<int>::iterator ii = i->begin(); ii != i->end(); ++ii , ++index_new)//looping around every feature
			{
				//only inserts if *ii does not exist
				index_2_index.insert( pair<int,int>(*ii,index_new) );
			}
		}
	}

	//resampled netowrk matrices
	volume4D<float> resamp_matrix(totalN,totalN,1,Nt);
	//resample masks
	volume<int> resamp_mask(totalN,totalN,1);

	resamp_matrix=0;
	resamp_mask=0;

	cout<<"Setting up within-network connections "<<endl;
	labels_out.resize(totalN);
	v_clusters_out.resize(totalN);
	//all within matrix connection
	//is breaking processing down by netowrk
	{
		//loops over subnetworks or clusters
		int index=0;
		for (vector< set<int> >::iterator i = group_inds.begin(); i != group_inds.end(); ++i,++index)
		{
			volume4D<float> net_matrix_r(i->size(),i->size(),1,Nt);
			net_matrix_r=0;
			for (int t = 0 ; t < Nt; ++t){

				totalN+=i->size();
				//            cout<<"group "<<index_2_features[index]<<endl;
				sub_nets_labels.push_back(index_2_features[index]);
				int sub_indexx=0, sub_indexy=0;
				for (set<int>::iterator ii = i->begin(); ii != i->end(); ++ii , ++sub_indexx)
				{

					set<int>::iterator iii=ii;
					++iii;
					sub_indexy=sub_indexx+1;//keep track of index in subnetowrk, for saving individual networks

					int indX=index_2_index[*ii];
					labels_out[indX] = labels[*ii];
					v_clusters_out[indX] = v_clusters[*ii];
					//                    cout<<"add v_cluster "<<indX<<" "<<*ii<<" "<<v_clusters[*ii]<<endl;

					resamp_mask.value(indX,indX,0)=index_2_features[index]; //set diagonal
					for ( ; iii != i->end(); ++iii , ++sub_indexy)
					{

						//					cout<<*ii<<" "<<*iii<<endl;
						int indY=index_2_index[*iii];
						resamp_matrix[t].value(indX,indY,0)=conn_matrix[t].value(*ii,*iii,0);
						resamp_matrix[t].value(indY,indX,0)=conn_matrix[t].value(*iii,*ii,0);
						resamp_mask.value(indX,indY,0)=index_2_features[index];
						resamp_mask.value(indY,indX,0)=index_2_features[index];

						//cout<<"net "<<sub_indexx<<" "<<sub_indexy<<" "<<*ii<<" "<<*iii<<endl;
						net_matrix_r[t].value(sub_indexx,sub_indexy,0) = conn_matrix[t].value(*ii,*iii,0);
						net_matrix_r[t].value(sub_indexy,sub_indexx,0) = conn_matrix[t].value(*iii,*ii,0);

					}
					//				cout<<endl;
				}
			}
			//			save_volume(net_matrix_r,"net_matrix_r");
			sub_nets.push_back(net_matrix_r);
		}
	}
	//internetwrok connections
	cout<<"Setting up internetwork connections "<<endl;
	int index=0;
	for (vector< set<int> >::iterator j = group_inds.begin(); j != group_inds.end(); ++j,++index)
	{
		for (int t = 0 ; t < Nt; ++t){

			for (set<int>::iterator jj = j->begin(); jj != j->end(); ++jj )
			{
				for (vector< set<int> >::iterator i = group_inds.begin(); i != group_inds.end(); ++i)
				{
					if ( i != j )
					{
						for (set<int>::iterator ii = i->begin(); ii != i->end(); ++ii )
						{

							int indX=index_2_index[*jj];
							int indY=index_2_index[*ii];
							resamp_matrix[t].value(indX,indY,0)=conn_matrix[t].value(*jj,*ii,0);
							resamp_matrix[t].value(indY,indX,0)=conn_matrix[t].value(*ii,*jj,0);

						}
					}
				}
			}
		}
	}
	//
	//	//setup diagonal
	//	{	
		//		//int index=0;
		////		for (vector<int>::iterator i = sub_nets_labels.begin(); i != sub_nets_labels.end(); ++i,++index)
			////		{
			////			cout<<"index "<<index<<" "<<*i<<endl;
			////			resamp_mask.value(index,index,0) = *i;
			////		}
		//	}

	resamp_maskout=resamp_mask;
	labels=labels_out;
	v_clusters=v_clusters_out;
	return resamp_matrix;
		}

UndirectedGraph do_work_3D_Graph( const volume<float> & connectivity_matrix, network_metrics & nmets, const g_thresh_method & g_meth,const float & weight_thresh, const  std::vector<int> & clusters,  const std::vector<int> & excl_inds, const bool & doBinarize )
{
	//assumes resampling us already been done

	float w_th_sc = weight_thresh ;//use for edge weight thresholding
	float w_th_ind = weight_thresh;//used for index threshold (i.e. percentiles)
	int num_nodes=connectivity_matrix.xsize();
	int num_edges=num_nodes * ( num_nodes -1 )  *0.5;

	cout<<"There are "<<num_nodes<<" nodes"<<endl;
	cout<<"total number of edges "<<num_edges<<endl;
	vector<string> name(num_nodes);
	for (int i = 0; i < num_nodes; ++i)
		name[i] = "region_"+int2str(i);


	list< pair<float,ind_coord> > l_edges_weights;
	// declare a graph object

	UndirectedGraph g(num_nodes);
	property_map<UndirectedGraph, edge_weight_t>::type weightmap = get(edge_weight, g);
	int edge_ind = 0;
	float Nweighted=0.0f;
	for (int i = 0; i < num_nodes; ++i)
		for (int j = i+1; j < num_nodes; ++j,++edge_ind)
		{
			// graph_traits<Graph>::edge_descriptor e; bool inserted;
			Edge e; bool inserted;

			if (connectivity_matrix.value(i,j,0) != 0)
			{
				tie(e, inserted) = add_edge(i,j, g);
				weightmap[e] = 1/abs(connectivity_matrix.value(i,j,0));
				Nweighted+=weightmap[e];
				l_edges_weights.push_back( pair<float,ind_coord>(weightmap[e],ind_coord(edge_ind,i,j) ));
			}
		}
	//sorting grpahs by edge weights
	l_edges_weights.sort(compare_weight);


//	property_map < UndirectedGraph, edge_weight_t >::type weight = get(edge_weight, g);



	std::vector < Edge > spanning_tree;

	if (g_meth == COMPLETE)
	{
		cout<<"do not threshold graph "<<endl;
		//do nothing
	}else if ( g_meth == MST )
    {
        cout<<"Running MST"<<endl;
        kruskal_minimum_spanning_tree(g, std::back_inserter(spanning_tree));

        setEdges(spanning_tree,g);


    }else if ( g_meth == MST_MIN_W )
    {
        cout<<"Running MST plus adding back in edges based on min weight"<<endl;
        kruskal_minimum_spanning_tree(g, std::back_inserter(spanning_tree));
        setEdges(spanning_tree,g);

        //remove MST from list
        //MST is ordered

        for (std::vector < Edge >::iterator ei = spanning_tree.begin(); ei != spanning_tree.end(); ++ei) {
            list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin();

            for ( ; i_e_w != l_edges_weights.end(); ++i_e_w )
            {
                if ((source(*ei, g) == i_e_w->second.x) && (target(*ei, g) == i_e_w->second.y) )
                {
                    i_e_w = l_edges_weights.erase (i_e_w);
                    break;
                }
            }

        }

        float thresh_wmax=weightmap[spanning_tree.back()];
        for (list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin(); i_e_w != l_edges_weights.end(); ++i_e_w)
        {
            if (i_e_w->first <= thresh_wmax)
            {
                Edge e; bool inserted;
                tie(e, inserted) = add_edge(i_e_w->second.x,i_e_w->second.y,g);
                weightmap[e]=i_e_w->first;
            }
        }


    }else if ( g_meth == MST_W_PERC )
	{
		cout<<"Running MST plus adding back in edges based on percentile of connections"<<endl;
		kruskal_minimum_spanning_tree(g, std::back_inserter(spanning_tree));
		setEdges(spanning_tree,g);
		//        cout<<"mst "<<boost::num_edges(g)<<endl;
		//        print_graphEdges(g);
		//remove MST from list
		//MST is ordered
		int Nerase=0;
		for (std::vector < Edge >::iterator ei = spanning_tree.begin(); ei != spanning_tree.end(); ++ei) {
			// cout<<"Try to erase "<<
			list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin();

			for ( ; i_e_w != l_edges_weights.end(); ++i_e_w )
			{
				if ( ((source(*ei, g) == i_e_w->second.x) && (target(*ei, g) == i_e_w->second.y) )  )
				{
					++Nerase;
					i_e_w = l_edges_weights.erase (i_e_w);
					break;
				}
			}
		}
		if (spanning_tree.size() < w_th_ind*num_edges)
		{
			int shift = w_th_ind*num_edges - spanning_tree.size();
			cout<<"lets add back edges "<<spanning_tree.size()<<" "<<w_th_ind*num_edges<<" "<<shift<<endl;

			list< pair<float,ind_coord> >::iterator i_e_w_thresh = l_edges_weights.begin();
			advance(i_e_w_thresh, shift);
			w_th_sc = i_e_w_thresh->first ;
			int count=0;
			for (list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin(); i_e_w != l_edges_weights.end(); ++i_e_w,++count)
			{
				if (i_e_w->first < w_th_sc)
				{
					Edge e; bool inserted;
					tie(e, inserted) = add_edge(i_e_w->second.x,i_e_w->second.y,g);
					weightmap[e]=i_e_w->first;
					//                    cout<<"count "<<count<<" "<<weight[e]<<endl;
				}else{
					break;
				}
			}

		}

	}else if ( g_meth == MST_W_ABS_THRESH )
    {
        cout<<"Running MST plus adding back in edges based on absolute threshold"<<endl;
        kruskal_minimum_spanning_tree(g, std::back_inserter(spanning_tree));
        setEdges(spanning_tree,g);

        //remove MST from list
        //MST is ordered

        for (std::vector < Edge >::iterator ei = spanning_tree.begin(); ei != spanning_tree.end(); ++ei) {
            list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin();

            for ( ; i_e_w != l_edges_weights.end(); ++i_e_w )
            {
                if ((source(*ei, g) == i_e_w->second.x) && (target(*ei, g) == i_e_w->second.y) )
                {
                    i_e_w = l_edges_weights.erase (i_e_w);
                    break;
                }
            }


        }


        for (list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin(); i_e_w != l_edges_weights.end(); ++i_e_w)
        {
        	cout<<"g2 : do i add back "<<i_e_w->first<<" "<<w_th_sc<<endl;
            if (i_e_w->first <= w_th_sc)
            {
                Edge e; bool inserted;
                tie(e, inserted) = add_edge(i_e_w->second.x,i_e_w->second.y,g);
                weightmap[e]=i_e_w->first;
            }
//            }else{
//                break;
//            }
        }


    }else if ( g_meth == ABS_THRESH ){
        cout<<"Graph based on absolute threshold (*Brian discourages the general use of this without adequate normalization)."<<endl;

        clearEdges(g);
        for (list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin(); i_e_w != l_edges_weights.end(); ++i_e_w)
        {
        	cout<<"thresh graph "<<i_e_w->first<<" "<<w_th_sc<<endl;
        	//weight are for distance
            if (i_e_w->first <= w_th_sc)
            {
                Edge e; bool inserted;
                tie(e, inserted) = add_edge(i_e_w->second.x,i_e_w->second.y,g);
                weightmap[e]=i_e_w->first;

            }
        }
    }else if ( g_meth == PERC ){
        cout<<"Graph based on Percentile of edges threshold."<<endl;

        clearEdges(g);
        list< pair<float,ind_coord> >::iterator i_e_w_thresh = l_edges_weights.begin();
        
        cout<<"g5 : " <<w_th_ind<<" "<<num_edges<<endl;
        advance(i_e_w_thresh, w_th_ind*num_edges);
        w_th_sc = i_e_w_thresh->first ;
        cout<<"thresh "<<w_th_sc<<endl;


        for (list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin(); i_e_w != l_edges_weights.end(); ++i_e_w)
        {
            if (i_e_w->first <= w_th_sc)
            {
                Edge e; bool inserted;
                tie(e, inserted) = add_edge(i_e_w->second.x,i_e_w->second.y,g);
                weightmap[e]=i_e_w->first;

            }
        }
    }else if ( g_meth == MST_U_PERC ){
        cout<<"Running Union of MST and percentile of connections"<<endl;
        kruskal_minimum_spanning_tree(g, std::back_inserter(spanning_tree));
        setEdges(spanning_tree,g);

        {//percentile part, calculate
            list< pair<float,ind_coord> >::iterator i_e_w_thresh = l_edges_weights.begin();//get edges list with weights
            advance(i_e_w_thresh, w_th_ind*num_edges);//got to percentile
            w_th_sc = i_e_w_thresh->first ;//save threhold
        }

        
        //add back in the percentile based on original pre-mst tthreshold
        for (list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin(); i_e_w != l_edges_weights.end(); ++i_e_w)
            //	,++index)
        {
//            cout<<"test "<<index<<" "<<i_e_w->first<<" "<<w_th_sc<<endl;
            if (i_e_w->first <= w_th_sc)
            {
                Edge e; bool inserted;
                tie(e, inserted) = add_edge(i_e_w->second.x,i_e_w->second.y,g);
                weightmap[e]=i_e_w->first;
            }
//            }else{
//                break;
//            }
        }

    }

	if (doBinarize)
	{
		//binarize all edges for unweighted graph
//		property_map < UndirectedGraph, edge_weight_t >::type weight = get(edge_weight, g);

		graph_traits < UndirectedGraph >::edge_iterator ei, eend;
		for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
			weightmap[*ei]=1;
		}

		//
		//    	    for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
		//    	        cout<<"edge "<<source(*ei, g)<<"<-->"<<target(*ei, g)<<" ---> "<<weight[*ei]<<endl;
		//    	    }

	}


	cout<<"Number of edges survived: "<<boost::num_edges(g)<<endl;
	calculateGraphMetrics(g,nmets,clusters,excl_inds);
	nmets.cost = sum(nmets.degree) / Nweighted;//NWeight is pre thresholding
	return g;

}

DirectedGraph do_work_3D_diGraph( const volume<float> & connectivity_matrix, network_metrics & nmets, const g_thresh_method & g_meth,const float & weight_thresh, const  std::vector<int> & clusters,  const std::vector<int> & excl_inds )
{
	//assumes resampling us already been done

	float w_th_sc = weight_thresh ;//use for edge weight thresholding
	float w_th_ind = weight_thresh;//used for index threshold (i.e. percentiles)
	int num_nodes=connectivity_matrix.xsize();
	int num_edges=num_nodes * ( num_nodes -1 )  *0.5;

	cout<<"There are "<<num_nodes<<" nodes"<<endl;
	cout<<"total number of edges "<<num_edges<<endl;
	vector<string> name(num_nodes);
	for (int i = 0; i < num_nodes; ++i)
		name[i] = "region_"+int2str(i);


	list< pair<float,ind_coord> > l_edges_weights;
	// declare a graph object

	DirectedGraph g(num_nodes);
	property_map<DirectedGraph, edge_weight_t>::type weightmap = get(edge_weight, g);
	int edge_ind = 0;
	float Nweighted=0.0f;
	for (int i = 0; i < num_nodes; ++i)
		for (int j = 0; j < num_nodes; ++j,++edge_ind)
		{
			// graph_traits<Graph>::edge_descriptor e; bool inserted;
			DirectedEdge e; bool inserted;

			if ((connectivity_matrix.value(i,j,0) != 0) && (i != j ))
			{
				//                    cout<<"add edge to digraph "<<i<<" "<<j<<" "<<connectivity_matrix.value(i,j,0)<<endl;
				tie(e, inserted) = add_edge(i,j, g);
				weightmap[e] = 1/abs(connectivity_matrix.value(i,j,0));
				Nweighted+=weightmap[e];
				l_edges_weights.push_back( pair<float,ind_coord>(weightmap[e],ind_coord(edge_ind,i,j) ));
			}
		}
	//sorting grpahs by edge weights
	//need to replace this sort with a faster one
	l_edges_weights.sort(compare_weight);
	if (g_meth == COMPLETE)
	{
		cout<<"do not threshold graph "<<endl;
		//do nothing
	}else if ( g_meth == ABS_THRESH ){
	        cout<<"Graph based on absolute threshold (*Brian discourages the general use of this without adequate normalization)."<<endl;
//	        property_map < DirectedGraph, edge_weight_t >::type weight = get(edge_weight, g);
	        clearEdges(g);
	        for (list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin(); i_e_w != l_edges_weights.end(); ++i_e_w)
	        {
	            if (i_e_w->first <= w_th_sc)
	            {
	            	DirectedEdge e; bool inserted;
	                tie(e, inserted) = add_edge(i_e_w->second.x,i_e_w->second.y,g);
	                weightmap[e]=i_e_w->first;

	            }
	        }
	    }else if ( g_meth == PERC ){
	    	 cout<<"Graph based on Percentile of edges threshold."<<endl;

	    	        clearEdges(g);
	    	        list< pair<float,ind_coord> >::iterator i_e_w_thresh = l_edges_weights.begin();
	    	        
	    	        cout<<"g5 : " <<w_th_ind<<" "<<num_edges<<endl;
	    	        advance(i_e_w_thresh, w_th_ind*num_edges);
	    	        w_th_sc = i_e_w_thresh->first ;
	    	        cout<<"thresh "<<w_th_sc<<endl;


	    	        for (list< pair<float,ind_coord> >::iterator i_e_w = l_edges_weights.begin(); i_e_w != l_edges_weights.end(); ++i_e_w)
	    	        {
	    	            if (i_e_w->first <= w_th_sc)
	    	            {
	    	                DirectedEdge e; bool inserted;
	    	                tie(e, inserted) = add_edge(i_e_w->second.x,i_e_w->second.y,g);
	    	                weightmap[e]=i_e_w->first;

	    	            }
	    	        }
	    }else if ( ( g_meth ==  MST_MIN_W ) || ( g_meth == MST_W_ABS_THRESH ) || 
				( g_meth == MST_W_ABS_THRESH ) || ( g_meth ==  MST ) || ( g_meth ==  MST_W_PERC ) ||
				( g_meth == MST_U_PERC ) )
	    {
	    	cerr<<"MST not supported for directed graph"<<endl;
	    	exit (EXIT_FAILURE);
	    }else{
	    	cerr<<"Unsupported Graph Type"<<endl;
	    	exit (EXIT_FAILURE);
	    }

//	property_map < DirectedGraph, edge_weight_t >::type weight = get(edge_weight, g);
	//for now assuming we are not thresholding


	cout<<"Number of edges survided: "<<boost::num_edges(g)<<endl;
	calculateGraphMetrics(g,nmets,clusters,excl_inds);
	nmets.cost = sum(nmets.degree) / Nweighted;//NWeight is pre thresholding
	return g;

}

UndirectedGraph do_work_3D_Graph( const volume<float> & connectivity_matrix, network_metrics & nmets,const g_thresh_method & g_meth,const float & weight_thresh, const  std::vector<int> & clusters, const bool & doBinarize )
{
	std::vector<int> excl_inds;//empty vector

	return do_work_3D_Graph(  connectivity_matrix,  nmets,g_meth, weight_thresh, clusters, excl_inds,doBinarize );
}

DirectedGraph do_work_3D_diGraph( const volume<float> & connectivity_matrix, network_metrics & nmets,const g_thresh_method & g_meth,const float & weight_thresh, const  std::vector<int> & clusters)
{
	std::vector<int> excl_inds;//empty vector

	return do_work_3D_diGraph(  connectivity_matrix,  nmets,g_meth, weight_thresh, clusters, excl_inds );
}



int do_work( const string & inname , const string & maskname, const string & labelname, const string & subjectlistname, const string & sub_groupname,const string & clustername, const string & c_namemap, const string & outname , const int & swap_index, const g_thresh_method & g_meth, const float & weight_thresh,  const  int & Nperms, const bool & do_knock_out, const bool & doBinarize ,const bool & directional, bool verbose=false  ){
	if (verbose)  cout<<"Reading input connectivity matrices : "<<inname<<endl;
	//    g_thresh_method g_meth = MST_MIN_W;

	//--------READ IN Data including masks, labels, subject names etc.....-------
	volume4D<float> connectivity_matrix;
	read_volume4D(connectivity_matrix,inname);
	int Nfeatures = connectivity_matrix.xsize();
	int Nsubjects =connectivity_matrix.tsize();
	vector<string> v_subnames(Nsubjects,"");


	if (subjectlistname.empty())
	{
		if (verbose)  cout<<"No file containing subject names found. Numbering from 0 -> "<<(Nsubjects-1)<<endl;

		for (int i = 0 ; i < Nsubjects; ++i)
			v_subnames[i]="Subject_"+int2str(i);


	}else{
		if (verbose)  cout<<"Reading in subject names..."<<endl;

		ifstream f_sub(subjectlistname.c_str());
		if (f_sub.is_open())
		{
			string name;
			vector<string>::iterator i_s = v_subnames.begin();
			while (getline(f_sub, name)) {
				*i_s = name;
				++i_s;
			}
			f_sub.close();
		}else{
			for (int i = 0 ; i < Nsubjects; ++i)
				v_subnames[i]="Subject_"+int2str(i);
		}

	}



	vector<int> v_group(Nsubjects,0);
	{
		if (! sub_groupname.empty())
		{
			if (verbose) cout<<"Reading in subject group assignment file : "<<sub_groupname<<endl;
			ifstream f_gr(sub_groupname.c_str());
			if (f_gr.is_open())
			{
				int gr;
				string sgr;
				vector<int>::iterator i_s = v_group.begin();
				while (getline(f_gr,sgr)) {
					*i_s = string2int(sgr);
					++i_s;
				}
				f_gr.close();
			}
		}else{
			if (verbose) cout<<"Group subject group assignment found."<<endl;

		}

	}

	volume<int> mask,mask_resamp;
	vector<int> v_clusters(Nfeatures,0);

	if (! maskname.empty() )
	{
		if (verbose) cout<<"Reading in mask : "<<maskname<<endl;
 		read_volume(mask,maskname);
 		//populate
	}else{
		if (verbose) cout<<"No mask for network matrices found."<<endl;

	}


//	set<int> s_networks_labels;
	if ( clustername.empty())
	{
		//get cluster values from mask
		//		s_networks_labels = getUniqueLabels(mask);
		//get and assign the cluster value to each feature

		if (!maskname.empty())
		{
			for (int i = 0 ; i < Nfeatures ; ++i)
			{
				v_clusters[i]=mask.value(i,i,0);
			}
		}
//		}else{
//			for (int i = 0 ; i < Nfeatures ; ++i)
//						{
//							v_clusters[i]=mask.value(i,i,0);
//						}
//		}
	}else{
		if (verbose) cout<<"Reading in cluster names : "<<clustername<<endl;
		ifstream f_labels(clustername.c_str());
		if (f_labels.is_open())
		{
			string label;
			int index=0;
			while (getline(f_labels, label)) {
				v_clusters[index]=(string2int(label));
				++index;
			}
			f_labels.close();
		}else{
			for (int i=1 ; i <= Nfeatures ; ++i)
				v_clusters[i]=(1);
		}
	}

	map<int,string> cl_name_map;//read in mapping of cluster labels to names, used for file and variable naming
	if ( ! c_namemap.empty() )
	{
		if (verbose) cout<<"Reading in names of defined clusters : "<<clustername<<endl;
		ifstream f_cmap(c_namemap.c_str());
		if (f_cmap.is_open())
		{
			int cl;
			string name,line;
			while (getline(f_cmap, line)) {
				stringstream ss;
				ss<<line;
				ss>>cl>>name;
				cl_name_map[cl] = name;
			}
			f_cmap.close();
		}

	}else{
		if (verbose) cout<<"No cluster names found."<<endl;

	}


	vector<string> v_labels;
	if (labelname.size() == 0)
	{
		if (verbose) cout<<"No feature names found. Using 0 -> "<<(Nfeatures-1)<<endl;

		for (int i=1 ; i <= Nfeatures ; ++i)
			v_labels.push_back(int2str(i));
	}else{
		if (verbose) cout<<"Reading in feature names : "<<labelname<<endl;

		ifstream f_labels(labelname.c_str());
		if (f_labels.is_open())
		{
			string label;
			while (getline(f_labels, label)) {
				v_labels.push_back(label);
			}
			f_labels.close();
		}else{
			for (int i=1 ; i <= Nfeatures ; ++i)
				v_labels.push_back(int2str(i));
		}
	}
	//--------DONE READ IN Data including masks, labels, subject names etc.....-------

cout<<"do masking"<<endl;
	//Resample to matrix to desired ROIs
	vector< volume4D<float> > im_subnetworks;
	vector< int > sub_nets_labels;
	if ( ! maskname.empty() )
	{
		cout<<"Applying mask and resampling matrices..."<<endl;
		connectivity_matrix = resampleMatrixAndSortByCluster(connectivity_matrix,mask,mask_resamp,v_labels,v_clusters,im_subnetworks,  sub_nets_labels);
		save_volume4D(connectivity_matrix, outname+"_resampled");
		save_volume(mask_resamp,outname+"mask_resamp");
		ofstream flabels((outname + "_features_resampled.txt").c_str());
		if (flabels.is_open())
		{
			for (auto i : v_labels )
				flabels<<i<<endl;
			flabels.close();
		}
		//output sorted labels by group

	}

	cout<<"done masking "<<endl;
	//    int Nsubjects = connectivity_matrix.tsize();
	//this is not the best for memory thow it does mean we have more freedom to work with graphs
	//could add option to delete the graph
	vector<network_metrics> v_nmets,v_nmets_permNodes, v_nmets_ko;
	vector<string> v_subnames_new;
	vector<int>::iterator i_gr = v_group.begin();
	vector<string>::iterator i_sub = v_subnames.begin();
	//create and analyze graphs for all subjects.
	//v_graphs and v_digrapghs are used for output
	vector<UndirectedGraph> v_graphs;
	vector<DirectedGraph> v_digraphs;
	for (int t = 0 ; t < Nsubjects; ++t,++i_gr,++i_sub)
	{
		cout<<"---------------------------"<<endl;
		cout<<"Subject "<<(*i_sub)<<" "<<t<<endl;
	

		//		if  (*i_gr == swap_group){//calculate original metrics
		///------------------------------Unaltered Graph Metrics------------------------------//
		{
			network_metrics nmets;
			if ( directional )
				v_digraphs.push_back(do_work_3D_diGraph(connectivity_matrix[t],  nmets,g_meth, weight_thresh, v_clusters));
			else
				v_graphs.push_back(do_work_3D_Graph(connectivity_matrix[t],  nmets,g_meth, weight_thresh, v_clusters,doBinarize));

			v_nmets.push_back(nmets);

		}
		///------------------------------Unaltered Graph Metrics- DONE-----------------------------//
		///------------------------------Swap in data from other graphs Graph Metrics------------------------------//
		if ( swap_index > -1 )
		{
			//            cout<<"Swap data in from other graphs "<<endl;

			network_metrics nmets_mean,nmets_mean_within;
			{//swaps data from other subject outside group
				int count = 0;
				vector<int>::iterator i_gr_swap = v_group.begin();
				for (int tswap = 0 ; tswap < Nsubjects; ++tswap,++i_gr_swap)
				{
					network_metrics nmets;
					if ( (*i_gr) != (*i_gr_swap) )
					{
						volume<float> imswap = connectivity_matrix[t];//resets matrix for each swap
						swapin_roi_connectivity(imswap,connectivity_matrix[tswap], swap_index);
						if ( directional )
//							v_digraphs.push_back(do_work_3D_diGraph(imswap,  nmets, g_meth, weight_thresh, v_clusters));
							do_work_3D_diGraph(imswap,  nmets, g_meth, weight_thresh, v_clusters);
						else
//							v_graphs.push_back(do_work_3D_Graph(imswap,  nmets, g_meth, weight_thresh, v_clusters,doBinarize));
							do_work_3D_Graph(imswap,  nmets, g_meth, weight_thresh, v_clusters,doBinarize);
						
						if (count == 0 )
							nmets_mean=nmets;
						else
							nmets_mean+=nmets;

						++count;
					}
				}
				nmets_mean/=count;

			}
			{//swaps data from other subject outside group
				int count = 0;
				vector<int>::iterator i_gr_swap = v_group.begin();
				for (int tswap = 0 ; tswap < Nsubjects; ++tswap,++i_gr_swap)
				{

					network_metrics nmets;

					if ( (*i_gr) == (*i_gr_swap) )
					{
						volume<float> imswap = connectivity_matrix[t];
						swapin_roi_connectivity(imswap,connectivity_matrix[tswap], swap_index);
						if ( directional ){
//							v_digraphs.push_back(do_work_3D_diGraph(imswap,  nmets, g_meth, weight_thresh, v_clusters));
							do_work_3D_diGraph(imswap,  nmets, g_meth, weight_thresh, v_clusters);
						}else{
//							v_graphs.push_back(do_work_3D_Graph(imswap,  nmets, g_meth, weight_thresh, v_clusters,doBinarize));
							do_work_3D_Graph(imswap,  nmets, g_meth, weight_thresh, v_clusters,doBinarize);
						}
						
						if (count == 0 )
							nmets_mean_within=nmets;
						else
							nmets_mean_within+=nmets;

						++count;

					}
				}
				nmets_mean_within/=count;
			}

			v_nmets.push_back(nmets_mean);
			v_nmets.push_back(nmets_mean_within);

		}
		///------------------------------Swap in data from other graphs Graph Metrics DONE------------------------------//

		///------------------------------Permute Nodes------------------------------//
		{
			//already loop across subject
			if (Nperms > 0 )
			{
				cout<<"Nperms "<<Nperms<<endl;

				cout<<"Permuting Connection with Nodes... "<<endl;
				int Nnodes=connectivity_matrix.xsize();
				for (int p_node = 0 ; p_node < Nnodes; ++p_node)
				{//permute for each node
					volume<float> im_conn = connectivity_matrix[t];

					volume4D<float> imtest;
					network_metrics nmets_mean;
					for (int i = 0 ; i < Nperms; ++i )
					{
						//                        cout<<"Nodes "<<p_node<<" : perm : "<<i<<endl;
						network_metrics nmets;
						permute(im_conn,p_node);
						imtest.addvolume(im_conn);
						if  (directional)
							do_work_3D_diGraph(im_conn,  nmets, g_meth, weight_thresh, v_clusters);
						else
							do_work_3D_Graph(im_conn,  nmets, g_meth, weight_thresh, v_clusters,doBinarize);

						if (i == 0 )
							nmets_mean=nmets;
						else
							nmets_mean+=nmets;

					}
					//                    save_volume4D(imtest,"perm_test_node" + int2str(p_node));
					//                exit (EXIT_FAILURE);
					nmets_mean/=Nperms;
					//                    cout<<"nemts-mean ";
					//                    nmets_mean.print();
					v_nmets_permNodes.push_back(nmets_mean);
				}
			}
		}
		///------------------------------Permute Nodes DONE------------------------------//
		///------------------------------KnockOut Nodes------------------------------//
		//already loop across subject
		if(do_knock_out){
			cout<<"Knocking out each node... "<<endl;
			int Nnodes=connectivity_matrix.xsize();
			//            volume4D<float> imtest;

			for (int p_node = 0 ; p_node < Nnodes; ++p_node)
			{//permute for each node
				volume<float> im_conn = connectivity_matrix[t];
				network_metrics nmets;
				zero_node(im_conn,p_node);
				//                imtest.addvolume(im_conn);
				vector<int> excl_inds(1,p_node);
				//                cout /Vo<<"excl2 "<<excl_inds.size()<<endl;
				if (directional){
					do_work_3D_diGraph(im_conn,  nmets,g_meth,  weight_thresh, v_clusters,excl_inds);
				}else{
					do_work_3D_Graph(im_conn,  nmets,g_meth,  weight_thresh, v_clusters,excl_inds,doBinarize);
				}
				//                nmets.printHeader();
				//                cout<<"konemts ";
				//                nmets.print();

				v_nmets_ko.push_back(nmets);

			}

		}
		//        cout<<"KnockOutSize "<<v_nmets_ko.size()<<endl;
		///------------------------------DONE KnockOut Nodes------------------------------//

		//		if  (*i_gr == swap_group)
		{

			v_subnames_new.push_back(*i_sub);

		}
	}
	
	
	if ( directional )
			writeGraphAsNIFTI(outname+"_graphs", v_digraphs,directional);
		else
			writeGraphAsNIFTI(outname+"_graphs", v_graphs,directional);

		
	//actual graphs metrics
	{
		//    	cout<<"write network metrics: "<<(outname + "_network_metrics.csv")<<" "<<v_nmets.size()<<" "<<endl;
		ofstream fout_netmets((outname + "_network_metrics.csv").c_str());
		//oupt network metrics
		//    ofstream fout_pernetmets(outname + "_per_network_metrics.csv");
		if ( ! v_nmets.empty() )
		{
			if (fout_netmets.is_open())
			{
				//        		cout<<"Write network metrics : label size "<<v_labels.size()<<endl;
				fout_netmets<<"SubjectID,group,";
				v_nmets[0].node_names = v_labels;
				v_nmets[0].clust_index_2_name = cl_name_map;
				v_nmets[0].writeHeader(fout_netmets);
				//            fout_netmets<<",";
				//            v_nmets[0].writeHeaderPerNodeAsRow(fout_netmets);
				fout_netmets<<endl;
				//        		cout<<"SubjectID,group";
				//        		network_metrics::printHeader();
				vector<string>::iterator i_s = v_subnames_new.begin();
				vector<int>::iterator i_gr = v_group.begin();


				for (vector< network_metrics >::iterator i_nm = v_nmets.begin(); i_nm != v_nmets.end(); ++i_nm,++i_s,++i_gr)
				{
					fout_netmets<<*i_s<<","<<(*i_gr)<<",";
					i_nm->write(fout_netmets);
					//                fout_netmets<<",";
					//                i_nm->writePerNodeAsRow(fout_netmets);
					fout_netmets<<endl;

					if ( swap_index > -1 )
					{//skip the 2 groups swap entries if present
						advance(i_nm,2);
					}
				}
				fout_netmets.close();
			}
		}else
		{
			cerr<<"Unable to write output file, "<<(outname + "_network_metrics.csv")<<endl;
			exit (EXIT_FAILURE);
		}
		{
			// cout<<"calculate pernode metrics"<<endl;
			//            cout<<"v_nmets "<<v_nmets.size()<<endl;
			ofstream fout_netmets((outname + "_network_metrics_per_node.csv").c_str());
			//oupt network metrics
			//    ofstream fout_pernetmets(outname + "_per_network_metrics.csv");
			if ( ! v_nmets.empty() )
				if (fout_netmets.is_open())
				{
					//calculate mean
					vector< network_metrics >::iterator i_nm = v_nmets.begin();
					network_metrics mean_net = *i_nm;
					++i_nm;
					int count=1;
					for ( ; i_nm != v_nmets.end(); ++i_nm,++count)
					{
						//cout<<"add subject metrics "<<count<<endl;
						mean_net += *i_nm;
						//                        i_nm->print();

					}

					mean_net /= v_nmets.size();

					mean_net.writeHeaderPerNode(fout_netmets);

					fout_netmets<<endl;
					mean_net.writePerNodeAsColumns(fout_netmets);

					fout_netmets.close();

				}
		}
	}
	//done writing
	{

		//t-test between groups
		map<int,int> groups2count;
		for (vector<int>::iterator i_group = v_group.begin(); i_group != v_group.end(); ++i_group)
			groups2count[*i_group]++;
		//groups.insert(*i_group);

		if (groups2count.size() == 2)//only do for two-group case
		{
			//            cout<<"Computing Cohen's D because there are 2 groups"<<endl;
			map<int,int> group2index;
			int index=0;
			for (map<int,int>::iterator i = group2index.begin(); i != group2index.end(); ++i,++index)
			{
				group2index[i->first]=index;
			}

			network_metrics net_cohend, net_gr0, net_gr1;
			int count_gr0=0;
			int count_gr1=0;
			vector<int>::iterator i_gr = v_group.begin();
			for (vector< network_metrics >::iterator i_nm = v_nmets.begin(); i_nm != v_nmets.end(); ++i_nm,++i_gr)
			{
				if (group2index[*i_gr]==0)
				{
					if (count_gr0==0)
						net_gr0=*i_nm;
					else
						net_gr0+= *i_nm;


					++count_gr0;
				}else if (group2index[*i_gr]==1)
				{
					if (count_gr1==0)
						net_gr1=*i_nm;
					else
						net_gr1+= *i_nm;


					++count_gr1;
				}
			}

			//just difference in means so far
			net_gr1/=count_gr1;
			net_gr0/=count_gr0;
			net_cohend = net_gr1;
			net_cohend -= net_gr0;

//			if (directional)
//				writeGraphAsNIFTI<etkin_graph::UndirectedGraph>(outname+"_graphs_cohenD",net_cohend,directional);
//			else
//				writeGraphAsNIFTI<etkin_graph::DirectedGraph>(outname+"_graphs_cohenD",net_cohend,directional);

		}

	}


	//----------subject swap  node output-----------------//
	if (swap_index > -1 )
	{
		ofstream fout_netmets((outname + "_network_metrics_groupswap.csv").c_str());
		//oupt network metrics
		//    ofstream fout_pernetmets(outname + "_per_network_metrics.csv");
		if ( ! v_nmets.empty() )
		{
			if (fout_netmets.is_open())
			{
				fout_netmets<<"SubjectID,swap_group,group,";
				v_nmets[0].clust_index_2_name = cl_name_map;
				v_nmets[0].writeHeader(fout_netmets);

				//    			cout<<"SubjectID,group";
				//    			network_metrics::printHeader();
				vector<string>::iterator i_s = v_subnames_new.begin();
				vector<int>::iterator i_gr = v_group.begin();

				int index=0;
				for (vector< network_metrics >::iterator i_nm = v_nmets.begin(); i_nm != v_nmets.end(); ++i_nm,++index)
				{
					if ((index % 3) == 0){
						fout_netmets<<*i_s<<","<<(index % 3)<<","<<(*i_gr)<<",";
					}else if ((index % 3) == 1 ){
						fout_netmets<<(*i_s + "_swap_withinGrp")<<","<<(index % 3)<<","<<(*i_gr)<<",";
					}else{
						fout_netmets<<(*i_s + "_swap_btwGrp")<<","<<(index % 3)<<","<<(*i_gr)<<",";
					}
					//    				cout<<*i_s<<",";
					//    				i_nm->print();
					i_nm->write(fout_netmets);
					//            i_nm->writePerNetwork(fout_pernetmets,clusters);
					if ((index % 3) == 2)
					{
						++i_s; //there are two entries for each subject
						++i_gr;
					}
				}
				fout_netmets.close();
			}else
			{
				cerr<<"Unable to write output file, "<<(outname + "_network_metrics_groupswap.csv")<<endl;
				exit (EXIT_FAILURE);
			}
		}
	}
	//----------permuted node output-----------------//
	if (Nperms > 0 )
	{
		if ( ! v_nmets_permNodes.empty() )
		{
			ofstream fout_p((outname + "_network_metrics_permNodes.csv").c_str());
			if (fout_p.is_open())
			{
				int Nnodes=connectivity_matrix.xsize();

				fout_p<<"SubjectID,group,node,";
				v_nmets_permNodes[0].writeHeader(fout_p);
				fout_p<<endl;

				vector<string>::iterator i_s = v_subnames_new.begin();
				vector<int>::iterator i_gr = v_group.begin();
				int index=0;
				for (vector< network_metrics >::iterator i_nm = v_nmets_permNodes.begin(); i_nm != v_nmets_permNodes.end(); ++i_nm,++index)
				{
					fout_p<<(*i_s)<<","<<(*i_gr)<<","<<index<<",";
					i_nm->write(fout_p);

					if ((index % Nnodes) == (Nnodes-1))//n=number of nodes
					{
						++i_s; //there are two entries for each subject
						++i_gr;
						index=-1;
					}



				}
				fout_p.close();
			}

		}

	}
	//----------knockout node output-----------------//
	if ( ! v_nmets_ko.empty() )
	{
		ofstream fout_p((outname + "_network_metrics_ko.csv").c_str());
		if (fout_p.is_open())
		{
			int Nnodes=connectivity_matrix.xsize();

			fout_p<<"SubjectID,group,node,";
			v_nmets_ko[0].writeHeader(fout_p);
			fout_p<<endl;
			vector<string>::iterator i_s = v_subnames_new.begin();
			vector<int>::iterator i_gr = v_group.begin();
			int index=0;
			for (vector< network_metrics >::iterator i_nm = v_nmets_ko.begin(); i_nm != v_nmets_ko.end(); ++i_nm,++index)
			{
				fout_p<<(*i_s)<<","<<(*i_gr)<<","<<index<<",";
				i_nm->write(fout_p);

				if ((index % Nnodes) == (Nnodes-1))//n=number of nodes
				{
					++i_s; //there are two entries for each subject
					++i_gr;
					index=-1;
				}



			}
			fout_p.close();
		}

	}

	//------------------------------------------------//





	/////////////----------------HERE's all the R - Output



	//Lets write a report in R markdown
	//this will allow for embedded plots and stats
	ofstream fout_rmd((outname + ".rmd").c_str());
	if (fout_rmd.is_open())
	{
		fout_rmd<<"Graphy Theory Report"<<endl;
		fout_rmd<<"==================="<<endl;
		fout_rmd<<"#read in data to R "<<endl;
		fout_rmd<<"```{r}"<<endl;
		//   fout_rmd<<"group <- read.table(\""<<sub_groupname<<"\",header=FALSE );"<<endl;
		fout_rmd<<"net_mets <- read.table(\""<<(outname + "_network_metrics.csv")<<"\",header=TRUE,sep= \",\")"<<endl;

		if (swap_index  > -1 )
			fout_rmd<<"net_mets_swap <- read.table(\""<<(outname + "_network_metrics_perms.csv")<<"\",header=TRUE,sep= \",\")"<<endl;

		if ( Nperms > 0 )
			fout_rmd<<"net_mets_perms <- read.table(\""<<(outname + "_network_metrics_permNodes.csv")<<"\",header=TRUE,sep= \",\")"<<endl;

		if ( ! v_nmets_ko.empty() )
			fout_rmd<<"net_mets_ko <- read.table(\""<<(outname + "_network_metrics_ko.csv")<<"\",header=TRUE,sep= \",\")"<<endl;

		fout_rmd<<"```"<<endl;


		//--------------------Actual network metrics-----------------//
		fout_rmd<<"#Network Statistics"<<endl;
		vector<string> var_names = network_metrics::getVariableNames();
		for (vector<string>::iterator i_n = var_names.begin(); i_n != var_names.end(); ++i_n)
		{
			//            cout<<"Write variable "<<endl;
			fout_rmd<<endl<<"* * *"<<endl<<endl; //add space aroudn break

			fout_rmd<<"###Variable : "<<(*i_n)<<endl;
			fout_rmd<<"```{r}"<<endl;

			fout_rmd<<"boxplot("<<(*i_n)<<"~group, data=net_mets,las=2,par(mar = c(12, 5, 4, 2)+ 0.1),names = c(\"TEHC\",\"PTSD\"))"<<endl;
			fout_rmd<<"```"<<endl;

			//omit from ttest because constant
			if  (! ( ( *i_n == "Nnodes" ) ||  ( *i_n == "Nedges" ) ||  ( *i_n == "cost" ) ||  ( *i_n == "cost_bin" ) ) )
			{

				//TEHC vs PTSD swap
				fout_rmd<<"```{r}"<<endl;
				fout_rmd<<"t.test(net_mets$"<<(*i_n)<<"~ net_mets$group,paired=FALSE)"<<endl;
				fout_rmd<<"```"<<endl;

			}

		}
		//--------------------Swap subject wihtin/without group-----------------//


		if ( swap_index > -1 )
		{
			fout_rmd<<"#Subject Swap Statistics"<<endl;

			vector<string> var_names = network_metrics::getVariableNames();
			for (vector<string>::iterator i_n = var_names.begin(); i_n != var_names.end(); ++i_n)
			{
				fout_rmd<<endl<<"* * *"<<endl<<endl; //add space aroudn break

				fout_rmd<<"###Variable : "<<(*i_n)<<endl;
				fout_rmd<<"```{r}"<<endl;

				//                fout_rmd<<"boxplot("<<(*i_n)<<"~swap_group*group, data=net_mets_swap,las=2,par(mar = c(12, 5, 4, 2)+ 0.1),names = c(\"OrigXTEHC\",\"AcrossGrSwapXTEHC\",\"WithinGrSwapXTEHC\",\"OrigXPTSD\",\"AcrossGrSwapXPTSD\",\"WithinGrSwapXPTSD\"))"<<endl;
				fout_rmd<<"```"<<endl;

				//omit from ttest because constant
				if  (! ( ( *i_n == "Nnodes" ) ||  ( *i_n == "Nedges" ) ||  ( *i_n == "cost" ) ||  ( *i_n == "cost_bin" ) ) )
				{

					//orig vs out of group swap
					fout_rmd<<endl<<"###T-test "<<(*i_n)<<" : Orig vs Outside-Group Swap"<<endl<<endl;
					fout_rmd<<"```{r}"<<endl;

					fout_rmd<<"t.test(subset(net_mets_swap$"<<(*i_n)<<", net_mets_swap$swap_group == 0 | net_mets_swap$swap_group == 1 ) ~ subset(net_mets_swap$swap_group,  net_mets_swap$swap_group == 0 | net_mets_swap$swap_group == 1 ),paired=TRUE)"<<endl;

					fout_rmd<<"```"<<endl;
					fout_rmd<<endl<<"###T-test "<<(*i_n)<<" : Orig vs Within-Group Swap"<<endl<<endl;

					fout_rmd<<"```{r}"<<endl;
					//orig vs ingroup swap
					fout_rmd<<"t.test(subset(net_mets_swap$"<<(*i_n)<<", net_mets_swap$swap_group == 0 | net_mets_swap$swap_group == 2 ) ~ subset(net_mets_swap$swap_group,  net_mets_swap$swap_group == 0 | net_mets_swap$swap_group == 2 ),paired=TRUE)"<<endl;
					fout_rmd<<"```"<<endl;

					fout_rmd<<endl<<"###"<<(*i_n)<<" Group 0 vs Group 1 (no swap)"<<endl<<endl;

					//TEHC vs PTSD swap
					fout_rmd<<"```{r}"<<endl;
					fout_rmd<<"t.test(subset(net_mets_swap$"<<(*i_n)<<", net_mets_swap$swap_group == 0 ) ~ subset(net_mets_swap$group,  net_mets_swap$swap_group == 0 ),paired=FALSE)"<<endl;
					fout_rmd<<"```"<<endl;

				}

			}
		}

		//--------------------Permutations -----------------//


		if (Nperms > 0 )
		{
			if ( ! v_nmets_permNodes.empty() )
			{
				fout_rmd<<endl<<"#Node Permutation Test "<<endl<<endl;

				vector<string> var_names = network_metrics::getVariableNames();
				for (vector<string>::iterator i_n = var_names.begin(); i_n != var_names.end(); ++i_n)
				{
					fout_rmd<<endl<<"* * *"<<endl<<endl; //add space aroudn break

					fout_rmd<<"###Variable : "<<(*i_n)<<endl;
					fout_rmd<<"```{r}"<<endl;
					fout_rmd<<"for (i in 0:max(net_mets_perms$node))"<<endl;
					fout_rmd<<"{"<<endl;
					fout_rmd<<"title <- paste(\"Node \",i)"<<endl;
					//                    fout_rmd<<"boxplot(subset(net_mets_perms$"<<(*i_n)<<", net_mets_perms$node == i)~ subset(net_mets_perms$group, net_mets_perms$node == i),main=title)"<<endl;

					if  (! ( ( *i_n == "Nnodes" ) ||  ( *i_n == "Nedges" ) ||  ( *i_n == "cost" ) ||  ( *i_n == "cost_bin" ) ) )
					{

						fout_rmd<<"if ( i == 0 ){"<<endl;
						fout_rmd<<"tstats <- t.test(subset(net_mets_perms$"<<(*i_n)<<", net_mets_perms$node == i)~ subset(net_mets_perms$group, net_mets_perms$node == i), paired=FALSE)$statistic"<<endl;
						fout_rmd<<"pvals <- t.test(subset(net_mets_perms$"<<(*i_n)<<", net_mets_perms$node == i)~ subset(net_mets_perms$group, net_mets_perms$node == i), paired=FALSE)$p.value"<<endl;
						fout_rmd<<"nodes <- i"<<endl;
						fout_rmd<<"}else{"<<endl;
						fout_rmd<<"tstats <- c(tstats,t.test(subset(net_mets_perms$"<<(*i_n)<<", net_mets_perms$node == i)~ subset(net_mets_perms$group, net_mets_perms$node == i), paired=FALSE)$statistic)"<<endl;
						fout_rmd<<"pvals <- c(pvals,t.test(subset(net_mets_perms$"<<(*i_n)<<", net_mets_perms$node == i)~ subset(net_mets_perms$group, net_mets_perms$node == i), paired=FALSE)$p.value)"<<endl;

						fout_rmd<<"nodes <- c(nodes,i)"<<endl;

						fout_rmd<<"}"<<endl;

					}
					fout_rmd<<"}"<<endl;
					if  (! ( ( *i_n == "Nnodes" ) ||  ( *i_n == "Nedges" ) ||  ( *i_n == "cost" ) ||  ( *i_n == "cost_bin" ) ) )
					{
						fout_rmd<<"barplot(tstats,main=\"t-statistic\", names.arg=nodes)"<<endl;
						fout_rmd<<"barplot(pvals,main=\"p-value\", names.arg=nodes)"<<endl;
					}
					fout_rmd<<"```"<<endl;



				}

			}
		}


		//--------------------KnockOut -----------------//

		{

			if ( ! v_nmets_ko.empty() )
			{
				fout_rmd<<endl<<"#Node Knock Out Results "<<endl<<endl;

				vector<string> var_names = network_metrics::getVariableNames();
				for (vector<string>::iterator i_n = var_names.begin(); i_n != var_names.end(); ++i_n)
				{
					fout_rmd<<endl<<"* * *"<<endl<<endl; //add space aroudn break

					fout_rmd<<"###Variable : "<<(*i_n)<<endl;
					fout_rmd<<"```{r}"<<endl;
					fout_rmd<<"for (i in 0:max(net_mets_ko$node))"<<endl;
					fout_rmd<<"{"<<endl;
					fout_rmd<<"title <- paste(\"Node \",i)"<<endl;
					fout_rmd<<"boxplot(subset(net_mets_ko$mean_p_coef, net_mets_ko$node == i)~ subset(net_mets_ko$group, net_mets_ko$node == i),main=title)"<<endl;

					if  (! ( ( *i_n == "Nnodes" ) ||  ( *i_n == "Nedges" ) ||  ( *i_n == "cost" ) ||  ( *i_n == "cost_bin" ) ) )
					{

						fout_rmd<<"if ( i == 0 ){"<<endl;
						fout_rmd<<"tstats <- t.test(subset(net_mets_ko$"<<(*i_n)<<", net_mets_ko$node == i)~ subset(net_mets_ko$group, net_mets_ko$node == i), paired=FALSE)$statistic"<<endl;
						fout_rmd<<"pvals <- t.test(subset(net_mets_ko$"<<(*i_n)<<", net_mets_ko$node == i)~ subset(net_mets_ko$group, net_mets_ko$node == i), paired=FALSE)$p.value"<<endl;
						fout_rmd<<"nodes <- i"<<endl;
						fout_rmd<<"}else{"<<endl;
						fout_rmd<<"tstats <- c(tstats,t.test(subset(net_mets_ko$"<<(*i_n)<<", net_mets_ko$node == i)~ subset(net_mets_ko$group, net_mets_ko$node == i), paired=FALSE)$statistic)"<<endl;
						fout_rmd<<"pvals <- c(pvals,t.test(subset(net_mets_ko$"<<(*i_n)<<", net_mets_ko$node == i)~ subset(net_mets_ko$group, net_mets_ko$node == i), paired=FALSE)$p.value)"<<endl;

						fout_rmd<<"nodes <- c(nodes,i)"<<endl;

						fout_rmd<<"}"<<endl;

					}
					fout_rmd<<"}"<<endl;
					if  (! ( ( *i_n == "Nnodes" ) ||  ( *i_n == "Nedges" ) ||  ( *i_n == "cost" ) ||  ( *i_n == "cost_bin" ) ) )
					{
						fout_rmd<<"barplot(tstats,main=\"t-statistic\", names.arg=nodes)"<<endl;
						fout_rmd<<"barplot(pvals,main=\"p-value\", names.arg=nodes)"<<endl;
					}
					fout_rmd<<"```"<<endl;



				}

			}
		}










		fout_rmd.close();
	}


	return 0;
}
int main (int argc,  char * argv[])
{
	//    if (argc < 2)
	//    {
	//        usage();
	//        return 0;
	//    }
	//    string inname(argv[1]);
	//

	/* initialize random seed: */
	srand (time(NULL));


	Tracer tr("main");
	OptionParser options(title, examples);

	try {
		// must include all wanted options here (the order determines how
		//  the help message is printed)
		options.add(inname);
		options.add(maskname);
		options.add(labelname);
		options.add(clustername);
		options.add(clust_name_map);
		options.add(thresh);
		options.add(doKnockOut);
		options.add(doBinarize_in);
		options.add(gmeth);
		options.add(swap_index_in);
		//		options.add(swap_group_in);
		options.add(outname);
		options.add(subjectname);
		options.add(groupsname);
		options.add(permutations);
		options.add(verbose);
		options.add(directional);
		options.add(help);
		nonoptarg = options.parse_command_line(argc, argv);
		if (  (!options.check_compulsory_arguments(true) ))
		{
			options.usage();
			exit(EXIT_FAILURE);
		}

		do_work(inname.value(),maskname.value(),labelname.value(),subjectname.value(),groupsname.value(),clustername.value(),clust_name_map.value(),outname.value(),swap_index_in.value(),g_thresh_method(gmeth.value()),thresh.value(),permutations.value(),doKnockOut.value(),doBinarize_in.value(),directional.value(), verbose.value());


	} catch(X_OptionError& e) {
		options.usage();
		cerr << endl << e.what() << endl;
		exit(EXIT_FAILURE);
	} catch(std::exception &e) {
		cerr << e.what() << endl;
	}
	return 0;
}

