#!/bin/sh

function Usage()
{
    echo "\n etkin_graph_metric <conn_matrix_4D.nii.gz> <output> <subject_list>"
    echo "This function will produce network measures for each graph."
	echo "\n"
    exit 1
}

#Using BCT toolbox

BCT="/Volumes/Smurf-Village/SoftwareRepository/graph_theory/BCT/2014_04_05 BCT/"


if [ $# -lt 2 ]; then
Usage	
fi
CON_MAT=`imglob -extension $1`
shift 1
#COMM=$1
#shift 1
OUT=$1
shift 1
SUBS=$1
shift 1

#
#if [ -f $OUT ] ; then
#	valid=0
#while [ $valid = 0 ] ; do
#echo "File already exists, overwrite (y/n)?"
#read write
#if [ $write = n ] ; then
#exit
#elif [ $write = y ] ; then
#valid=1
#rm $OUT
#fi
#
#done
#fi
#

echo "Processing $CON_MAT ..."

#write matlab script
bOUT=`basename $OUT`
echo "Run script run_${bOUT}.m"

echo "addpath /Volumes/Smurf-Village/home/brianp/bitbucket/graph_theory" > run_${bOUT}.m
echo "etkin_graph_bct_metrics('$CON_MAT','${SUBS}','${OUT}')" >> run_${bOUT}.m
#echo "etkin_graph_bct_metrics('$CON_MAT','${SUBS}','${OUT}')" | matlab -nojvm 
echo run_${bOUT} | matlab -nojvm 