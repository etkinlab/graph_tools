//
//  main.cpp
//  interpret_motion_parameters
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include <string>
#include <set>
//FSL INCLUDE
#include "newimage/newimageall.h"
#include "utils/options.h"


using namespace NEWIMAGE;
using namespace Utilities;

using namespace std;

#include "etkin_graph_typedefs.h"
#include "network_metrics.h"
using namespace etkin_graph;


string title="etkin_matrix_mirror Stanford University (Brian Patenaude) \n \t This function is used to create masks for the connectviity matrices inorder to facility netowrk specific analysis";
string examples="etkin_matrix_mirror -i <connection_matrix.nii.gz> -o <output_base>";




Option<bool> verbose(string("-v,--verbose"), false,
                     string("switch on diagnostic messages"),
                     false, no_argument);
Option<bool> help(string("-h,--help"), false,
                  string("display this message"),
                  false, no_argument);


//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
					  string("Filename of list of groups"),
					  true, requires_argument);
					Option<float> val(string("-l,--l"), 1,
										  string("Value to set diagonal"),
										  true, requires_argument);

Option<bool> setdiag(string("-d,--setdiag"), false,
                  string("Set Diagonal To Network Label"),
                  false, no_argument);


Option<string> outname(string("-o,--output"), string(""),
					   string("Output name"),
					   true, requires_argument);
int nonoptarg;


int do_work( const string & inname ,const bool & setdiag_in,  const string & outname ){
   
    
    volume<float> matrix;
    read_volume(matrix,inname);
    
    int xsize=matrix.xsize();
    
    
    for (int x = 0 ; x < xsize ; ++x)
    {
                for (int y = x+1 ; y< xsize; ++y)
                {
		  matrix.value(y,x,0)=matrix.value(x,y,0);
		}
        
    }
    if (setdiag_in){
      for (int x = 0 ; x < xsize ; ++x)
	{//set diagoinal for each x

	  //look for non-zero in upper triangle
	      for (int y = 0 ; y< xsize; ++y)
		{
		  if ((matrix.value(x,y,0) != 0 ))
		    {
		      matrix.value(x,x,0) = matrix.value(x,y,0); 
		      break;
		    }

		  if ((matrix.value(y,x,0) != 0 ))
		    {
		      matrix.value(x,x,0) = matrix.value(y,x,0);
		      break;
		    }
		}
	    
	}

    }
    


    
    save_volume(matrix,outname);
  
    return 0;
}
int main (int argc,  char * argv[])
{
    //    if (argc < 2)
    //    {
    //        usage();
    //        return 0;
    //    }
    //    string inname(argv[1]);
    //
    
	Tracer tr("main");
	OptionParser options(title, examples);
	
	try {
		// must include all wanted options here (the order determines how
		//  the help message is printed)
		options.add(inname);
		options.add(setdiag);
		options.add(outname);
		options.add(verbose);
		options.add(help);
        
    	nonoptarg = options.parse_command_line(argc, argv);
        if (  (!options.check_compulsory_arguments(true) ))
		{
			options.usage();
			exit(EXIT_FAILURE);
		}
        
        do_work(inname.value(),setdiag.value(),outname.value());

    } catch(X_OptionError& e) {
		options.usage();
		cerr << endl << e.what() << endl;
		exit(EXIT_FAILURE);
	} catch(std::exception &e) {
		cerr << e.what() << endl;
	}
    return 0;
}

