//
//  main.cpp
//  interpret_motion_parameters
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include <string>
#include "newimage/newimageall.h"

using namespace NEWIMAGE;
using namespace std;



void usage(){
    
    cout<<"\n Usage : "<<endl;
    cout<<"etkin_graph_theory <correlations.nii.gz> <output_correlations> \n"<<endl;
    
}


int do_work( const string & inname  , const string & outname ){
    cout<<"inname "<<inname<<endl;
    volume4D<float> con_mats;
    read_volume4D(con_mats,inname);

    int nt = con_mats.tsize();
    int nx = con_mats.xsize();
    int ny = con_mats.ysize();
    int nz = con_mats.zsize();
    //nz shoud equal 1
    if (nz != 1 )
        return 1;
    vector< pair<float,float> > all_mean_var;
    
    for (int t = 0 ; t < nt ; ++t)
    {
        float sx=0;
        float sxx=0;
        //loop over upper traingle
        int N = 0 ;
        for ( int x = 0 ; x < nx ; ++x)
            for (int  y =x+1 ; y < ny ; ++y)
            {
                float val = con_mats[t].value(x,y,0);
                if (!isnan(val))
                {
                    sx+=val;
                    sxx+=val*val;
                    ++N;
                }else{
                    cout<<"found nan "<<t<<" : "<< x<<" "<<y<<endl;
                }
                
            }
        float mean = sx/N;
        float var = (sxx - sx*sx/N)/(N-1);
	all_mean_var.push_back( pair<float,float>(mean,var));
     cout<<"subject "<<t<<", mean/variance "<<mean<<" / "<<var<<endl;
    }

    float grand_mean = 0 ;
    float mean_var = 0 ; 
    for (vector< pair<float,float> >::iterator i = all_mean_var.begin(); i != all_mean_var.end(); ++i)
      {
	cout<<"mean/var" << i->first<<" "<<i->second<<endl;
	grand_mean+=i->first;
	mean_var += i->second;
      }
    grand_mean /= all_mean_var.size();
    mean_var /= all_mean_var.size();

    cout<<"grand mean/var "<<grand_mean<<" "<<mean_var<<endl;
    //apply to data 
    if (!outname.empty())
      {
    vector< pair<float,float> >::iterator i_mv  = all_mean_var.begin();
    for (int t = 0 ; t < nt ; ++t,++i_mv)
      {
	
	
	for ( int x = 0 ; x < nx ; ++x)
	  for (int  y =x+1 ; y < ny ; ++y)
            {
	      //	      cout<<"xy "<<x<<" "<<y<<endl;
	      con_mats[t].value(x,y,0) -= i_mv->first;
              con_mats[t].value(x,y,0) /= sqrt( i_mv->second );
              con_mats[t].value(x,y,0) *= sqrt( mean_var );

	      con_mats[t].value(x,y,0) += grand_mean ;

	      con_mats[t].value(y,x,0) = con_mats[t].value(x,y,0);
	      //      float val = con_mats[t].value(x,y,0);
	      //if (!isnan(val))
	      // {
	      //  sx+=val;
	      //  sxx+=val*val;
	      //  ++N;
	      //}else{
	      //	cout<<"found nan "<<t<<" : "<< x<<" "<<y<<endl;
	      //}
	    }
      }
    save_volume4D(con_mats,outname);
      }
return 0;
}
int main (int argc, const char * argv[])
{
    if (argc < 2)
    {
        usage();
        return 0;
    }
    string inname(argv[1]);
    string outname;
    cout<<"ragc "<<argc<<endl;
    if (argc==3)
      {
	outname = string(argv[2]);
      }
    //    string outname(argv[2]);
    
    do_work(inname,outname);
    
    return 0;
}

