//
//  circos_conf_create.cpp
//  circos_conf_create
//
//  Created by Brian Patenaude on 05/17/15.
//  Copyright 2015 __Stanford University__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include <string>
#include <set>
//FSL INCLUDE
#include "newimage/newimageall.h"
#include "utils/options.h"
#include "fslsurface/fslsurface.h"
#include "fslsurface/fslsurfacefns.h"

#include <boost/filesystem.hpp>



using namespace NEWIMAGE;
using namespace Utilities;

using namespace std;
//using namespace etkin_graph;
using namespace fslsurface_name;
namespace fs = boost::filesystem;
string title="circos_conf_create Stanford University (Brian Patenaude)";
string examples="circos_conf_create -i <connections_4D_gr0.nii.gz,connections_4D_gr1> -o <output_base>";




Option<bool> verbose(string("-v,--verbose"), false,
                     string("switch on diagnostic messages"),
                     false, no_argument);
Option<bool> help(string("-h,--help"), false,
                  string("display this message"),
                  false, no_argument);
//Option<bool> inter_only(string("--inter_only,--help"), false,
//                  string("only include inter-network links"),
//                  false, no_argument);
//
//Option<bool> intra_only(string("--intra_only,--help"), false,
//                        string("only include intra-network links"),
//                        false, no_argument);

//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
                      string("Filename of input image correlation matrix (i.e. connection strengths. If two comma separated images given, difference will be computed."),
                      true, requires_argument);
Option<string> inname_csv(string("-c,--incsv"), string(""),
                      string("Filename of node metric values "),
                      false, requires_argument);
Option<float> thresh(string("-t,--thresh"), 0,
                          string("Threshold links"),
                          true, requires_argument);
Option<float> rwidth(string("-r,--rwidth"), 0.05,
                     string("Radius width for csv data."),
                     false, requires_argument);
Option<int> label_size(string("-l,--labelsize"), 48,
                     string("Size of Labels."),
                     false, requires_argument);
Option<float> ideogram_radius(string("-s,--radius"), 0.80,
                     string("Size of circle as fraction of total radius "),
                     false, requires_argument);
Option<float> maximum(string("-n,--maximum"), 0,
        string("Maximum value used for rendering (positive only)"),
        false, requires_argument);
Option<float> minimum(string("-m,--minimum"), 0,
        string("Minimum value used for rendering  (positive only)"),
        false, requires_argument);
//Option<string> ticksPerBand(string("-t,--t"), 1,
//                      string("Number of ticks found in each band"),
//                      true, requires_argument);
Option<string> karyoname(string("-k,--kin"), string(""),
                         string("Filename karyotype."),
                         true, requires_argument);
Option<string> outname(string("-o,--output"), string(""),
                       string("Output name"),
                       true, requires_argument);


int nonoptarg;



string num2str( const float & val )
{
    stringstream ss;
    ss<<val;
    string snum;
    ss>>snum;
    return snum;
    
}


set<int> getUniqueLabels(const volume<int> & mask)
{
    set<int> labels;
    
    const int* m_ptr = mask.fbegin();
    for ( unsigned long int i =0 ; i < mask.nvoxels(); ++i)
    {
        if (*m_ptr != 0 )
            labels.insert(*m_ptr);
    }
    
    return labels;
}


void mapValueToColour(const float & min, const float & max, const float & val, int & r,  int & g, int & b)
{
    
    vector<float> v_R(6,0), v_G(6,0),v_B(6,0), v_sc(6,0);
    
    v_R[0]=255; v_R[1]=255; v_R[2]=255; v_R[3]=255; v_R[4]=200; v_R[5]=130;
    v_G[0]=0; v_G[1]=255; v_G[2]=255; v_G[3]=0; v_G[4]=110; v_G[5]=217;
    v_B[0]=0; v_B[1]=180; v_B[2]=0; v_B[3]=255; v_B[4]=250; v_B[5]=240;
    
    float step = (max - min)/5;
    float t = (val-min)/step;
    cout<<"t "<<t<<endl;
    int step_a = floor(t);
    float step_b = t-step_a;
    cout<<"map "<<min<<" "<<max<<" "<<val<<" "<<step<<" "<<step_a<<" "<<step_b<<endl;
    r = static_cast<int>(std::round(v_R[step_a] + step_b *  (  v_R[step_a+1] - v_R[step_a] )));
    g = static_cast<int>(std::round(v_G[step_a] + step_b *  (  v_G[step_a+1] - v_G[step_a] )));
    b = static_cast<int>(std::round(v_B[step_a] + step_b *  (  v_B[step_a+1] - v_B[step_a] )));
    
}




void mapValueToColour2sided(const float & min, const float & max, const float & val_signed, int & r,  int & g, int & b)
{
    //min/max  should be positive
	//uses absoluate value
    vector<float> v_R(6,0), v_G(6,0),v_B(6,0), v_sc(6,0);
    //0-2 positive , 3-5 negative
    //  v_R[0]=255; v_R[1]=255; v_R[2]=255; v_R[3]=0; v_R[4]=76; v_R[5]=152;
    //  v_G[0]=0; v_G[1]=127.5; v_G[2]=255; v_G[3]=0; v_G[4]=127.5; v_G[5]=255;
    //  v_B[0]=0; v_B[1]=0; v_B[2]=0; v_B[3]=255; v_B[4]=255; v_B[5]=255;
    
    v_R[0]=221; v_R[1]=244; v_R[2]=180; v_R[3]=221; v_R[4]=141; v_R[5]=59;
    v_G[0]=221; v_G[1]=154 ; v_G[2]=4; v_G[3]=221; v_G[4]=176; v_G[5]=76;
    v_B[0]=221; v_B[1]=123; v_B[2]=38; v_B[3]=221; v_B[4]=254; v_B[5]=192;
    
    
    float step = (max - min)/2;//3 points, 2 sections
    int c_offset=0;
    float val = val_signed;
    //  cout<<"val "<<val_signed<<endl;
    if (val_signed < 0)
    {
        c_offset=3;
        val=abs(val);
    }
    
    float t = (val-min)/step;
    cout<<"t "<<t<<endl;
    int step_a = floor(t);
    float step_b = t-step_a;
    step_a += c_offset;
    
    //cout<<"map "<<min<<" "<<max<<" "<<val<<" "<<step<<" "<<step_a<<" "<<step_b<<endl;
    r = static_cast<int>(std::round(v_R[step_a] + step_b *  (  v_R[step_a+1] - v_R[step_a] )));
    g = static_cast<int>(std::round(v_G[step_a] + step_b *  (  v_G[step_a+1] - v_G[step_a] )));
    b = static_cast<int>(std::round(v_B[step_a] + step_b *  (  v_B[step_a+1] - v_B[step_a] )));
    
}


void writeMapValueToColour2sided(const string & filename, const float & min, const float & max )
{
    //min/max  should be positive
	//uses absoluate value
	ofstream fmap(filename.c_str());
	if (fmap.is_open())
	{
		int r,g,b;
		//min/max  should be positive
		float value=-max;
		mapValueToColour2sided(min, max,value, r,  g,  b);
		fmap<<value<<" "<<static_cast<float>(r)/255<<" "<<static_cast<float>(g)/255<<" "<<static_cast<float>(b)/255<<endl;

		value=(min-max)/2;
		mapValueToColour2sided(min, max,value , r,  g,  b);
		fmap<<value<<" "<<static_cast<float>(r)/255<<" "<<static_cast<float>(g)/255<<" "<<static_cast<float>(b)/255<<endl;

		value=-min;
		mapValueToColour2sided(min, max,value , r,  g,  b);
		fmap<<value<<" "<<static_cast<float>(r)/255<<" "<<static_cast<float>(g)/255<<" "<<static_cast<float>(b)/255<<endl;

		value=min;
		mapValueToColour2sided(min, max,value, r,  g,  b);
		fmap<<value<<" "<<static_cast<float>(r)/255<<" "<<static_cast<float>(g)/255<<" "<<static_cast<float>(b)/255<<endl;

		value=(max-min)/2;
		mapValueToColour2sided(min, max,value , r,  g,  b);
		fmap<<value<<" "<<static_cast<float>(r)/255<<" "<<static_cast<float>(g)/255<<" "<<static_cast<float>(b)/255<<endl;

		value=max;
		mapValueToColour2sided(min, max,value , r,  g,  b);
		fmap<<value<<" "<<static_cast<float>(r)/255<<" "<<static_cast<float>(g)/255<<" "<<static_cast<float>(b)/255<<endl;

		fmap.close();
	}else{

		cerr<<"Failed to open file "<<filename<<endl;
	}

}

template<class T>
vector<T> parse_csv( const string & csv_in ) //want a copy, do not want original
{
    vector<T> vline;
    string csv = csv_in;
    int pos=0;
    while ( pos  != -1 ) {
        pos=csv.find(',',pos+1);
        if (pos == -1)
            break;
        csv.replace(pos,1," ");
    }
    stringstream ss;
    //ss.flags( ios::skipws );
    //cout<<"sline "<<sline<<endl;
    ss << csv;
    
    
    //  cout<<"push back"<<endl;
    T sval;
    while ( ss>>sval )
    {
        //    cout<<sval<<" ";
        vline.push_back(sval);
    }
    //cout<<endl;
    
    
    return vline;
    
}


void copyIdeogram( const fs::path & outname, const fs::path & dest)
{

    fs::copy_file(outname.string()+"/ideogram.conf", dest.string()+"/ideogram.conf",fs::copy_option::overwrite_if_exists);
    fs::copy_file(outname.string()+"/ideogram.position.conf", dest.string()+"/ideogram.position.conf",fs::copy_option::overwrite_if_exists);
    fs::copy_file(outname.string()+"/ideogram.label.conf", dest.string()+"/ideogram.label.conf",fs::copy_option::overwrite_if_exists);
    fs::copy_file(outname.string()+"/bands.conf", dest.string()+"/bands.conf",fs::copy_option::overwrite_if_exists);

}



void writeIdeogram( const string & outname , const int & label_size,const float & ideogram_radius, int br = 2)
{
    
    ofstream fout((outname+"/ideogram.conf").c_str());
    if (fout.is_open())
    {
        fout<<"<ideogram>"<<endl;
        
        fout<<"<spacing>"<<endl;
        fout<<"default = 0.01r"<<endl;
        fout<<"break   = "<<br<<"u"<<endl;
        fout<<"</spacing>"<<endl;
        
        fout<<"<<include ideogram.position.conf>>"<<endl;
        fout<<"<<include ideogram.label.conf>>"<<endl;
        fout<<"<<include bands.conf>>"<<endl;
        
        fout<<"</ideogram>"<<endl;
        fout.close();
    }
    
    
    ofstream fpos((outname+"/ideogram.position.conf").c_str());
    if (fpos.is_open())
    {
        fpos<<"radius           = "<<ideogram_radius<<"r"<<endl;
        fpos<<"thickness        = 50p"<<endl;
        fpos<<"fill             = yes"<<endl;
        fpos<<"fill_color       = black"<<endl;
        fpos<<"stroke_thickness = 2"<<endl;
        fpos<<"stroke_color     = black"<<endl;
        fpos.close();
    }

    ofstream flb((outname+"/ideogram.label.conf").c_str());
    if (flb.is_open())
    {
        flb<<"label_radius   = 1.35*dims(ideogram,radius_outer)"<<endl;
        flb<<"label_center   = yes"<<endl;
        flb<<"label_size     = "<<label_size<<endl;
        flb<<"label_with_tag = yes"<<endl;
        flb<<"label_parallel = yes"<<endl;
        flb<<"label_case     = upper"<<endl;
        flb.close();
    }
    
    ofstream fbands((outname+"/bands.conf").c_str());
    if (fbands.is_open())
    {
        fbands<<"show_bands            = yes"<<endl;
        fbands<<"fill_bands            = yes"<<endl;
        fbands<<"band_stroke_thickness = 1"<<endl;
        fbands<<"band_stroke_color     = white"<<endl;
        fbands<<"band_transparency     = 5"<<endl;
       fbands.close();
    }
    
}
void writeKaryotype(const string & outname, const vector<string> & chroms, const vector< vector<string> > & bands, const unsigned int & ticksPerBand)
{
    
    cout<<"write kayo "<<chroms.size()<<" "<<bands.size()<<endl;
    ofstream fout(outname.c_str());

    
    vector<string> colors;
    colors.push_back("green");
    colors.push_back("red");
    colors.push_back("blue");
    colors.push_back("yellow");
    colors.push_back("purple");
    colors.push_back("orange");

    vector<string> b_color;
    b_color.push_back("black");
    b_color.push_back("white");
    
    
    
    if (fout.is_open())
    {
        
        //write chromosome
        {
            vector<string>::const_iterator i_c = colors.begin();
            vector< vector<string> >::const_iterator i_b = bands.begin();
            for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i,++i_b,++i_c)
            {
                fout<<"chr - "<<(*i)<<" "<<(*i)<<" "<<0<<" "<<(i_b->size() * ticksPerBand)<<" "<<(*i_c)<<endl;
            }
        }
        
        {//write abnds
            vector< vector<string> >::const_iterator i_b = bands.begin();
            for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i,++i_b)
            {
                int index=0;
                int color_index=0;
                for (vector<string>::const_iterator ii_b  = i_b->begin(); ii_b != i_b->end(); ++ii_b, index+=ticksPerBand,++color_index)
                {
                    fout<<"band "<<(*i)<<" "<<(*ii_b)<<" "<<(*ii_b)<<" "<<index<<" "<<(index+ticksPerBand)<<" "<<b_color[color_index%2]<<endl;
                    
                }
            }
        }
        
        
        
        
        fout.close();
    }
    cout<<"end - write kayo "<<endl;

}

void writeKaryotypeByChrom(const string & outname, const vector<string> & chroms, const vector< vector<string> > & bands, const unsigned int & ticksPerBand)
{
    
    cout<<"write kayo "<<chroms.size()<<" "<<bands.size()<<endl;
//    ofstream fout(outname.c_str());
    
    
    vector<string> colors;
    colors.push_back("green");
    colors.push_back("red");
    colors.push_back("blue");
    colors.push_back("yellow");
    colors.push_back("purple");
    colors.push_back("orange");
    
    vector<string> b_color;
    b_color.push_back("black");
    b_color.push_back("white");
    vector<string>::iterator i_c = colors.begin();
    vector< vector<string> >::const_iterator i_b = bands.begin();
    for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i,++i_c,++i_b)
    {
        cout<<"chroms "<<(*i)<<endl;
        
        fs::path dir_chr(outname + "_" + (*i));
        if (! fs::is_directory(dir_chr))
            if( ! create_directory(dir_chr))//if not succesful
            {
                cerr<<"Failed to open directory : "<<dir_chr<<endl;
                exit (EXIT_FAILURE);
            }
        
        fs::path karyo(dir_chr.string() + "/karyotype.txt");
        ofstream fout(karyo.string().c_str());
        if (fout.is_open())
        {
            fout<<"chr - "<<(*i)<<" "<<(*i)<<" "<<0<<" "<<(i_b->size() * ticksPerBand)<<" "<<(*i_c)<<endl;
            
//            {//write abnds
//                vector< vector<string> >::const_iterator i_b = bands.begin();
//                for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i,++i_b)
//                {
                    int index=0;
                    int color_index=0;
                    for (vector<string>::const_iterator ii_b  = i_b->begin(); ii_b != i_b->end(); ++ii_b, index+=ticksPerBand,++color_index)
                    {
                        fout<<"band "<<(*i)<<" "<<(*ii_b)<<" "<<(*ii_b)<<" "<<index<<" "<<(index+ticksPerBand)<<" "<<b_color[color_index%2]<<endl;
                        
                    }
//                }
//            }

            fout.close();
        }
        
        
    }
    
    
}



void writeNodeLabels(const string & outname , const vector<string> & chroms, const vector< vector<string> > & bands, const unsigned int & ticksPerBand,  const vector<string> & labels )
{
    ofstream fout(outname.c_str());
    
    if (fout.is_open())
    {
        {//write abnds
            vector< vector<string> >::const_iterator i_b = bands.begin();
          
            vector<string>::const_iterator i_lb = labels.begin();
            for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i,++i_b)
            {
                int index=0;
                for (vector<string>::const_iterator ii_b  = i_b->begin(); ii_b != i_b->end(); ++ii_b, index+=ticksPerBand,++i_lb)
                {
                    fout<<(*i)<<" "<<index<<" "<<(index+ticksPerBand)<<" "<<(*i_lb)<<endl;
                    
                }
            }
        }
        
        fout.close();
    }
    
}

void writeNodeLabelsByChrom(const string & outname , const vector<string> & chroms, const vector< vector<string> > & bands, const unsigned int & ticksPerBand,  const vector<string> & labels )
{
    vector< vector<string> >::const_iterator i_b = bands.begin();
    vector<string>::const_iterator i_lb = labels.begin();
    for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i,++i_b)
    {
        cout<<"chroms "<<(*i)<<endl;
        
        fs::path dir_chr(outname + "_" + (*i));
        if (! fs::is_directory(dir_chr))
            if( ! create_directory(dir_chr))//if not succesful
            {
                cerr<<"Failed to open directory : "<<dir_chr<<endl;
                exit (EXIT_FAILURE);
            }
        
        fs::path dir_lb(dir_chr.string() + "/node_labels.txt");
        cout<<"dirlb "<<dir_lb.string()<<endl;
        
        ofstream fout(dir_lb.string().c_str());
        
        if (fout.is_open())
        {
            cout<<"write flabel"<<endl;
           
                
                    int index=0;
                    for (vector<string>::const_iterator ii_b  = i_b->begin(); ii_b != i_b->end(); ++ii_b, index+=ticksPerBand,++i_lb)
                    {
                        cout<<"write labe "<<endl;
                        fout<<(*i)<<" "<<index<<" "<<(index+ticksPerBand)<<" "<<(*i_lb)<<endl;
                        
                    }
            
            fout.close();
        }
    }
}

void writeCircosConfByChrom(const string & outname, const string & kname , const vector<string> & csv_filenames, const vector<string> & chroms, const vector< vector<string> > & bands , const float & linkrad, const int & label_size)
{
    vector< vector<string> >::const_iterator i_b = bands.begin();

    for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i,++i_b)
    {
        cout<<"chroms "<<(*i)<<endl;
        
        fs::path dir_chr(outname + "_" + (*i));
        if (! fs::is_directory(dir_chr))
            if( ! create_directory(dir_chr))//if not succesful
            {
                cerr<<"Failed to open directory : "<<dir_chr<<endl;
                exit (EXIT_FAILURE);
            }
        
        fs::path circo(dir_chr.string() + "/circos.conf");
        string full_k = dir_chr.string() + "/" + kname;
        ofstream fout(circo.string().c_str());
        
        if (fout.is_open())
        {
            fout<<"<<include etc/colors_fonts_patterns.conf>>"<<endl;
            fout<<"<<include ideogram.conf>>"<<endl;
            fout<<"<image>"<<endl;
            fout<<"<<include etc/image.conf>>"<<endl;
            //        fout<<"file = "
            fout<<"</image>"<<endl;
            fout<<endl<<"karyotype = "<<full_k<<endl;
            fout<<endl<<"chromosomes_units           = 1"<<endl;
            fout<<"chromosomes_display_default = no"<<endl;
            fout<<"chromosomes                 = "<<(*i)<<";"<<endl;
            //        for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i)
            //            fout<<(*i)<<";";
            fout<<endl;
            fout<<endl<<"<plots>"<<endl;
            fout<<endl<<"<plot>"<<endl;
            fout<<"type             = text"<<endl;
            fout<<"color            = black"<<endl;
            fout<<"file             = node_labels.txt"<<endl;
            fout<<endl<<"r0 = 1.05r"<<endl;
            fout<<"r1 = 1.5r"<<endl;
            fout<<endl<<"label_size   = "<<label_size<<"p"<<endl;
            fout<<"label_font   = condensed"<<endl;
            fout<<endl<<"padding  = 0p"<<endl;
            fout<<"rpadding = 0p"<<endl;
            fout<<endl<<"</plot>"<<endl;
            
            if (!csv_filenames.empty())
            {
                fout<<"<<include csv_plots.conf>>"<<endl;
                
            }
            
            fout<<endl<<"</plots>"<<endl;
            
            fout<<endl<<"<links> "<<endl;
            fout<<endl<<"<link> "<<endl;
            fout<<"file          = links.txt "<<endl;
            fout<<"ribbon           = no "<<endl;
            fout<<"color            = black "<<endl;
            fout<<"thickness        = 20 "<<endl;
            fout<<"radius           = "<<linkrad<<"*dims(ideogram,radius_inner);"<<endl;
            fout<<"bezier_radius    = 0r"<<endl;
            fout<<"crest                = 0.5"<<endl;
            fout<<"bezier_radius_purity = 0.75"<<endl;
            fout<<endl<<"</link>"<<endl;
            fout<<endl<<"</links>"<<endl;
            fout<<endl<<"<<include etc/housekeeping.conf>>"<<endl;
            
            fout.close();
        }
    }
}


void writeCircosConf(const string & outname, const string & kname , const vector<string> & chroms, const vector< vector<string> > & bands , const float & linkrad , const int & label_size, const vector<string> & csv_filenames)
{
    ofstream fout(outname.c_str());
    
    if (fout.is_open())
    {
        fout<<"<<include etc/colors_fonts_patterns.conf>>"<<endl;
        fout<<"<<include ideogram.conf>>"<<endl;
        fout<<"<image>"<<endl;
        fout<<"<<include etc/image.conf>>"<<endl;
        //        fout<<"file = "
        fout<<"</image>"<<endl;
        fout<<endl<<"karyotype = "<<kname<<endl;
        fout<<endl<<"chromosomes_units           = 1"<<endl;
        fout<<"chromosomes_display_default = no"<<endl;
        fout<<"chromosomes                 = ";
        for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i)
            fout<<(*i)<<";";
        fout<<endl;
        fout<<endl<<"<plots>"<<endl;
        fout<<endl<<"<plot>"<<endl;
        fout<<"type             = text"<<endl;
        fout<<"color            = black"<<endl;
        fout<<"file             = node_labels.txt"<<endl;
        fout<<endl<<"r0 = 1.05r"<<endl;
        fout<<"r1 = 1.5r"<<endl;
        fout<<endl<<"label_size   = "<<label_size<<"p"<<endl;
        fout<<"label_font   = condensed"<<endl;
        fout<<endl<<"padding  = 0p"<<endl;
        fout<<"rpadding = 0p"<<endl;
        fout<<endl<<"</plot>"<<endl;
        
        if (!csv_filenames.empty())
        {
            fout<<"<<include csv_plots.conf>>"<<endl;
            
        }
        fout<<endl<<"</plots>"<<endl;
        
        fout<<endl<<"<links> "<<endl;
        fout<<endl<<"<link> "<<endl;
        fout<<"file          = links.txt "<<endl;
        fout<<"ribbon           = no "<<endl;
        fout<<"color            = black "<<endl;
        fout<<"thickness        = 20 "<<endl;
        fout<<"radius           = "<<linkrad<<"*dims(ideogram,radius_inner);"<<endl;
        fout<<"bezier_radius    = 0r"<<endl;
        fout<<"crest                = 0.5"<<endl;
        fout<<"bezier_radius_purity = 0.75"<<endl;
        fout<<endl<<"</link>"<<endl;
        fout<<endl<<"</links>"<<endl;
        fout<<endl<<"<<include etc/housekeeping.conf>>"<<endl;
        
        fout.close();
    }
}


//void writeLinks(const string & inname, const string & outname, const  vector< pair<string,unsigned int> > & id_block, const unsigned int & ticksPerBand, bool rib = false){
void writeLinksByChrom(const volume<float> & connectivity_matrix, const string & outname, const vector<string> & chroms, const  vector< pair<string,unsigned int> > & id_block, const unsigned int & ticksPerBand, const float & min, const float & max, const float & thresh_in, bool inter_only_in=false,bool intra_only_in=false, bool rib = false){
    
    //    volume<float> connectivity_matrix;
    //    read_volume(connectivity_matrix,inname);
    
    int N = connectivity_matrix.xsize();
 
    
//    vector< vector<string> >::const_iterator i_b = bands.begin();
    
    for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i)//,++i_b)
    {
        cout<<"chroms "<<(*i)<<endl;
        
        fs::path dir_chr(outname + "_" + (*i));
        if (! fs::is_directory(dir_chr))
            if( ! create_directory(dir_chr))//if not succesful
            {
                cerr<<"Failed to open directory : "<<dir_chr<<endl;
                exit (EXIT_FAILURE);
            }
        
        fs::path plink(dir_chr.string() + "/links.txt");
        ofstream flinks(plink.string().c_str());
        
        if (! flinks.is_open())
        {
            cerr<<"Could not open "<<outname<<endl;
            exit (EXIT_FAILURE);
        }
        //    flinks<<"<<include colors_fonts_patterns.conf>>"<<endl<<"<colors>"<<endl<<"blackweak = 0,0,0,100"<<endl<<"</colors>"<<endl;
        
        string ribbon= (rib == true ) ? "yes" : "no" ;
        
        //let's do binary connectivity
        for (int x = 0 ; x < N ; ++x)
            for (int y = x+1 ; y < N ; ++y)
            {
//                cout<<"xy "<<x<<" "<<y<<endl;
                if ( connectivity_matrix.value(x,y,0) != 0 )
                {
                    
//                    if ( ( (inter_only_in) && (id_block[x].first != id_block[y].first) )  ||
//                        ( (intra_only_in) && (id_block[x].first == id_block[y].first) ) ||
 //                       ((!intra_only_in) && (!inter_only_in))                       )
                        
                    if ((id_block[x].first == *i ) && (id_block[y].first == *i ))
                    {
                        float val = connectivity_matrix.value(x,y,0);
                        //if ((x==0)&&(y==1)) val=2;
                        if ( abs(val) > thresh_in  ){
                        
                        
                        int r,g,b;
                        //                mapValueToColour(min,max,val,r,g,b);
                        mapValueToColour2sided(min,max,val,r,g,b);
                        //		cout<<"rgb "<<r<<" "<<g<<" "<<b<<endl;
                        float thick = 1+ abs(val/max*20);
                        float alpha = 0;
                        
                        int z = std::round(abs(val/max*100));
//                        cout<<"link "<<id_block.size()<<" "<<x<<" "<<y<<endl;
                        flinks<<id_block[x].first<<" "<<id_block[x].second <<" "<< (id_block[x].second+ticksPerBand)<<" "<<id_block[y].first<<" "<<id_block[y].second <<" "<< (id_block[y].second+ticksPerBand)<<" bezier_radius=0.1r,thickness="<<thick<<",ribbon="<<ribbon<<",color=("<<num2str(r)<<","<<num2str(g)<<","<<num2str(b)<<","<<alpha<<"),z="<<z<<endl;
                        }
                    
                    }
                }
                
                
            }
        
        
        flinks.close();
    }
    cout<<"done links by chrom"<<endl;
}
void writeLinks(const volume<float> & connectivity_matrix, const string & outname, const  vector< pair<string,unsigned int> > & id_block, const unsigned int & ticksPerBand, const float & min, const float & max,  const float & thresh_in, bool inter_only_in=false,bool intra_only_in=false, bool rib = false){
    
    //    volume<float> connectivity_matrix;
    //    read_volume(connectivity_matrix,inname);
    
    int N = connectivity_matrix.xsize();
    
    ofstream flinks(outname.c_str());
    if (! flinks.is_open())
    {
        cerr<<"Could not open "<<outname<<endl;
        exit (EXIT_FAILURE);
    }
    //    flinks<<"<<include colors_fonts_patterns.conf>>"<<endl<<"<colors>"<<endl<<"blackweak = 0,0,0,100"<<endl<<"</colors>"<<endl;
    
    string ribbon= (rib == true ) ? "yes" : "no" ;
    cout<<"MODE "<<inter_only_in<<" "<<intra_only_in<<endl;
    //let's do binary connectivity
    for (int x = 0 ; x < N ; ++x)
        for (int y = x+1 ; y < N ; ++y)
        {
//            cout<<"xy "<<x<<" "<<y<<endl;
            if ( connectivity_matrix.value(x,y,0) != 0 )
            {
                
                if ( ( (inter_only_in) && (id_block[x].first != id_block[y].first) )  || \
                    ( (intra_only_in) && (id_block[x].first == id_block[y].first) ) || \
                    ((!intra_only_in) && (!inter_only_in))                       )
                    
                    
                {
                    float val = connectivity_matrix.value(x,y,0);
                    if ( abs(val) > thresh_in  ){
                    
                        //if ((x==0)&&(y==1)) val=2;
                        int r,g,b;
                        //                mapValueToColour(min,max,val,r,g,b);
                        mapValueToColour2sided(min,max,val,r,g,b);
                        //		cout<<"rgb "<<r<<" "<<g<<" "<<b<<endl;
                        float thick = 1+ abs(val/max*20);
                        float alpha = 0;
                        
                        int z = std::round(abs(val/max*100));
//                        cout<<"link "<<id_block.size()<<" "<<x<<" "<<y<<endl;
                        flinks<<id_block[x].first<<" "<<id_block[x].second <<" "<< (id_block[x].second+ticksPerBand)<<" "<<id_block[y].first<<" "<<id_block[y].second <<" "<< (id_block[y].second+ticksPerBand)<<" bezier_radius=0.1r,thickness="<<thick<<",ribbon="<<ribbon<<",color=("<<num2str(r)<<","<<num2str(g)<<","<<num2str(b)<<","<<alpha<<"),z="<<z<<endl;
                    }
                    
                }
            }
            
            
        }
    
    
    flinks.close();
    
}

void createDirectoryStructure(const string & outname)
{
    
    fs::path dir(outname);
    fs::path dir_full(outname+ "/full");
    fs::path dir_inter(outname+ "/inter");
    fs::path dir_intra(outname+ "/intra");
    fs::path dir_data(outname+ "/full/data");
    fs::path dir_inter_data(outname+ "/inter/data");
    fs::path dir_intra_data(outname+ "/intra/data");

    //    create_directory(dir);
    ////    boost::filesystem::path dir(outname);
    cout<<"Creating output directory structure..."<<endl;
    if (! fs::is_directory(dir)) //iuf directory doesn't exist create,
    {
        cout<<"is not a directory "<<endl;
        if( ! create_directory(dir))//if succesful
        {
            cerr<<"Failed to open directory : "<<dir<<endl;
            exit (EXIT_FAILURE);
        }
    }
    cout<<dir.string()<<endl;
    
    
    //create subdirectories
    if (! fs::is_directory(dir_full))
        if( ! create_directory(dir_full))//if not succesful
        {
            cerr<<"Failed to open directory : "<<dir_full<<endl;
            exit (EXIT_FAILURE);
        }
    cout<<"./"<<dir_full.filename().string()<<endl;
    
    if (! fs::is_directory(dir_inter))
        if( ! create_directory(dir_inter))//if not succesful
        {
            cerr<<"Failed to open directory : "<<dir_inter<<endl;
            exit (EXIT_FAILURE);
        }
    cout<<"./"<<dir_inter.filename().string()<<endl;
    
    if (! fs::is_directory(dir_intra))
        if( ! create_directory(dir_intra))//if not succesful
        {
            cerr<<"Failed to open directory : "<<dir_intra<<endl;
            exit (EXIT_FAILURE);
        }
    cout<<"./"<<dir_intra.filename().string()<<endl;
    
    if (! fs::is_directory(dir_data))
        if( ! create_directory(dir_data))//if not succesful
        {
            cerr<<"Failed to open directory : "<<dir_data<<endl;
            exit (EXIT_FAILURE);
        }
    cout<<"./"<<dir_inter_data.filename().string()<<endl;
    
    if (! fs::is_directory(dir_inter_data))
        if( ! create_directory(dir_inter_data))//if not succesful
        {
            cerr<<"Failed to open directory : "<<dir_inter_data<<endl;
            exit (EXIT_FAILURE);
        }
    cout<<"./"<<dir_inter_data.filename().string()<<endl;
    
    if (! fs::is_directory(dir_intra_data))
        if( ! create_directory(dir_intra_data))//if not succesful
        {
            cerr<<"Failed to open directory : "<<dir_intra_data<<endl;
            exit (EXIT_FAILURE);
        }
    cout<<"./"<<dir_intra_data.filename().string()<<endl;
    
    
    
    

}

//convert unisgned int to string
string uint2str( const unsigned int & val )
{
    string sval;
    stringstream ss;
    ss<<val;
    ss>>sval;
    return sval;
}

//this function writes out data files from csv into circos compatibale formats
//As such it can create concentric rings of data
//The radius returned is to no where histograms end
int writeDataFilesForPlots(const vector<string> & inname_csv, const string & outname,  vector<string> & filenames ,const vector< pair<string,unsigned int> > & bands, const unsigned int &  ticksPerBand, const float & radius_width , float & radius_link )
{
    
    cout<<"writeDataFilesForPlots "<<endl;
    ofstream fout_plot((outname+"/csv_plots.conf").c_str());
    vector<string> colors(2);
    colors[0]="red";
    colors[1]="blue";
    unsigned int group=0;
    for (vector<string>::const_iterator i_csv = inname_csv.begin(); i_csv != inname_csv.end(); ++i_csv,++group)
    {
        vector<string> fnames_local;
        
        fs::path p_csv(*i_csv);
        if (! fs::exists(p_csv))
        {
            cerr<<"File does not exist: "<<p_csv.string();
            return 1;
        }
        
        ifstream fin(i_csv->c_str());
        if ( ! fin.is_open()){
            cerr<<"Could not open csv file"<<endl;
            return 1;
        }
        
        
        
        //read in adn store all data in spreadsheet
        string line;
        getline(fin,line);
        vector<string> header = parse_csv<string>(line);
        
        unsigned int index = 0;
        vector< vector<float> > all_data;
        while (getline(fin,line)) {
            cout<<"line "<<line<<endl;
            vector<float> vals = parse_csv<float>(line);
            
            if (index==0)
            {  cout<<"valssize "<<vals.size()<<endl;
                all_data.resize(vals.size());
            }
            vector< vector<float> >::iterator i_all_data = all_data.begin();
            for (vector<float>::iterator i_v = vals.begin(); i_v != vals.end(); ++i_v, ++i_all_data )
            {
                i_all_data->push_back(*i_v);
            }
            
            ++index;
        }
        fin.close();
        
        //now lets write out the data
        vector<string>::iterator i_header = header.begin();
        for (vector< vector<float> >::iterator i_all_data = all_data.begin(); i_all_data != all_data.end(); ++i_all_data,++i_header)
        {
	  ofstream fout( ((outname+"/data/"+(*i_header))+"_gr" + uint2str(group) + ".txt").c_str());
            if (fout.is_open())
            {
                fnames_local.push_back((outname+"/data/"+(*i_header))+"_gr" + uint2str(group) +".txt");
                vector<float>::iterator i_data = i_all_data->begin();
                for ( vector< pair<string,unsigned int> >::const_iterator i_chr = bands.begin(); i_chr != bands.end(); ++i_chr,++i_data)
                {
                    cout<<"writedata "<<i_chr->second<<" "<<ticksPerBand<<" "<<group<<endl;
                    fout<<(i_chr->first)<<" "<<(i_chr->second + group)<<" "<<(i_chr->second + group+1)<<" "<<(*i_data)<<endl;
                }
                
                fout.close();
            }
        }
        
        radius_link=0.999;
      
        //lets write out
        if (fout_plot.is_open())
        {
            for (vector<string>::iterator i_names = fnames_local.begin();  i_names != fnames_local.end(); ++i_names)
            {
                fout_plot<<endl<<endl;
                
                fout_plot<<"<plot>"<<endl;
                fout_plot<<"type      = histogram"<<endl;
                fout_plot<<"min_value_change = 0.00001"<<endl;
                fout_plot<<"file      = "<<(*i_names)<<endl;
                
                fout_plot<<"radius           = dims(ideogram,radius_inner);"<<endl;
                fout_plot<<"r1="<<radius_link<<"r"<<endl;
                radius_link -= radius_width;
                fout_plot<<"r0="<<radius_link<<"r"<<endl;
                
                fout_plot<<"orientation = in"<<endl;
                fout_plot<<"fill_color = "<<colors[group]<<endl;
                //only supprts 2 groups at the moment
      
                fout_plot<<"stroke_type = bin"<<endl;
                fout_plot<<"color       = black"<<endl;
                fout_plot<<"stroke_thickness = 3"<<endl;
                
                
                fout_plot<<"extend_bin  = no"<<endl;
                
                if (group==0)
                {
                fout_plot<<"<axes>"<<endl;
                fout_plot<<"<axis>"<<endl;
                fout_plot<<"spacing   = 0.05r"<<endl;
                fout_plot<<"color     = vlgrey"<<endl;
                fout_plot<<"thickness = 2"<<endl;
                fout_plot<<"</axis>"<<endl;
                fout_plot<<"</axes>"<<endl;
                }
                
                fout_plot<<"</plot>"<<endl;
                fout_plot<<endl<<endl<<endl;
            }
        }
        
        filenames.insert(filenames.end(), fnames_local.begin(), fnames_local.end());
    }
    fout_plot.close();

    return 0;
}



//this function writes out data files from csv into circos compatibale formats
//As such it can create concentric rings of data
//The radius returned is to no where histograms end
int writeDataFilesForPlotsByChrom(const vector<string> & inname_csv, const string & outname ,const vector<string> & chroms,const vector< pair<string,unsigned int> > & bands, const unsigned int &  ticksPerBand )
{
    
    for (vector<string>::const_iterator i = chroms.begin(); i != chroms.end(); ++i)//,++i_b)
    {
        cout<<"chroms "<<(*i)<<endl;
        
        fs::path dir_chr(outname + "_" + (*i) +"/data");
        if (! fs::is_directory(dir_chr))
            if( ! create_directories(dir_chr))//if not succesful
            {
                cerr<<"Failed to open directory : "<<dir_chr<<endl;
                exit (EXIT_FAILURE);
            }
        
        
        
        cout<<"writeDataFilesForPlots "<<endl;
//        ofstream fout_plot(dir_chr.string()+"/csv_plots.conf");
        vector<string> colors(2);
        colors[0]="red";
        colors[1]="blue";
        unsigned int group=0;
        for (vector<string>::const_iterator i_csv = inname_csv.begin(); i_csv != inname_csv.end(); ++i_csv,++group)
        {
            vector<string> fnames_local;
            
            fs::path p_csv(*i_csv);
            if (! fs::exists(p_csv))
            {
                cerr<<"File does not exist: "<<p_csv.string();
                return 1;
            }
            
            ifstream fin(i_csv->c_str());
            if ( ! fin.is_open()){
                cerr<<"Could not open csv file"<<endl;
                return 1;
            }
            
            
            
            //read in adn store all data in spreadsheet
            string line;
            getline(fin,line);
            vector<string> header = parse_csv<string>(line);
            
            unsigned int index = 0;
            vector< vector<float> > all_data;
            while (getline(fin,line)) {
                cout<<"line "<<line<<endl;
                vector<float> vals = parse_csv<float>(line);
                
                if (index==0)
                {  cout<<"valssize "<<vals.size()<<endl;
                    all_data.resize(vals.size());
                }
                vector< vector<float> >::iterator i_all_data = all_data.begin();
                for (vector<float>::iterator i_v = vals.begin(); i_v != vals.end(); ++i_v, ++i_all_data )
                {
                    i_all_data->push_back(*i_v);
                }
                
                ++index;
            }
            fin.close();
            
            //now lets write out the data
            vector<string>::iterator i_header = header.begin();
            for (vector< vector<float> >::iterator i_all_data = all_data.begin(); i_all_data != all_data.end(); ++i_all_data,++i_header)
            {
	      ofstream fout((dir_chr.string()+"/"+(*i_header)+"_gr" + uint2str(group) + ".txt").c_str());
                if (fout.is_open())
                {
                    vector<float>::iterator i_data = i_all_data->begin();
                    for ( vector< pair<string,unsigned int> >::const_iterator i_chr = bands.begin(); i_chr != bands.end(); ++i_chr,++i_data)
                    {
                        cout<<"writedata "<<i_chr->second<<" "<<ticksPerBand<<" "<<group<<endl;
                        //only write out data for the given chromosome
                        if (*i == i_chr->first)
                            fout<<(i_chr->first)<<" "<<(i_chr->second + group)<<" "<<(i_chr->second + group+1)<<" "<<(*i_data)<<endl;
                    }
                    
                    fout.close();
                }
            }
            
        }
        
    }
    
    return 0;
}



void do_work(const string & inname, const vector<string> & inname_csv, const string & kname, const string & outname, const bool & inter_only_in, const bool & intra_only_in, const float & thresh_in, const float & rad_width, const int & label_size , const float & ideogram_radius, const float & min_in , const float & max_in )
{
    
    if (inname_csv.size() > 2)
    {
        cerr<<"Does not support more than 2 csv files"<<endl;
        exit (EXIT_FAILURE);
    }
    
    //check to make sure inputs exist
    vector<string> conn_names = parse_csv<string>(inname);
    for (vector<string>::iterator i_in =conn_names.begin(); i_in != conn_names.end(); ++i_in)
    {
        fs::path in_im(*i_in);
        if (! fs::exists(in_im))
        {
            cerr<<"File does not exist : "<<in_im<<endl;
            exit (EXIT_FAILURE);
        }
    }
    
    for (vector<string>::const_iterator i_csv = inname_csv.begin(); i_csv != inname_csv.end(); ++i_csv)
    {
        if ( ! i_csv->empty())//only do this it its been set
        {
            fs::path in_csv(*i_csv);
            if (! fs::exists(in_csv))
            {
                cerr<<"File does not exist : "<<inname_csv<<endl;
                exit (EXIT_FAILURE);
            }
        }
    }
    fs::path dir(outname);
    fs::path dir_full(outname+ "/full");
    fs::path dir_inter(outname+ "/inter");
    fs::path dir_intra(outname+ "/intra");
    fs::path dir_data(outname+ "/full/data");

    createDirectoryStructure(outname);

    
    
    
    unsigned int Ngroups=conn_names.size();
    
    volume<float> connectivity_matrix;
    read_volume(connectivity_matrix,conn_names[0]);
    
    if (conn_names.size() == 2)
    {
        volume<float> connectivity_matrix2;
        read_volume(connectivity_matrix2,conn_names[1]);
        connectivity_matrix = connectivity_matrix - connectivity_matrix2;
    }
    
    
    
 
    //{
////        std::cout << "Success" << "\n";
////    }
    vector<string> labels;
    vector<string> chromosomes;
    vector< vector<string> > bands;
    unsigned int ticksPerBand = Ngroups;//redudant but makes clear
    if (inname_csv.size() > ticksPerBand) ticksPerBand = inname_csv.size();
    vector< pair<string,unsigned int> > id_block;

    
    //--------READ IN Karyotype file---------//
    ifstream fk(kname.c_str());
    if (fk.is_open())
    {
        string line;
        while (getline(fk,line)) {
            stringstream ss;
            ss<<line;
            string val,label;
            ss>>val;
            if (val == "chr" )
            {
                ss>>val>>val>>label;
//                cout<<"label "<<label<<endl;
//                unsigned int block, block_end;
//                ss>>block>>block_end;
                chromosomes.push_back(val);
                bands.push_back(vector<string>());
//                for (int i = block ; i < block_end;  i+=ticksPerBand)
//                {
//                    cout<<"add block "<<val<<" "<<i<<endl;
//                    id_block.push_back( pair<string, unsigned int>(val,i));
//                }
                
            }else if (val == "band")
            {
                string chr;
                ss>>chr>>val;
                labels.push_back(val);

                int index=0;
                for (vector<string>::const_iterator i = chromosomes.begin(); i != chromosomes.end(); ++i,++index)
                {
//                    cout<<"comare "<<chr<<" "<<(*i)<<endl;
                    if (chr == *i)
                    {
//                        cout<<"pushback at index "<<index<<endl;
                        id_block.push_back( pair<string, unsigned int>(chr,( bands[index].size()*ticksPerBand)));

                    bands[index].push_back(val);
//                        cout<<"add block "<<chr<<" "<<( bands[index].size()*ticksPerBand)<<endl;

                        break;
                    }
                }
                
                
            }
        }
        fk.close();
    }
    
    int N = connectivity_matrix.xsize();
    float min = abs(connectivity_matrix.value(1,0,0));
    float max = abs(connectivity_matrix.value(1,0,0));
    //make absolute min/max
    for (int x = 0 ; x < N ; ++x)
        for (int y = x+1 ; y < N ; ++y)
        {
            float val =  abs(connectivity_matrix.value(x,y,0));
            if (min > val) min = val;
            if (max < val) max = val;
        }
    //pverride data definecd max min

   //greater than because only supports positive
    		if (min_in > 0) min = min_in;
   if (max_in > 0) max = max_in;

    
    //write data
    
    for (vector< pair<string,unsigned int> >::iterator i_id = id_block.begin(); i_id != id_block.end(); ++i_id)
    {
        cout<<"idblock "<<i_id->first<<" "<<i_id->second<<endl;
    }
    
    vector<string> csv_filenames;
    float radius_link;
    writeDataFilesForPlots(inname_csv,dir_full.string(),csv_filenames,id_block, ticksPerBand,rad_width,radius_link);
    writeDataFilesForPlotsByChrom(inname_csv,dir_intra.string(),chromosomes,id_block, ticksPerBand);

    //this is the case where there is no csvs
    if (radius_link == 0 ) radius_link = 1;
    
    
    for (vector<string>::iterator i = csv_filenames.begin(); i != csv_filenames.end(); ++i)
    {
        cout<<"copy file to inter and intra "<<(fs::path(*i)).filename().string()<<endl;
        fs::copy_file(*i, dir_intra.string()+"/data/"+(fs::path(*i)).filename().string(),fs::copy_option::overwrite_if_exists);
        fs::copy_file(*i, dir_inter.string()+"/data/"+(fs::path(*i)).filename().string(),fs::copy_option::overwrite_if_exists);

    }
    fs::copy_file(dir_full.string()+"/csv_plots.conf", dir_intra.string()+"/csv_plots.conf",fs::copy_option::overwrite_if_exists);
    fs::copy_file(dir_full.string()+"/csv_plots.conf", dir_inter.string()+"/csv_plots.conf",fs::copy_option::overwrite_if_exists);

    for (vector<string>::const_iterator i = chromosomes.begin(); i != chromosomes.end(); ++i)
    {
        fs::copy_file(dir_full.string()+"/csv_plots.conf", dir_intra.string()+"_"+(*i)+"/csv_plots.conf",fs::copy_option::overwrite_if_exists);

    }

    
    cout<<"Writing links..."<<endl;
    writeLinks(connectivity_matrix, dir_full.string()+"/links.txt", id_block,ticksPerBand,min,max,thresh_in,false, false);
    writeLinks(connectivity_matrix, dir_inter.string()+"/links.txt", id_block,ticksPerBand,min,max,thresh_in,true, false);
    writeLinks(connectivity_matrix, dir_intra.string()+"/links.txt", id_block,ticksPerBand,min,max,thresh_in,false, true);
    writeLinksByChrom(connectivity_matrix, dir_intra.string(),chromosomes, id_block,ticksPerBand,min,max,thresh_in,false, true);
   cout<<"Writing ColourTable.txt "<< outname + "/ctab.txt"<<endl;
    writeMapValueToColour2sided( outname + "/ctab.txt", min,max );




    cout<<"Writing node labels..."<<endl;
    writeNodeLabels(dir_full.string()+"/node_labels.txt",chromosomes,bands, ticksPerBand,labels);
    fs::copy_file(dir_full.string()+"/node_labels.txt", dir_inter.string()+"/node_labels.txt",fs::copy_option::overwrite_if_exists);
    fs::copy_file(dir_full.string()+"/node_labels.txt", dir_intra.string()+"/node_labels.txt",fs::copy_option::overwrite_if_exists);
    writeNodeLabelsByChrom(dir_intra.string(),chromosomes,bands, ticksPerBand,labels);

    cout<<"Writing karyotype..."<<endl;
    writeKaryotype(dir_full.string()+"/karyotype.txt", chromosomes,bands, ticksPerBand);
    fs::copy_file(dir_full.string()+"/karyotype.txt", dir_inter.string()+"/karyotype.txt",fs::copy_option::overwrite_if_exists);
    fs::copy_file(dir_full.string()+"/karyotype.txt", dir_intra.string()+"/karyotype.txt",fs::copy_option::overwrite_if_exists);
    writeKaryotypeByChrom(dir_intra.string(),chromosomes,bands, ticksPerBand);
    cout<<"Writing circos.cong..."<<endl;
    writeCircosConf(dir_full.string()+"/circos.conf", "karyotype.txt", chromosomes,bands,radius_link,label_size,csv_filenames);
    fs::copy_file(dir_full.string()+"/circos.conf", dir_inter.string()+"/circos.conf",fs::copy_option::overwrite_if_exists);
    fs::copy_file(dir_full.string()+"/circos.conf", dir_intra.string()+"/circos.conf",fs::copy_option::overwrite_if_exists);
    writeCircosConfByChrom(dir_intra.string(), "karyotype.txt", csv_filenames, chromosomes,bands,radius_link,label_size);
    cout<<"Writing ideogram..."<<endl;
    writeIdeogram(dir_full.string(),label_size,ideogram_radius);
    copyIdeogram(dir_full,dir_inter);
    copyIdeogram(dir_full,dir_intra);
    
    for (vector<string>::const_iterator i = chromosomes.begin(); i != chromosomes.end(); ++i)
    {
//        fs::path dir_chr(dir_intra.string()+"_"+(*i));
//        copyIdeogram(dir_full,dir_chr);
        writeIdeogram(dir_intra.string()+"_"+(*i),label_size,ideogram_radius,0);


    }
    
    
    
}


int main (int argc,  char * argv[])
{
    //    if (argc < 2)
    //    {
    //        usage();
    //        return 0;
    //    }
    //    string inname(argv[1]);
    //
    
    Tracer tr("main");
    OptionParser options(title, examples);
    
    try {
        // must include all wanted options here (the order determines how
        //  the help message is printed)
        options.add(inname);
        options.add(inname_csv);
//        options.add(inter_only);
//        options.add(intra_only);
        options.add(label_size);
        options.add(rwidth);
        options.add(thresh);
        options.add(karyoname);
        options.add(outname);
        options.add(verbose);
        options.add(ideogram_radius);
        options.add(minimum);
        options.add(maximum);

        options.add(help);
        nonoptarg = options.parse_command_line(argc, argv);
        if (  (!options.check_compulsory_arguments(true) ))
        {
            options.usage();
            exit(EXIT_FAILURE);
        }

        
        do_work(inname.value(),parse_csv<string>(inname_csv.value()),karyoname.value(), outname.value(), false,false, thresh.value(),rwidth.value(), label_size.value(),ideogram_radius.value(),minimum.value(),maximum.value());
        
        
    } catch(X_OptionError& e) {
        options.usage();
        cerr << endl << e.what() << endl;
        exit(EXIT_FAILURE);
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
    
}

