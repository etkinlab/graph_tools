//
//  main.cpp
//  interpret_motion_parameters
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include <string>
#include <set>
//FSL INCLUDE
#include "newimage/newimageall.h"
#include "utils/options.h"
#include "fslsurface/fslsurface.h"
#include "fslsurface/fslsurfacefns.h"

using namespace NEWIMAGE;
using namespace Utilities;

using namespace std;
//using namespace etkin_graph;
using namespace fslsurface_name;

string title="etkin_graph_surfaces Stanford University (Brian Patenaude)";
string examples="etkin_graph_surfaces -i <connection_matrix.nii.gz> -o <output_base>";




Option<bool> verbose(string("-v,--verbose"), false,
                     string("switch on diagnostic messages"),
                     false, no_argument);
Option<bool> help(string("-h,--help"), false,
                  string("display this message"),
                  false, no_argument);

//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
                      string("Filename of input image correlation matrix"),
                      true, requires_argument);

Option<string> nodenames(string("-n,--nin"), string(""),
                         string("Filename containing names of each node."),
                         true, requires_argument);
Option<string> coordname(string("-c,--cin"), string(""),
                      string("Filename containing coordinates of COG for each node."),
                      true, requires_argument);
Option<string> outname(string("-o,--output"), string(""),
                       string("Output name"),
                       true, requires_argument);
int nonoptarg;



set<int> getUniqueLabels(const volume<int> & mask)
{
    set<int> labels;
    
    const int* m_ptr = mask.fbegin();
    for ( unsigned long int i =0 ; i < mask.nvoxels(); ++i)
    {
        if (*m_ptr != 0 )
            labels.insert(*m_ptr);
    }
    
    return labels;
}

int do_work( const string & inname , const string & nodenames, const string & coordname, const string & outname ){
    cout<<"Reading connectivity matrices... "<<inname<<endl;
    //    g_thresh_method g_meth = MST_MIN_W;
    //READ IN
    volume<float> connectivity_matrix;
    cout<<"Read connectivity matrix : "<<inname<<endl;

    read_volume(connectivity_matrix,inname);
    
    
    
    //read in XYZ coords
    vector<string> v_names;
    vector< vec3<float> > v_coord;
    
    cout<<"Read COG for each feature : "<<coordname<<endl;

    ifstream fcoord(coordname.c_str());
    if (fcoord.is_open())
    {
        stringstream ss;
        string line;
        while ( getline(fcoord, line) )
        {
            ss<<line;
            float x,y,z;
            ss>>x>>y>>z;
            v_coord.push_back(vec3<float>(x,y,z));
            
        }
        fcoord.close();
    }
    
    cout<<"Read in feature names : "<<nodenames<<endl;
    ifstream fname(nodenames.c_str());
    if (fname.is_open())
    {
        int index;
        string line;
        while ( getline(fname, line) )
        {
            stringstream ss;
            ss<<line;
            ss>>index>>line;
            v_names.push_back(line);
        }
        fname.close();
    }
    
    
    //assume square nXnX1
    int n=connectivity_matrix.xsize();
    
    
    fslSurface<float,unsigned int> surf_graph_all;
    vec3<float> cog(0,0,0);
    int count =0 ;
    for ( typename vector< vec3<float> >::iterator i_c = v_coord.begin(); i_c != v_coord.end(); ++i_c,++count )
    {
        fslSurface<float,unsigned int> surf_graph;
        
        makeSphere( surf_graph, 5, 20 , 20 , *i_c );
        surf_graph.addScalars(count+1,"graph_index");
        if (count == 0 )
            surf_graph_all= surf_graph;
        else
            surf_graph_all.append(surf_graph);
    }
    writeGIFTI(surf_graph_all, outname +".gii");
//    writeVTK(surf_graph_all, outname+".vtk");

    
    //write connectivity file
    //custom connectivity file
    ofstream fconn((outname+".conn").c_str());
    
    if (fconn.is_open())
    {
        typename vector< vec3<float> >::iterator i_c = v_coord.begin();
        for ( vector<string>::iterator i_name = v_names.begin(); i_name != v_names.end(); ++i_name, ++i_c)
        {
            fconn<<(*i_name)<<" "<<i_c->x<<" "<<i_c->y<<" "<<i_c->z<<endl;
        }
        
        
        
        for (int x = 0 ; x < n ; ++x )
            for (int y = 0 ; y < n ; ++y )
            {
                float val = connectivity_matrix.value(x,y,0);
                if (abs(val) > 0 )
                {
                    fconn << "conn "<<x<<" "<<y<<" "<<abs(val)<<endl;
                }
            }
        
        fconn.close();
    }
    
    
       return 0;
}
int main (int argc,  char * argv[])
{
    //    if (argc < 2)
    //    {
    //        usage();
    //        return 0;
    //    }
    //    string inname(argv[1]);
    //
    
    Tracer tr("main");
    OptionParser options(title, examples);
    
    try {
        // must include all wanted options here (the order determines how
        //  the help message is printed)
        options.add(inname);
        options.add(nodenames);
        options.add(coordname);
        options.add(outname);

        options.add(verbose);
        options.add(help);
        nonoptarg = options.parse_command_line(argc, argv);
        if (  (!options.check_compulsory_arguments(true) ))
        {
            options.usage();
            exit(EXIT_FAILURE);
        }

        
        do_work(inname.value(), nodenames.value(), coordname.value(), outname.value());

        
    } catch(X_OptionError& e) {
        options.usage();
        cerr << endl << e.what() << endl;
        exit(EXIT_FAILURE);
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
    return 0;
}

