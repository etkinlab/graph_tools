//
//  network_metric.hpp
//  this code implements various network metrics for BGL Grpahs, in particular it implments thiouse found in BCT tool box
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include <iostream>
#include <math.h>
#include <vector>
#include <map>
//#include <pair>
using namespace std;

#include "etkin_graph_typedefs.h"
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/betweenness_centrality.hpp>
//#include <betweenness_centrality.hpp> //use local for debugging

using namespace boost;

namespace etkin_graph {
	
    
    template< class T >
    float mean( const vector<T> & vec )
    {
        float mean=0;
        int N=0;
        for ( typename vector<T>::const_iterator i = vec.begin(); i != vec.end(); ++i)
        {
            if (! isnan(*i) )
            {
            mean+=*i;
                ++N;
            }
//                cout<<"calculate mean "<<*i<<" "<<isnan(*i)<<endl;
        }
//        return mean/vec.size();
        return mean/N;

    }

    
    
	float corr_coef( const std::vector< std::pair<float,float> > & data )
	{
//		for (vector< pair<float,float> >::const_iterator i_d = data.begin(); i_d != data.end(); ++i_d)
//			cout<<i_d->first<<" "<<i_d->second<<endl;
//		
		//use double precision for processiing
        //reowrk the equation slightly to avoid numerical problems
		
        double sx=0, sy=0, sxx=0, syy=0, sxy=0; 
        unsigned int N = 0;
		for (vector< pair<float,float> >::const_iterator i_d = data.begin(); i_d != data.end(); ++i_d)
		{
			sx+=static_cast<double> (i_d->first);
			sy+=static_cast<double> (i_d->second);
			sxy+=static_cast<double> (i_d->first * i_d->second);
			sxx += static_cast<double> (i_d->first) * static_cast<double> (i_d->first);
			syy += static_cast<double> (i_d->second) * static_cast<double> (i_d->second);
			++N;
//			cout<<"calc sums "<<sx<<endl;
		}
		
	//caluclated this way because of numerical precision limits
        double num = sxy - (sx / N * sy);
        double N2=sqrt(N);
//		cout<<"num "<<num<<" "<<(sxx - (sx / N * sx))<<" "<<(syy - (sy / N * sy ))<<endl;
        return static_cast<float>(N*num/( N2*sqrt(sxx - (sx / N * sx))) / (N2 * sqrt(syy - (sy / N * sy )) ));
		
		
		
	}
//
//    
    template< class T >
	int getNumberOfNodes(const T & g ) { return num_vertices(g); }
    template int getNumberOfNodes<UndirectedGraph>(const UndirectedGraph & g );
    template int getNumberOfNodes<DirectedGraph>(const DirectedGraph & g );

    
    template< class T >
	int getNumberOfEdges(const T & g ) { return num_edges(g); }
    
    template int getNumberOfEdges<UndirectedGraph>(const UndirectedGraph & g );
    template int getNumberOfEdges<DirectedGraph>(const DirectedGraph & g );
//    int getNumberOfNodes(const DirectedGraph & g ) { return num_vertices(g); }
//    int getNumberOfEdges(const DirectedGraph & g ) { return num_edges(g); }

   template < class T >
    float getCostBin(const T & g) {
        int Nnodes=num_vertices(g);
        return static_cast<float>(num_edges(g))/ (Nnodes*(Nnodes-1)/2);
    }
   	 template float getCostBin<UndirectedGraph>(const UndirectedGraph & g);
   template float getCostBin<DirectedGraph>(const DirectedGraph & g);
    
    map<int,float> cluster_means( const vector<float> & vals, const vector<int> & clusters){
        set<int> s_clusters;
        for (vector<int>::const_iterator i_cl = clusters.begin(); i_cl != clusters.begin(); ++i_cl)
            s_clusters.insert(*i_cl);
        
        
        map<int,int> clust_2_count;
        map<int,float> clust_2_means;
        for (set<int>::iterator i_s = s_clusters.begin(); i_s != s_clusters.begin(); ++i_s)
        {
            clust_2_count[*i_s]=0;
            clust_2_means[*i_s]=0;
        }
        
        vector<int>::const_iterator i_cl = clusters.begin();
        for (vector<float>::const_iterator i_v = vals.begin(); i_v != vals.end(); ++i_v,++i_cl)
        {
            ++clust_2_count[*i_cl];
            clust_2_means[*i_cl]+= (*i_v);
        }
        
        for (set<int>::iterator i_s = s_clusters.begin(); i_s != s_clusters.begin(); ++i_s)
            clust_2_means[*i_s]/=clust_2_count[*i_s];
        
        return clust_2_means;
     
    }
    
    map<int,float> cluster_sums( const vector<float> & vals, const vector<int> & clusters){
        set<int> s_clusters;
        for (vector<int>::const_iterator i_cl = clusters.begin(); i_cl != clusters.begin(); ++i_cl)
            s_clusters.insert(*i_cl);
        
        map<int,float> clust_2_means;
        for (set<int>::iterator i_s = s_clusters.begin(); i_s != s_clusters.begin(); ++i_s)
        {
            clust_2_means[*i_s]=0;
        }
        
        vector<int>::const_iterator i_cl = clusters.begin();
        for (vector<float>::const_iterator i_v = vals.begin(); i_v != vals.end(); ++i_v,++i_cl)
        {
            clust_2_means[*i_cl]+= (*i_v);
        }
 
        return clust_2_means;
        
    }
    map<int,float> cluster_sums( const vector<float> & vals, const vector<unsigned int> & n, const vector<int> & clusters, float & overall_mean){
        set<int> s_clusters;
        for (vector<int>::const_iterator i_cl = clusters.begin(); i_cl != clusters.begin(); ++i_cl)
        s_clusters.insert(*i_cl);
        
        map<int,float> clust_2_means;
        map<int,unsigned int> clust_2_n;
        
        for (set<int>::iterator i_s = s_clusters.begin(); i_s != s_clusters.begin(); ++i_s)
        {
            clust_2_means[*i_s]=0;
            clust_2_n[*i_s] = 0 ;
        }
//        cout<<"add up clusters "<<clusters.size()<<" "<<n.size()<<" "<<vals.size()<<endl;
        {
            vector<int>::const_iterator i_cl = clusters.begin();
            vector<unsigned int>::const_iterator i_n = n.begin();
            for (vector<float>::const_iterator i_v = vals.begin(); i_v != vals.end(); ++i_v,++i_cl,++i_n)
            {
//                cout<<"add to mean "<<(*i_v)<<" "<<(*i_n)<<endl;
                if (*i_n > 0 )
                {
                    clust_2_means[*i_cl]+= (*i_v) *(*i_n); //account for different n
                    clust_2_n[*i_cl]+= (*i_n);
                }
            }
        }
        
        overall_mean = 0;
//        cout<<"norm values "<<clust_2_n.size()<<" "<<clust_2_means.size()<<endl;
        unsigned int totalN=0;
        {
            map<int,unsigned int>::iterator i_n = clust_2_n.begin();
            for ( map<int,float>::iterator i_m = clust_2_means.begin(); i_m != clust_2_means.end(); ++i_m, ++i_n)
            {
//                cout<<"cluster "<<i_m->second <<" "<< i_n->second<<endl;
                //need to do before nrom
                overall_mean+=i_m->second ;
                totalN+= i_n->second;
                i_m->second /= i_n->second;
                
            }
        }
//        cout<<"overallmean "<<overall_mean<<" "<<totalN<<" "<<(overall_mean/totalN)<<endl;

            if  (totalN>0)     overall_mean/= totalN;

//        if (overall_mean>1)
//        {
//            cerr<<"EXIT"<<endl;
//            exit (EXIT_FAILURE);
//        }
        return clust_2_means;
        
    }
    
    
//	vector<float> clustering_coef( const Graph & g )
//    {
//        vector< float > cluster_coefs(num_vertices(g),0);
//        
//        graph_traits < Graph >::adjacency_iterator ai, aend;
//        graph_traits < Graph >::adjacency_iterator aii, aaend;
//        graph_traits < Graph >::vertex_iterator vi, vend;
//        vector< float >::iterator i_clust = cluster_coefs.begin();
//        for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi,++i_clust) {
//			//            cout<<"vertex "<<*vi<<endl;
//            vector<float> clust_count(num_vertices(g),0);
//            unsigned int Nadj=0;
//			//defines neighbours of current vertex
//            for (boost::tie(ai, aend) = adjacent_vertices(*vi,g); ai != aend; ++ai,++Nadj)
//                clust_count[*ai]++;
//            
//            for (boost::tie(ai, aend) = adjacent_vertices(*vi,g); ai != aend; ++ai)
//            {
//				//                cout<<"------>"<<*ai<<endl;
//                
//                for (boost::tie(aii, aaend) = adjacent_vertices(*ai,g); aii != aaend; ++aii)
//                {
//					//                    cout<<"<-------"<<*aii<<endl;
//                    if (clust_count[*aii]>0)
//                        clust_count[*aii]++;
//                }
//            }
//			//            cout<<"vertex joint con "<<*vi<<endl;
//            int count=0;
//            for (vector<unsigned int>::iterator i = clust_count.begin(); i != clust_count.end(); ++i,++count)
//            {
//				//                cout<<*i<<" ";
//                if ((count != *vi) && (*i>1))
//                {
//                    ++*i_clust;
//					//                    cout<<*vi<<" : "<<count<<" "<<*i<<endl;
//                }
//            }//
//			//                cout<<*i<<" ";
//			//            cout<<endl;
//            if (*i_clust > 0 )
//            {
//                *i_clust -=1;
//				*i_clust/=Nadj;
//            }
//        }
//        return cluster_coefs;
//    }
    vector<float> clustering_coef_bu( const UndirectedGraph & g )
    {
        vector< float > cluster_coefs(num_vertices(g),0);
        
        graph_traits < UndirectedGraph >::adjacency_iterator ai, aend;
        graph_traits < UndirectedGraph >::adjacency_iterator aii, aaend;
        graph_traits < UndirectedGraph >::vertex_iterator vi, vend;
        vector< float >::iterator i_clust = cluster_coefs.begin();
        for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi,++i_clust) {
//            cout<<"vertex "<<*vi<<endl;
            vector<unsigned int> clust_count(num_vertices(g),0);
            unsigned int Nadj=0;
            for (boost::tie(ai, aend) = adjacent_vertices(*vi,g); ai != aend; ++ai,++Nadj)
                clust_count[*ai]++;
            
            for (boost::tie(ai, aend) = adjacent_vertices(*vi,g); ai != aend; ++ai)
            {
//                cout<<"------>"<<*ai<<endl;
                
                for (boost::tie(aii, aaend) = adjacent_vertices(*ai,g); aii != aaend; ++aii)
                {
//                    cout<<"<-------"<<*aii<<endl;
                    if (clust_count[*aii]>0)
                        clust_count[*aii]++;
                }
            }
//            cout<<"vertex joint con "<<*vi<<endl;
            int count=0;
            for (vector<unsigned int>::iterator i = clust_count.begin(); i != clust_count.end(); ++i,++count)
            {
//                cout<<*i<<" ";
                if ((count != *vi) && (*i>1))
                {
                    ++*i_clust;
//                    cout<<*vi<<" : "<<count<<" "<<*i<<endl;
                }
            }//
//                cout<<*i<<" ";
//            cout<<endl;
            if (*i_clust > 0 )
            {
                *i_clust -=1;
            *i_clust/=Nadj;
            }
        }
        return cluster_coefs;
    }
//    vector<float>
    
	float modularity(const UndirectedGraph & g, const  std::vector<int> & clusters )
	{
//		cout<<"modularity "<<endl;
		//used wikipedia for equation, http://en.wikipedia.org/wiki/Modularity_(networks)
		//vector<float> k = degree_und(g);
		float m2=0.0f;
	//	for (vector<float>::iterator i_k = k.begin(); i_k != k.end(); ++i_k)
//			m2+=*i_k;
//		float inv2m=1.0f/m2;
		//count nodes in cluster 
		map<int,int> clust2count;
		map<int,int> clust2index;
        for ( vector<int>::const_iterator i_c = clusters.begin(); i_c != clusters.end(); ++i_c)
            clust2count[*i_c]++;
		
		int index=0; 
		for (map<int,int>::iterator i_c= clust2count.begin(); i_c != clust2count.end(); ++i_c,++index)
			clust2index[i_c->first]=index;
			
	
		int C = clust2count.size();//C clusters
		
		vector<float> eii(C,0);
		vector<float> ai(C,0);
		property_map < UndirectedGraph, edge_weight_t >::type weight = get(edge_weight, const_cast<UndirectedGraph&>(g));
		graph_traits < UndirectedGraph >::edge_iterator ei, eend;
		for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
			int v0=source(*ei, g);
			int v1=target(*ei, g);
			int clust0=clusters[v0];
			int clust1=clusters[v1];
			if ( clust0 == clust1 )
			{
				eii[clust2index[clust0]] += 1.0f / weight[*ei];// * inv2m; do the normalization later
			}
			ai[clust2index[clust0]] += 1.0f / weight[*ei];
			ai[clust2index[clust1]] += 1.0f / weight[*ei];

			m2+=1.0f / weight[*ei];
		}
		
		//do nromalization and calculate Q 
		float Q = 0 ; 
	//	cout<<"mod calc "<<m2<<endl;
		vector<float>::iterator i_e = eii.begin();
		for ( vector<float>::iterator i_a = ai.begin();  i_a != ai.end(); ++i_a,++i_e)
		{
			//cout<<"a/e "<<*i_a<<" "<<*i_e<<endl;
			*i_a /= 2*m2; //2 factor is there because use edge ends
			*i_e /= m2;
			Q+= *i_e - ( (*i_a) * (*i_a)); 
		}
		
        //for output let's replicate across node
        
        
        
		return Q; 		
	}

    float modularity(const UndirectedGraph & g, const  std::vector<int> & clusters , std::vector<float> & eii_repl, std::vector<float> & ai_repl)
    {
        //		cout<<"modularity "<<endl;
        //used wikipedia for equation, http://en.wikipedia.org/wiki/Modularity_(networks)
        //vector<float> k = degree_und(g);
        float m2=0.0f;
        //	for (vector<float>::iterator i_k = k.begin(); i_k != k.end(); ++i_k)
        //			m2+=*i_k;
        //		float inv2m=1.0f/m2;
        //count nodes in cluster
        map<int,int> clust2count;
        map<int,int> clust2index;
        for ( vector<int>::const_iterator i_c = clusters.begin(); i_c != clusters.end(); ++i_c)
        clust2count[*i_c]++;
        
        int index=0;
        unsigned int Nnodes=0;
        for (map<int,int>::iterator i_c= clust2count.begin(); i_c != clust2count.end(); ++i_c,++index)
        {
            
        clust2index[i_c->first]=index;
            Nnodes +=i_c->second;
        }
        
        int C = clust2count.size();//C clusters
        
        vector<float> eii(C,0);
        vector<float> ai(C,0);
      
        property_map < UndirectedGraph, edge_weight_t >::type weight = get(edge_weight, const_cast<UndirectedGraph&>(g));
        graph_traits < UndirectedGraph >::edge_iterator ei, eend;
        for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
            int v0=source(*ei, g);
            int v1=target(*ei, g);
            int clust0=clusters[v0];
            int clust1=clusters[v1];
            if ( clust0 == clust1 )
            {
                eii[clust2index[clust0]] += 1.0f / weight[*ei];// * inv2m; do the normalization later
            }
            ai[clust2index[clust0]] += 1.0f / weight[*ei];
            ai[clust2index[clust1]] += 1.0f / weight[*ei];
            
            m2+=1.0f / weight[*ei];
        }
        
        //do nromalization and calculate Q
        float Q = 0 ;
        //	cout<<"mod calc "<<m2<<endl;
        vector<float>::iterator i_e = eii.begin();
        for ( vector<float>::iterator i_a = ai.begin();  i_a != ai.end(); ++i_a,++i_e)
        {
            //cout<<"a/e "<<*i_a<<" "<<*i_e<<endl;
            *i_a /= 2*m2; //2 factor is there because use edge ends
            *i_e /= m2;
            Q+= *i_e - ( (*i_a) * (*i_a)); 
        }
        
        //replicate the value into all vertices of the same cvluster, to faclitate circle plots and pther graphs
        eii_repl.assign(Nnodes,0);
        ai_repl.assign(Nnodes,0);
        for (int v = 0 ; v < Nnodes; ++v) {
            ai_repl[v] = ai[clust2index[clusters[v]]];
            eii_repl[v] = eii[clust2index[clusters[v]]];

        }
        
        
        
        return Q; 		
    }

    
    
    template <class T>
    vector<float> degree_und(const T & g ){
            //this differs from wihtin-module z-score, in that its not poer cluster degree
        int n = num_vertices(g);

        vector<float> Ki(n,0);
        //dont need count, don tneed to normalize
        //        vector<int> count(n,0);//this is the degree
        {//calculate Ki
            typename property_map < T, edge_weight_t >::type weight = get(edge_weight, const_cast<T&>(g));
            typename graph_traits < T >::edge_iterator ei, eend;
            for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
                int v0=source(*ei, g);
                int v1=target(*ei, g);
//                cout<<"degere "<<v0<<" "<<v1<<" "<<weight[*ei]<<endl;
                //inverts from weight to connection strength
                Ki[v0]+= 1.0f / weight[*ei];
                Ki[v1]+= 1.0f / weight[*ei];

            }
            
            
        }

        return Ki;
    }
    template vector<float> degree_und<UndirectedGraph>(const UndirectedGraph& g );
    template vector<float> degree_und<DirectedGraph>(const DirectedGraph& g );
    

    template <class T>
    std::vector<float> degree_out(const T & g)
    {
        //this differs from wihtin-module z-score, in that its not poer cluster degree
        int n = num_vertices(g);
        
        vector<float> Ki(n,0);
        //dont need count, don tneed to normalize
        //        vector<int> count(n,0);//this is the degree
        {//calculate Ki
            typename property_map < T, edge_weight_t >::type weight = get(edge_weight, const_cast<T&>(g));
            typename graph_traits < T >::edge_iterator ei, eend;
            for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
                int v0=source(*ei, g);
                int v1=target(*ei, g);
//                cout<<"degere_out "<<v0<<" "<<v1<<" "<<weight[*ei]<<endl;
                //inverts from weight to connection strength
                Ki[v0]+= 1.0f / weight[*ei];
//                Ki[v1]+= 1.0f / weight[*ei];
            }
        }
        
        return Ki;
    }
    template std::vector<float> degree_out<UndirectedGraph>(const UndirectedGraph & g);
    template std::vector<float> degree_out<DirectedGraph>(const DirectedGraph & g);

    
    template <class T>
    std::vector<float> degree_in(const T & g)
    {
        //this differs from wihtin-module z-score, in that its not poer cluster degree
        int n = num_vertices(g);
        
        vector<float> Ki(n,0);
        //dont need count, don tneed to normalize
        //        vector<int> count(n,0);//this is the degree
        {//calculate Ki
            typename property_map < T, edge_weight_t >::type weight = get(edge_weight, const_cast<T&>(g));
            typename graph_traits < T >::edge_iterator ei, eend;
            for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
                int v0=source(*ei, g);
                int v1=target(*ei, g);
//                cout<<"degere_in "<<v0<<" "<<v1<<" "<<weight[*ei]<<endl;
                //inverts from weight to connection strength
//                Ki[v0]+= 1.0f / weight[*ei];
                Ki[v1]+= 1.0f / weight[*ei];
            }
        }
//        for ( vector<float>::iterator  i = Ki.begin(); i != Ki.end(); ++i)
//	  cout<<(*i)<<" ";
//	cout<<endl;
        return Ki;
    }
    template std::vector<float> degree_in<UndirectedGraph>(const UndirectedGraph & g);
    template std::vector<float> degree_in<DirectedGraph>(const DirectedGraph & g);
    
    
    
    
    
    
    
    float powerLawGamma(const UndirectedGraph & g, const unsigned int & Nbins){
        //find min max
        
        vector<float> degree = degree_und(g);
        list<float> l_deg;
        for (vector<float>::iterator i = degree.begin(); i != degree.end(); ++i)
            l_deg.push_back(*i);
        
        l_deg.sort();
        
//        cout<<"Power gamma law "<<endl;
//        cout<<"Degrees "<< degree.size()<<" "<<degree[0]<<endl;
//        for (list<float>::iterator i = l_deg.begin(); i != l_deg.end(); ++i)
//            cout<<"degree "<<*i<<endl;
        
        float min = l_deg.front();
        float max = l_deg.back();
        float d_pk = (max-min)/(Nbins-1);
        unsigned int Ntotal = degree.size();
//        cout<<"min/max "<<min<<" "<<max<<endl;
//        vector<float> k(Nbins),p_k(Nbins);
//        vector<float>::iterator i_k = k.begin();
//        vector<float>::iterator i_pk = p_k.begin();
        list<float>::iterator i_deg = l_deg.begin();
      
        float sx(0),sy(0),sxx(0),sxy(0);
        for (int i = 0 ; i < Nbins; ++i )//,++i_pk, ++i_k )
        {
            float count=0;
            float up_bound=(i+1)*d_pk;
//            cout<<"upbound "<<up_bound<<endl;
            if (i_deg != l_deg.end())
            while (*i_deg <= (i+1)*d_pk )
            {
                ++count;
                ++i_deg;
                if (i_deg == l_deg.end()) break;
            }

            float logp = log(count / Ntotal);
            float logk = log(min + (i +0.5) * d_pk);
            sx += logk;
            sy += logp;
            sxx += logk*logk;
            sxy += logk*logp;

//           cout<<count<<" "<<logk<<" "<<logp<<endl;
            //calculate probability
        }
//        cout<<"]"<<endl;
        sxy/=Nbins;
        sxx/=Nbins;
        sx/=Nbins;
        sy/=Nbins;
        
        ////not finished
        float gamma = -1 * ( sxy - sx* sy ) / (sxx - sx*sx) ;
//        cout<<"gamma "<<gamma<<endl;
        return gamma;
        
        
    }
	
	float assortativity( const UndirectedGraph & g )
	{//need to find teh corresponding degree between each pair of connected node (i.e. what is the degree at each end of a edge)
		//for now this is just undirected and binary
        //this differs form bct at momenent, would need to double count for ins and outs.
		//		cout<<"calculating assortativity"<<endl;
		
		//calculate degrees 
		vector<float> deg = degree_und(g);
		//        vector<unsigned int> deg = degrees_und(g,edges);
		
		//loop over and stores and store in two pairs
		vector< pair<float,float> > paired_degrees;
        
        graph_traits < UndirectedGraph >::edge_iterator ei, eend;
//		cout<<"pairs "<<endl;		
		for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
//			cout<<"pairs "<<source(*ei, g)<<" "<<target(*ei, g)<<" "<<deg[source(*ei, g)]<<" "<<deg[target(*ei, g)]<<endl;
//			cout<<deg[source(*ei, g)]<<" "<<deg[target(*ei, g)]<<endl;
//			cout<<deg[target(*ei, g)]<<" "<<deg[source(*ei, g)]<<endl;

		
			paired_degrees.push_back( pair<float,float>(deg[source(*ei, g)],deg[target(*ei, g)]));
			paired_degrees.push_back( pair<float,float>(deg[target(*ei, g)],deg[source(*ei, g)]));

		}
//		cout<<"pairs---done "<<endl;
		//double count because unidirectional
		//paired_degrees.insert(paired_degrees.end(), paired_degrees.begin(),paired_degrees.end());
		//		}
		float r = corr_coef(paired_degrees);
		
		return r;
	}
    
//    float average_degree( const Graph & g)
//    {
////        vector<unsigned int> degrees(num_vertices(g));
//      //need to chnage to get unweighted
//        graph_traits < Graph >::vertex_iterator vi, vend;
////        vector<unsigned int>::iterator i_deg = degrees.begin();
//        float avg_degree = 0.0f;
//        for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {
//            cout<<"degree "<<boost::degree(*vi,g)<<endl;
//            avg_degree += boost::degree(*vi,g);
//            
//        }
//        avg_degree /= num_vertices(g);
//        
//        
//        return avg_degree;
//    }
    void multiplyWeights(UndirectedGraph & g,  const float & factor)
    {
        
        property_map < UndirectedGraph, edge_weight_t >::type weight = get(edge_weight, const_cast<UndirectedGraph&>(g));
        graph_traits < UndirectedGraph >::edge_iterator ei, eend;
        for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
            weight[*ei]*=factor;
        }
        
//        for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
//            cout<<"wiefhgt "<<weight[*ei]<<endl;
//        }


        
    }
    
    template<class T>
    void degree_intra_inter(const T & g, const  std::vector<int> & clusters, vector<float> & degree_intra,vector<float> & degree_inter)
    {
//    	cout<<"Calc degree_intra_inter"<<endl;
        int n = num_vertices(g);
       degree_inter.assign(n,0);
        degree_intra.assign(n,0);

        //dont need count, don tneed to normalize
        //        vector<int> count(n,0);//this is the degree
        {//calculate Ki
            typename property_map < T, edge_weight_t >::type weight = get(edge_weight, const_cast<T&>(g));
            typename graph_traits < T >::edge_iterator ei, eend;
            for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
                int v0=source(*ei, g);
                int v1=target(*ei, g);
          
                //cout<<"cluster "<<clusters[v0]<<" "<<clusters[v1]<<endl;
                if (clusters[v0] == clusters[v1])
                {
                    //cout<<"same cluster "<<endl;
                    degree_intra[v0]+= 1.0f / weight[*ei]; //convert weight to connection (1/r -> r)
                    degree_intra[v1]+= 1.0f / weight[*ei]; //convert weight to connection (1/r -> r)

                }else {
                    degree_inter[v0]+= 1.0f / weight[*ei];
                    degree_inter[v1]+= 1.0f / weight[*ei];
                }

            }
            
            
        }
//    	cout<<"done Calc degree_intra_inter"<<endl;

    }
    template void degree_intra_inter<UndirectedGraph>(const UndirectedGraph & g, const  std::vector<int> & clusters, vector<float> & degree_intra,vector<float> & degree_inter);
    template void degree_intra_inter<DirectedGraph>(const DirectedGraph & g, const  std::vector<int> & clusters, vector<float> & degree_intra,vector<float> & degree_inter);

    //lets return avg_conn
    template<class T>
    void avgconn_intra_inter(const T & g, const  std::vector<int> & clusters, vector<float> & avgconn_intra,vector<float> & avgconn_inter,vector<unsigned int>  & n_intra, vector<unsigned int> & n_inter, float & avgconn)
    {
    	avgconn = 0 ;
        int n = num_vertices(g);
        avgconn_inter.assign(n,0);
        avgconn_intra.assign(n,0);
        n_intra.assign(n,0);
        n_inter.assign(n,0);
        
        
        
        //dont need count, don tneed to normalize
        //        vector<int> count(n,0);//this is the avgconn
        unsigned int N = 0 ;

        {//calculate Ki
            typename property_map < T, edge_weight_t >::type weight = get(edge_weight, const_cast<T&>(g));
            typename graph_traits < T >::edge_iterator ei, eend;
            for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
                int v0=source(*ei, g);
                int v1=target(*ei, g);
//                if ((v0 == 5 ) || (v1 ==5 ))
//                {
//                cout<<"weight "<<(1.0f / weight[*ei])<<endl;
//                cout<<"cluster "<<clusters[v0]<<" "<<clusters[v1]<<endl;
//                }
                if (clusters[v0] == clusters[v1])
                {
                   // cout<<"same cluster "<<endl;
                    avgconn_intra[v0]+= 1.0f / weight[*ei]; //convert weight to connection (1/r -> r)
                    avgconn_intra[v1]+= 1.0f / weight[*ei]; //convert weight to connection (1/r -> r)
                    ++n_intra[v0];
                    ++n_intra[v1];
                    ++N;
//                    if ((v0 == 5 ) || (v1 ==5 ))
//                    cout<<"same cluster v0 "<<v0<<" "<<v1<<" "<<avgconn_intra[v0]<<" "<<avgconn_intra[v1]<<" "<<n_intra[v0]<<" "<<n_intra[v1]<<endl;
                    
                }else {
                    avgconn_inter[v0]+= 1.0f / weight[*ei];
                    avgconn_inter[v1]+= 1.0f / weight[*ei];
                    ++n_inter[v0];
                    ++n_inter[v1];
                    ++N;
                }
                //include inter and intrea
                avgconn+=1.0f / weight[*ei];
                
            }
//            cout<<"N "<<N<<" edges found "<<endl;
            
        }
    
        
        {
            vector<unsigned int>::iterator i_n = n_intra.begin();
            for (vector<float>::iterator i_sum = avgconn_intra.begin(); i_sum != avgconn_intra.end(); ++i_sum,++i_n)
            {
                if (*i_n > 0)
                {
                    *i_sum /= *i_n;
//                    cout<<"avgintra "<<(*i_sum)<<endl;
                }
            }
        }
        
        
        {
            vector<unsigned int>::iterator i_n = n_inter.begin();
            for (vector<float>::iterator i_sum = avgconn_inter.begin(); i_sum != avgconn_inter.end(); ++i_sum,++i_n)
            {
                if (*i_n > 0)
                {
                    *i_sum /= *i_n;
//                    cout<<"avgintee "<<(*i_sum)<<endl;
                }
            }
        }

        //global connectivity estimate
        avgconn/=N;
    }
    template void avgconn_intra_inter<UndirectedGraph>(const UndirectedGraph & g, const  std::vector<int> & clusters, vector<float> & avgconn_intra,vector<float> & avgconn_inter,vector<unsigned int>  & n_intra, vector<unsigned int> & n_inter, float & avgconn);
    template void avgconn_intra_inter<DirectedGraph>(const DirectedGraph & g, const  std::vector<int> & clusters, vector<float> & avgconn_intra,vector<float> & avgconn_inter,vector<unsigned int>  & n_intra, vector<unsigned int> & n_inter, float & avgconn);


	float systemSegregation( const UndirectedGraph & g, const  std::vector<int> & clusters )
	{
		//inverting the weights, such that high value is high connection i.e. high degree = more connected
        //if comparing against BCT, make sure to zero the diagonal
      
        int n = num_vertices(g);
        float sysSeg(0.0), intraNet(0.0), interNet(0.0);			
		unsigned int Nintra(0), Ninter(0);
	   
            property_map < UndirectedGraph, edge_weight_t >::type weight = get(edge_weight, const_cast<UndirectedGraph&>(g));
            graph_traits < UndirectedGraph >::edge_iterator ei, eend;
            for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
					//		cout<<"edge "<<source(*ei, g)<<"<-->"<<target(*ei, g)<<endl;
                int v0=source(*ei, g);
                int v1=target(*ei, g);
				//cout<<"cluster "<<clusters[v0]<<" "<<clusters[v1]<<endl;
                if (clusters[v0] == clusters[v1])
                {
					//cout<<"same cluster "<<endl;
					intraNet+= 1.0f / weight[*ei]; //convert weight to connection (1/r -> r)
					++Nintra;
                }else {
					interNet+= 1.0f / weight[*ei];
					++Ninter;
				}
				
                
            }
		   //cout<<"inter/intra "<<Ninter<<" "<<Nintra<<" "<<interNet<<" "<<intraNet<<endl;
	   
		intraNet /= Nintra;
		interNet /= Ninter;
		sysSeg = (intraNet - interNet) / intraNet;
				
        return sysSeg;
		
	}

    vector<float> within_module_degree_z( const UndirectedGraph & g, const vector<int> & clusters )
    {
            //inverting the weights, such that high value is high connection i.e. high degree = more connected
        //if comparing against BCT, make sure to zero the diagonal
//        cout<<"within_module_degree_z : number of edges "<<num_edges(g)<<endl;
        int n = num_vertices(g);
        vector<float> withinMod_z(n,0);
        map<int,int> clust2count;
//        map<int,float> clust2Ki;
        map<int,float> clust2Kis;
        map<int,float> clust2Ki_stddev;


        for ( vector<int>::const_iterator i_c = clusters.begin(); i_c != clusters.end(); ++i_c)
        {
            clust2count[*i_c]++;
        }
       
        vector<float> Ki(n,0);//this is the degree
        {//calculate Ki
            property_map < UndirectedGraph, edge_weight_t >::type weight = get(edge_weight, const_cast<UndirectedGraph&>(g));
            graph_traits < UndirectedGraph >::edge_iterator ei, eend;
            for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
//                cout<<"edge "<<source(*ei, g)<<"<-->"<<target(*ei, g)<<endl;
                int v0=source(*ei, g);
                int v1=target(*ei, g);
                if (clusters[v0] == clusters[v1])
                {
//                    cout<<"clusters "<<clusters[v0]<<" "<<weight[*ei]<<" "<<1 / weight[*ei]<<" "<<1 / weight[*ei]<<endl;
                    Ki[v0]+= 1.0f / weight[*ei];
                    Ki[v1]+= 1.0f / weight[*ei];
                    
                }
                
            }
        }
//        for ( vector<float>::const_iterator i_Ki = Ki.begin(); i_Ki != Ki.end(); ++i_Ki)
//        {
//            cout<<"Ki "<<*i_Ki<<endl;
//        }
//        
        //calculate Ksi and stdevK
        vector<float>::iterator i_Ki = Ki.begin();
        for ( vector<int>::const_iterator i_c = clusters.begin(); i_c != clusters.end(); ++i_c,++i_Ki)
        {
            clust2Kis[*i_c] += *i_Ki;
            clust2Ki_stddev[*i_c] += (*i_Ki) * ( *i_Ki) ;//calculate sum of squares, will write over below using sx,sxx, and n
        }
        
        
        for (map<int,float>::iterator i_Kis = clust2Kis.begin(); i_Kis != clust2Kis.end(); ++i_Kis)
        {
            int N = clust2count[i_Kis->first];

//            cout<<"Kis "<<i_Kis->first<<" "<<clust2count[i_Kis->first]<<" "<<i_Kis->second<<" "<<i_Kis->second / clust2count[i_Kis->first]<<" "<<( ( clust2Ki_stddev[i_Kis->first] - ( i_Kis->second * i_Kis->second )/N ) / (N-1))<<endl;
            //need to do stddev first
            clust2Ki_stddev[i_Kis->first] =  sqrtf(( clust2Ki_stddev[i_Kis->first] - ( i_Kis->second * i_Kis->second )/N ) / (N-1));
            i_Kis->second /= N;
        }
        
        vector<int>::const_iterator i_c = clusters.begin();
        vector<float>::iterator i_z = withinMod_z.begin();
        for ( vector<float>::const_iterator i_Ki = Ki.begin(); i_Ki != Ki.end(); ++i_Ki,++i_c,++i_z)
        {
//            cout<<"(*i_Ki) "<<(*i_Ki)<<" "<<clust2Kis[*i_c] <<" "<<clust2Ki_stddev[*i_c] <<endl;
//            cout<<"z-score "<<(((*i_Ki) - clust2Kis[*i_c] )/ clust2Ki_stddev[*i_c] ) <<endl;
            *i_z =(((*i_Ki) - clust2Kis[*i_c] )/ clust2Ki_stddev[*i_c] );
        }
        
        return withinMod_z;
    }
    vector<float> participation_coefficient( const UndirectedGraph & g, const vector<int> & clusters, map<int, vector<float> > & p_coef_per_clust  )
    {
        //inverting the weights, such that high value is high connection i.e. high degree = more connected
        //if comparing against BCT, make sure to zero the diagonal
//        cout<<"number of edges "<<num_edges(g)<<endl;
        int n = num_vertices(g);
        vector<float> p_coef(n,0);
        vector<float> ki_all(n,0);

        
        map<int,int> clust2count;
        //        map<int,float> clust2Ki;
//        map<int,float> clust2Kis;
        
        
        for ( vector<int>::const_iterator i_c = clusters.begin(); i_c != clusters.end(); ++i_c)
        {
            clust2count[*i_c]++;
        }

        //initialize Ki for each cluster (i.e. ki(m))
        map< int ,vector<float> > clust2Ki;
        vector<float> Ki(n,0);
        for (map<int, int >::iterator i_c = clust2count.begin(); i_c != clust2count.end(); ++i_c )
        {
            clust2Ki[i_c->first] = Ki;
        }
//        cout<<"calculate Ki "<<n<<endl;
        {//calculate Ki
            property_map < UndirectedGraph, edge_weight_t >::type weight = get(edge_weight, const_cast<UndirectedGraph&>(g));
            graph_traits < UndirectedGraph >::edge_iterator ei, eend;
            for (boost::tie(ei, eend) = edges(g); ei != eend; ++ei) {
                //                cout<<"edge "<<source(*ei, g)<<"<-->"<<target(*ei, g)<<endl;
                int v0=source(*ei, g);
                int v1=target(*ei, g);
                float conn = 1.0f / weight[*ei];
//                if (clusters[v0] == clusters[v1])
//                {
//                    //                    cout<<"clusters "<<clusters[v0]<<" "<<scale / weight[*ei]<<" "<<scale / weight[*ei]<<endl;
//                    Ki[v0]+= conn;
//                    Ki[v1]+= conn;
//                   
//                }
//                cout<<"add to ki "<<clusters[v1]<<" "<<v1<<" "<<conn<<endl;
//                cout<<"clust2Ki[clusters[v1]] "<<clust2Ki[clusters[v1]].size()<<endl;
                clust2Ki[clusters[v1]][v0] += conn; //conn of v0 to cluster of v1
                clust2Ki[clusters[v0]].at(v1) += conn; //conn of v0 to cluster of v1
//                cout<<"conn "<<v0<<" "<<v1<<" "<<conn<<endl;
                ki_all[v0] += conn;
                ki_all[v1] += conn;
//                cout<<"ki_all[v0] "<<ki_all[v0]<<" "<<ki_all[v1]<<" "<<conn<<endl;

                
            }
        }
      
        
        for ( map<int, vector<float>  >::iterator i_Ki = clust2Ki.begin(); i_Ki != clust2Ki.end(); ++i_Ki )
        {
            vector<float>::iterator i_p = p_coef.begin();
            vector<float>::iterator i_kiall = ki_all.begin();
                    //loop over vertex for given cluster
//            cout<<"cluster "<<i_Ki->first<<endl;
            for ( vector<float>::iterator ii_Ki = i_Ki->second.begin(); ii_Ki != i_Ki->second.end(); ++ii_Ki,++i_p, ++i_kiall)
            {
                *i_p += (*ii_Ki) * (*ii_Ki) / ((*i_kiall)  * (*i_kiall));
            }
                
        }
        
        vector<float>::iterator i_kiall = ki_all.begin();
        for ( vector<float>::iterator i_p = p_coef.begin(); i_p != p_coef.end(); ++i_p,++i_kiall)
        {
//            cout<<"kiall "<<*i_kiall<<endl;
//			cout<<"i_p "<<*i_p<<endl;

            if ( *i_kiall == 0 )
                *i_p = 0 ;
            else    
                *i_p = 1.0f - *i_p;
        }

        p_coef_per_clust = clust2Ki;

        map<int, vector<float>  >::iterator i_p_coef_per_clust = p_coef_per_clust.begin();
        for ( map<int, vector<float>  >::iterator i_Ki = clust2Ki.begin(); i_Ki != clust2Ki.end(); ++i_Ki,++i_p_coef_per_clust )
        {
            vector<float>::iterator i_kiall = ki_all.begin();
            //loop over vertex for given cluster
            vector<float>::iterator ii_p_coef_per_clust = i_p_coef_per_clust->second.begin();
            for ( vector<float>::iterator ii_Ki = i_Ki->second.begin(); ii_Ki != i_Ki->second.end(); ++ii_Ki, ++i_kiall, ++ii_p_coef_per_clust)
            {
//                cout<<"ii_KI "<<(*ii_Ki)<<endl;
                
                *ii_p_coef_per_clust =  (*ii_Ki) * (*ii_Ki) / ((*i_kiall)  * (*i_kiall));
            }
            
        }
        
        
        
        
        return p_coef;
    }
    
    std::vector<float> closeness_centrality( const UndirectedGraph & g , const vector<int> & excl_inds )//assumes connected
    {
        int n = num_vertices(g);
        
        //create mask
        vector<bool> mask(n,1);
        int index =0 ;
//        cout<<"excl "<<excl_inds.size()<<endl;
        for (vector<int>::const_iterator i_e = excl_inds.begin(); i_e != excl_inds.end(); ++i_e, ++index )
        {
            mask[*i_e]=0;
        }
            
        
        
        std::vector<vertex_descriptor> p(num_vertices(g));
        std::vector<float> d(num_vertices(g));
        std::pair<vertex_iterator, vertex_iterator> i_verts = vertices(g);
        
        std::vector<float> closeness(num_vertices(g),0);
//        std::vector<int> v_count(num_vertices(g),0);
        // dijkstra_shortest_paths
        for (vertex_iterator i_v = i_verts.first; i_v != i_verts.second; ++i_v)
        {
            float farness=0;
            dijkstra_shortest_paths(g, *(i_v), predecessor_map(boost::make_iterator_property_map(p.begin(), get(boost::vertex_index, g))).distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
            
            //            std::cout << "distances and parents:" << std::endl;
            float avg_inv_dist = 0.0;
            int Nnew=0;
            graph_traits < UndirectedGraph >::vertex_iterator vi, vend;
            for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {
                if (*vi != *i_v)
                {
                    if ((mask[*vi]) && (mask[*i_v]))
                    {
                        farness += d[*vi];
//                        cout<<"farness "<<(*i_v)<<" "<<(*vi)<<" "<<d[*vi]<<" "<<d.size()<<endl;
                        ++Nnew;

                    }
//                    else
//                        cout<<"farness0 "<<(*i_v)<<" "<<(*vi)<<" "<<0<<" "<<d.size()<<endl;
                }
            }
//            cout<<"sum farness "<<farness<<endl;
            closeness[*i_v] = static_cast<float>(Nnew )/farness;

//            closeness[*i_v] = static_cast<float>(n - excl_inds.size() -1 )/farness;
        }
        
        return closeness;//(n*(n-1)/2.0f) )  ;        
    }

    
    void  centrality_brandes( const UndirectedGraph & g ,std::vector<double> & v_centrality_vec,  bool rel)
    {
        
        VertexIndexMap v_index = get(boost::vertex_index, g);
        v_centrality_vec.assign(boost::num_vertices(g), 0.0);
        
//        EdgeIndexMap e_index = get(boost::edge_index, g);
//        e_centrality_vec.assign(boost::num_vertices(g), 0.0);
      
        
        std::vector<vertex_descriptor> p(num_vertices(g));
        std::vector<float> d(num_vertices(g));

        boost::iterator_property_map< std::vector< double >::iterator, VertexIndexMap > v_centrality_map(v_centrality_vec.begin(), v_index);
//        boost::iterator_property_map< std::vector< double >::iterator, EdgeIndexMap > e_centrality_map(e_centrality_vec.begin(), e_index);

        
      
        brandes_betweenness_centrality(g,v_centrality_map);

        //scale by (2 / ((N^2 -3n +2)
       if (rel)
            relative_betweenness_centrality(g,v_centrality_map);

        
    }
    
    template <class T>
    float efficiency_wd( const T & g , const vector<int> & excl_inds )
    {
        int n = num_vertices(g);

        //create mask
        vector<bool> mask(n,1);
        int index =0 ;
//        cout<<"excl "<<excl_inds.size()<<endl;
        for (vector<int>::const_iterator i_e = excl_inds.begin(); i_e != excl_inds.end(); ++i_e, ++index )
        {
            mask[*i_e]=0;
        }
        
        std::vector<vertex_descriptor> p(num_vertices(g));
        std::vector<float> d(num_vertices(g));
        std::pair<vertex_iterator, vertex_iterator> i_verts = vertices(g);
        
        float efficiency_global = 0.0f;
        int Nnew=0;

        // dijkstra_shortest_paths
        for (vertex_iterator i_v = i_verts.first; i_v != i_verts.second; ++i_v)
        {
     
            dijkstra_shortest_paths(g, *(i_v), predecessor_map(boost::make_iterator_property_map(p.begin(), get(boost::vertex_index, g))).distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
            
//            std::cout << "distances and parents:" << std::endl;
            float avg_inv_dist = 0.0;
            graph_traits < UndirectedGraph >::vertex_iterator vi, vend;
            for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {
                            if (*vi != *i_v)
                            {
                                // std::cout << "parent(" << *vi << ") = " << p[*vi] << std::endl;
                                //                std::cout << "distance("<<(*(i_v))<<"," << (*vi)<<") = " << (d[*vi] )<<endl;
                                // avg_inv_dist+=d[*vi] / scale;
                                if ((mask[*vi]) && (mask[*i_v]))
                                {
                                    efficiency_global += 1.0f / (d[*vi] ) ;
                                    ++Nnew;
                                }
                                //(avg_inv_dist / (num_vertices(g)-1));
                            }
            }
            //            v_effeciency[*i_v] = 1.0f / (avg_inv_dist / (num_vertices(g)-1));
            //            std::cout << std::endl;
        }
        
        
        return (efficiency_global / (n*n - n));//(n*(n-1)/2.0f) )  ;
    }
    template float efficiency_wd<UndirectedGraph>( const UndirectedGraph & g , const vector<int> & excl_inds );
    template float efficiency_wd<DirectedGraph>( const DirectedGraph & g , const vector<int> & excl_inds );



    template<class T>
    float characteristicPathLength_wd( const T & g , const vector<int> & excl_inds )
    {
        int n = num_vertices(g);
        //create mask
        vector<bool> mask(n,1);
        int index =0 ;
//        cout<<"excl "<<excl_inds.size()<<endl;
        for (vector<int>::const_iterator i_e = excl_inds.begin(); i_e != excl_inds.end(); ++i_e, ++index )
        {
            mask[*i_e]=0;
        }
        

        std::vector<vertex_descriptor> p(num_vertices(g));
        std::vector<float> d(num_vertices(g));
        std::pair<vertex_iterator, vertex_iterator> i_verts = vertices(g);
        
        float charPLen = 0.0f;
        int Nnew=0;
        // dijkstra_shortest_paths
        for (vertex_iterator i_v = i_verts.first; i_v != i_verts.second; ++i_v)
        {
            dijkstra_shortest_paths(g, *(i_v), predecessor_map(boost::make_iterator_property_map(p.begin(), get(boost::vertex_index, g))).distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));
            
            //            std::cout << "distances and parents:" << std::endl;
            float avg_inv_dist = 0.0;
            typename graph_traits < T >::vertex_iterator vi, vend;
            for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi) {
                if (*vi != *i_v)
                {
                    //                std::cout << "parent(" << *vi << ") = " << p[*vi] << std::endl;
                    //                std::cout << "distance("<<(*(i_v))<<"," << (*vi)<<") = " << 1.0f / (d[*vi] / scale) <<endl;
                    // avg_inv_dist+=d[*vi] / scale;
                    if ((mask[*vi]) && (mask[*i_v]))
                    {
                        charPLen += (d[*vi] ) ;
                        Nnew++;
                    }
                    //(avg_inv_dist / (num_vertices(g)-1));
                }
            }
            //            v_effeciency[*i_v] = 1.0f / (avg_inv_dist / (num_vertices(g)-1));
            //            std::cout << std::endl;
        }
//        int Nnew = n - excl_inds.size();

//        return (charPLen / (n*n - n));//(n*(n-1)/2.0f) )  ;
        return charPLen/Nnew;
    }
    template  float characteristicPathLength_wd<UndirectedGraph>( const UndirectedGraph & g , const vector<int> & excl_inds );
    template  float characteristicPathLength_wd<DirectedGraph>( const DirectedGraph & g , const vector<int> & excl_inds );

    
    ///use the network_mnetrics struct
    template<class T>
    void getNumberOfNodes(const T & g, network_metrics & nmets  ) { nmets.Nnodes = getNumberOfNodes(g); }
    template void getNumberOfNodes<UndirectedGraph>(const UndirectedGraph & g, network_metrics & nmets );
    template void getNumberOfNodes<DirectedGraph>(const DirectedGraph & g, network_metrics & nmets );

    
    template<class T>
    void getNumberOfEdges(const T & g , network_metrics & nmets ){  nmets.Nedges = getNumberOfEdges(g); }
    template void getNumberOfEdges<UndirectedGraph>(const UndirectedGraph & g, network_metrics & nmets );
    template void getNumberOfEdges<DirectedGraph>(const DirectedGraph & g, network_metrics & nmets );

        
    template <class T>
    void degree_und(const T & g, network_metrics & nmets){
        nmets.degree = degree_und(g);
        nmets.avg_degree = mean(nmets.degree);

    }
    template void degree_und<UndirectedGraph>(const UndirectedGraph & g, network_metrics & nmets);
    template void degree_und<DirectedGraph>(const DirectedGraph & g, network_metrics & nmets);

    
        template <class T>
    void degree_in(const T & g, network_metrics & nmets)
    {
        nmets.degree_in = degree_in(g);
        nmets.avg_degree_in = mean(nmets.degree_in);
        
    }
    template void degree_in<UndirectedGraph>(const UndirectedGraph & g, network_metrics & nmets);
    template void degree_in<DirectedGraph>(const DirectedGraph & g, network_metrics & nmets);

        template <class T>
    void degree_out(const T & g, network_metrics & nmets)
    {
        nmets.degree_out = degree_out(g);
        nmets.avg_degree_out = mean(nmets.degree_out);
        
    }
    template void degree_out<UndirectedGraph>(const UndirectedGraph & g, network_metrics & nmets);
    template void degree_out<DirectedGraph>(const DirectedGraph & g, network_metrics & nmets);

    
    template<class T>
    void degree_intra_inter(const T & g, const  std::vector<int> & clusters, network_metrics & nmets)
    {

        vector<float> degree_intra,degree_inter;
        degree_intra_inter( g, clusters,degree_intra, degree_inter);
        nmets.degree_inter = degree_inter;
        nmets.degree_intra = degree_intra;
        nmets.degree_inter_per_clust = cluster_sums(degree_inter,clusters);
        nmets.degree_intra_per_clust = cluster_sums(degree_intra,clusters);
        nmets.avg_degree_intra = mean(degree_intra);
        nmets.avg_degree_inter = mean(degree_inter);

        //the following way differs by a factor of two
        //nmets.avg_degree = (degree_inter.size() *  nmets.avg_degree_inter + degree_intra.size() * nmets.avg_degree_intra)/(degree_intra.size() + degree_inter.size());
    }
    template void degree_intra_inter<UndirectedGraph>(const UndirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets);
    template void degree_intra_inter<DirectedGraph>(const DirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets);

//    template<class T>
    void avgconn_intra_inter(const UndirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets)
    {
        vector<float>  avgconn_intra,avgconn_inter;
        vector<unsigned int>  n_intra,n_inter;
        float all_avgconn;
        avgconn_intra_inter( g, clusters,avgconn_intra, avgconn_inter,n_intra,n_inter,all_avgconn);
        nmets.avgconn_inter = avgconn_inter;
        nmets.avgconn_intra = avgconn_intra;
        nmets.avgconn_inter_per_clust = cluster_sums(avgconn_inter,n_inter, clusters,nmets.all_avgconn_inter);
        nmets.avgconn_intra_per_clust = cluster_sums(avgconn_intra,n_intra, clusters,nmets.all_avgconn_intra);
        nmets.all_avgconn = all_avgconn;
//        cout<<"avg_conn calc sums "<<n_inter.size()<<" "<<n_intra.size()<<" "<<nmets.avgconn_inter_per_clust.size()<<endl;
//        cout<<"avgconn "<<nmets.all_avgconn_inter<<" "<<nmets.all_avgconn_intra<<endl;
    }
//    template  void avgconn_intra_inter<UndirectedGraph>(const UndirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets);
//    template  void avgconn_intra_inter<DirectedGraph>(const DirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets);


    
    template<class T>
    void  efficiency_wd( const T & g , const vector<int> & excl_inds , network_metrics & nmets ){ nmets.efficiency = efficiency_wd(g,excl_inds) ; }
    template void  efficiency_wd<UndirectedGraph>( const UndirectedGraph & g , const vector<int> & excl_inds , network_metrics & nmets );
    template void  efficiency_wd<DirectedGraph>( const DirectedGraph & g , const vector<int> & excl_inds , network_metrics & nmets );


    template<class T>
    void characteristicPathLength_wd( const T & g , const vector<int> & excl_inds , network_metrics & nmets ) { nmets.charPathLength = characteristicPathLength_wd(g,excl_inds );}
    template void characteristicPathLength_wd<UndirectedGraph>( const UndirectedGraph & g , const vector<int> & excl_inds , network_metrics & nmets );
    template void characteristicPathLength_wd<DirectedGraph>( const DirectedGraph & g , const vector<int> & excl_inds , network_metrics & nmets );


    void systemSegregation( const UndirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets ) { nmets.systemSeg = systemSegregation(g,clusters); }
    void modularity(const UndirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets  ) { nmets.Q = modularity(g,clusters,nmets.Q_eii,nmets.Q_ai);}
    void within_module_degree_z( const UndirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets) { nmets.withinMod_z = within_module_degree_z(g,clusters); }
    void closeness_centrality( const UndirectedGraph & g, const vector<int> & excl_inds , network_metrics & nmets ) { nmets.closeness = closeness_centrality(g,excl_inds); nmets.mean_closeness = mean(nmets.closeness); }
    void participation_coefficient( const UndirectedGraph & g, const  std::vector<int> & clusters , network_metrics & nmets )
    {
        map<int, vector<float> > p_coef_per_clust;
        nmets.p_coef = participation_coefficient(g,clusters,p_coef_per_clust);
        nmets.p_coef_per_clust =p_coef_per_clust;
        nmets.mean_p_coef = mean(nmets.p_coef);
        
    }
    void assortativity( const UndirectedGraph & g, network_metrics & nmets ){ nmets.assort = assortativity(g);}

    template<class T>
    void getCostBin(const T & g, network_metrics & nmets ) { nmets.cost_bin = getCostBin(g); }
    template void getCostBin<UndirectedGraph>(const UndirectedGraph & g, network_metrics & nmets );
    template void getCostBin<DirectedGraph>(const DirectedGraph & g, network_metrics & nmets );


    void powerLawGamma(const UndirectedGraph & g,network_metrics & nmets, const unsigned int & Nbins) { nmets.powerLawGamma  = powerLawGamma(g,Nbins); }

    
    
	
}
