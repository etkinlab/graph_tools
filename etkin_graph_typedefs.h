#ifndef GRAPH_TYPE_DEFS_H
#define GRAPH_TYPE_DEFS_H
#include <boost/graph/adjacency_list.hpp>
#include <boost/property_map/property_map.hpp>
#include <iostream>
#include <fstream>
#include <sstream>

namespace etkin_graph {
    struct network_metrics{
        //Network summary measures
//        static string int2str(int a )
//        {
//            stringstream ss;
//            ss<<a;
//            string sa;
//            ss>>sa;
//            return sa;
//        }
        
        network_metrics(){
            avg_degree=avg_degree_in=avg_degree_out=charPathLength=cost=cost_bin=systemSeg=Q=assort=efficiency=powerLawGamma=0;
            mean_closeness=mean_p_coef=0;
        };
        
        static vector<string> getVariableNames()
        {
            vector<string> vars;
            vars.push_back("Nnodes");
            vars.push_back("Nedges");
            vars.push_back("cost");
            vars.push_back("cost_bin");
            vars.push_back("avg_degree");
            vars.push_back("avg_degree_in");
            vars.push_back("avg_degree_out");

            vars.push_back("avg_degree_intra");
            vars.push_back("avg_degree_inter");
            vars.push_back("powerLawGamma");
            vars.push_back("efficiency");
            vars.push_back("charPathLength");
            vars.push_back("systemSeg");
            vars.push_back("Q");
            vars.push_back("assort");
            vars.push_back("mean_closeness");
            vars.push_back("mean_p_coef");
            vars.push_back("all_avgconn");
            vars.push_back("all_avgconn_intra");
            vars.push_back("all_avgconn_inter");

            return vars;
        }
        
        static void printHeader(){
              std::cout<<"Nnodes,Nedges,cost,cost_bin,avg_degree,avg_degree_in,avg_degree_out,avg_degree_intra,avg_degree_inter,powerLawGamma,efficiency,charPathLength,systemSeg,Q,assort,mean_closeness,mean_p_coef,all_avgconn,all_avgconn_intra,all_avgconn_inter"<<std::endl;
        }
        void writeHeader(std::ofstream & fout){
            fout<<"Nnodes,Nedges,cost,cost_bin,avg_degree,avg_degree_in,avg_degree_out,avg_degree_intra,avg_degree_inter,powerLawGamma,efficiency,charPathLength,systemSeg,Q,assort,mean_closeness,mean_p_coef,all_avgconn,all_avgconn_intra,all_avgconn_inter";
            if (clust_index_2_name.empty())
            {
                //output cluster/netowrk info
                for (map<int, float >::iterator i = degree_intra_per_clust.begin(); i != degree_intra_per_clust.end(); ++i)
                    fout<<",degree_intra_clust_"<<(i->first);
                for (map<int, float >::iterator i = degree_inter_per_clust.begin(); i != degree_inter_per_clust.end(); ++i)
                    fout<<",degree_inter_clust_"<<(i->first);
                for (map<int, float >::iterator i = avgconn_intra_per_clust.begin(); i != avgconn_intra_per_clust.end(); ++i)
                    fout<<",avgconn_intra_clust_"<<(i->first);
                for (map<int, float >::iterator i = avgconn_inter_per_clust.begin(); i != avgconn_inter_per_clust.end(); ++i)
                    fout<<",avgconn_inter_clust_"<<(i->first);

//                for (map<int, vector<float> >::iterator i = p_coef_per_clust.begin(); i != p_coef_per_clust.end(); ++i)
//                    fout<<",p_coef_clust_"<<(i->first);
                
            }else{
                //output cluster/netowrk info
                for (map<int, float >::iterator i = degree_intra_per_clust.begin(); i != degree_intra_per_clust.end(); ++i)
                    fout<<",degree_intra_clust_"<<clust_index_2_name[i->first];
                for (map<int, float >::iterator i = degree_inter_per_clust.begin(); i != degree_inter_per_clust.end(); ++i)
                    fout<<",degree_inter_clust_"<<clust_index_2_name[i->first ];
                for (map<int, float >::iterator i = avgconn_intra_per_clust.begin(); i != avgconn_intra_per_clust.end(); ++i)
                fout<<",avgconn_intra_clust_"<<clust_index_2_name[i->first];
                for (map<int, float >::iterator i = avgconn_inter_per_clust.begin(); i != avgconn_inter_per_clust.end(); ++i)
                fout<<",avgconn_inter_clust_"<<clust_index_2_name[i->first ];

//                for (map<int, vector<float> >::iterator i = p_coef_per_clust.begin(); i != p_coef_per_clust.end(); ++i)
//                    fout<<",p_coef_clust_"<<clust_index_2_name[i->first ];
            }
//            fout<<std::endl;
        }
        void writeHeaderPerNodeAsRow(std::ofstream & fout){
            vector<string> names;
            names.push_back("closeness");
            names.push_back("degree");
            names.push_back("degree_in");
            names.push_back("degree_out");

            names.push_back("degree_inter");
            names.push_back("degree_intra");
            names.push_back("avgconn_inter");
            names.push_back("avgconn_intra");
            names.push_back("p_coef");
            names.push_back("withinMod_z");
            names.push_back("Q_eii");
            names.push_back("Q_ai");

            //            names.push_back("centrality_rel");
            
            int count=0;
            
//            fout<<"NodeName";
//            for ( vector<string>::iterator i_vname = names.begin(); i_vname != names.end(); ++i_vname)
//            {
//                fout<<","<<(*i_vname);
//                
//            }
            //            fout<<endl;
//            cout<<"names "<<names.size()<<" "<<node_names.size()<<endl;
             for ( vector<string>::iterator i_vname = names.begin(); i_vname != names.end(); ++i_vname)
                            for  ( vector<string>::iterator i_nname = node_names.begin(); i_nname != node_names.end(); ++i_nname,++count)
                            {
                                if (count > 0)
                                    fout<<",";
                                fout<<( *i_vname + "_" + *i_nname);
                            }
        }

        void writeHeaderPerNode(std::ofstream & fout){
            vector<string> names;
            names.push_back("closeness");
            names.push_back("degree");
            names.push_back("degree_in");
            names.push_back("degree_out");

            names.push_back("degree_inter");
            names.push_back("degree_intra");
            names.push_back("avgconn_inter");
            names.push_back("avgconn_intra");
            names.push_back("p_coef");
            names.push_back("withinMod_z");
            names.push_back("Q_eii");
            names.push_back("Q_ai");

            fout<<"NodeName";
            for ( vector<string>::iterator i_vname = names.begin(); i_vname != names.end(); ++i_vname)
            {
                fout<<","<<(*i_vname);
                
            }

        }

        
      
        void write( std::ofstream & fout){
            fout<<Nnodes<<","<<Nedges<<","<<cost<<","<<cost_bin<<","<<avg_degree<<","<<avg_degree_in<<","<<avg_degree_out<<","<<avg_degree_intra<<","<<avg_degree_inter<<","<<powerLawGamma<<","<<efficiency<<","<<charPathLength<<","<< systemSeg<<","<<Q<<","<<assort<<","<<mean_closeness<<","<<mean_p_coef<<","<<all_avgconn<<","<<all_avgconn_intra<<","<<all_avgconn_inter;
            

            //output cluster/netowrk info
            for (map<int, float >::iterator i = degree_intra_per_clust.begin(); i != degree_intra_per_clust.end(); ++i)
                fout<<","<<(i->second);
            for (map<int, float >::iterator i = degree_inter_per_clust.begin(); i != degree_inter_per_clust.end(); ++i)
                fout<<","<<(i->second);
            
            for (map<int, float >::iterator i = avgconn_intra_per_clust.begin(); i != avgconn_intra_per_clust.end(); ++i)
            fout<<","<<(i->second);
            for (map<int, float >::iterator i = avgconn_inter_per_clust.begin(); i != avgconn_inter_per_clust.end(); ++i)
            fout<<","<<(i->second);
//            for (map<int, vector<float> >::iterator i = p_coef_per_clust.begin(); i != p_coef_per_clust.end(); ++i)
//                fout<<","<<(i->second);
//            
          // fout<<std::endl;
        }
        void writePerNodeAsRow( std::ofstream & fout){
            writeVector(fout, closeness);
            fout<<",";

            writeVector(fout, degree);
            fout<<",";
            writeVector(fout, degree_in);
            fout<<",";
            writeVector(fout, degree_out);
            fout<<",";

            writeVector(fout, degree_inter);
            fout<<",";

            writeVector(fout, degree_intra);
            fout<<",";
//            cout<<"Write avgconn_inter "<<endl;

            writeVector(fout, avgconn_inter);
            fout<<",";
            
            writeVector(fout, avgconn_intra);
            fout<<",";
            
            writeVector(fout, p_coef);
            fout<<",";
            
            writeVector(fout, withinMod_z);
            fout<<",";
//            cout<<"Write Q_eii "<<endl;
            
            writeVector(fout, Q_eii);
            fout<<",";
            
            writeVector(fout, Q_ai);
            fout<<",";
            
//            cout<<"write centrality_rel"<<endl;

//            writeVector(fout, centrality_rel);


        }

        void writePerNodeAsColumns( std::ofstream & fout){
            
            vector<string> names;
            vector< vector<float> > all_mets;
            all_mets.push_back(closeness);
            names.push_back("closeness");
            all_mets.push_back(degree);
            names.push_back("degree");
            all_mets.push_back(degree_in);
            names.push_back("degree_in");
            all_mets.push_back(degree_out);
            names.push_back("degree_out");
            all_mets.push_back(degree_inter);
            names.push_back("degree_inter");
            all_mets.push_back(degree_intra);
            names.push_back("degree_intra");
            
            all_mets.push_back(avgconn_inter);
            names.push_back("avgconn_inter");
            all_mets.push_back(avgconn_intra);
            names.push_back("avgconn_intra");

            all_mets.push_back(p_coef);
            names.push_back("p_coef");
            all_mets.push_back(withinMod_z);
            names.push_back("withinMod_z");
            all_mets.push_back(Q_eii);
            names.push_back("Q_eii");
            all_mets.push_back(Q_ai);
            names.push_back("Q_ai");
//            all_mets.push_back(centrality_rel);
//            names.push_back("centrality_rel");
            //do cluster vals, Q_eii, Q_aii, replicate for each node
            
            writeVectors(fout, all_mets,names,node_names);

        }
        void print(){
            std::cout<<Nnodes<<","<<Nedges<<","<<cost<<","<<cost_bin<<","<<avg_degree<<","<<avg_degree_intra<<","<<avg_degree_inter<<","<<efficiency<<","<<charPathLength<<","<< systemSeg<<","<<Q<<","<<assort<<","<<mean_closeness<<","<<mean_p_coef<<std::endl;
        }
    
		//operators
		
		network_metrics& operator =(const network_metrics& a)
		{
			Nnodes=a.Nnodes;
			Nedges=a.Nedges;
			
			avg_degree=a.avg_degree ;
            avg_degree_in=a.avg_degree_in ;
            avg_degree_out=a.avg_degree_out ;

			avg_degree_inter=a.avg_degree_inter ;
			avg_degree_intra=a.avg_degree_intra ;
            
			all_avgconn=a.all_avgconn;
            all_avgconn_intra=a.all_avgconn_intra;
            all_avgconn_inter=a.all_avgconn_inter;

            
            powerLawGamma = a.powerLawGamma;
            
			charPathLength=a.charPathLength ;
			cost=a.cost ;
			cost_bin=a.cost_bin ;
			systemSeg=a.systemSeg ;
			Q=a.Q ;
			assort=a.assort ;
			efficiency=a.efficiency ;
			mean_closeness=a.mean_closeness ;
			mean_p_coef=a.mean_p_coef ;
			
			clust_index_2_name = a.clust_index_2_name ;
			p_coef_per_clust = a.p_coef_per_clust ;
			degree_inter_per_clust= a.degree_inter_per_clust; 
			degree_intra_per_clust = a.degree_intra_per_clust ;
            avgconn_inter_per_clust= a.avgconn_inter_per_clust;
            avgconn_intra_per_clust = a.avgconn_intra_per_clust ;
            
			closeness=a.closeness ; 
			degree=a.degree ;
            degree_in=a.degree_in ;

            degree_out=a.degree_out ;

			degree_inter=a.degree_inter ;
			degree_intra=a.degree_intra ;
            avgconn_inter=a.avgconn_inter ;
            avgconn_intra=a.avgconn_intra ;

			p_coef=a.p_coef ; 
			withinMod_z=a.withinMod_z ; 
			centrality_rel=a.centrality_rel;
			
            Q_eii = a.Q_eii;
            Q_ai = a.Q_ai;
			
//            intra_conn_per_clust = a.intra_conn_per_clust;
//            inter_conn_per_clust = a.inter_conn_per_clust;

			
			return *this;
		}
        static void writeVector(ofstream & fout,const vector<float> & v_src)
        {
            if (v_src.empty()) cout<<"vector empty"<<endl;
            if (v_src.empty()) return;

            vector<float>::const_iterator i_src = v_src.begin();
            fout<<(*i_src);
            ++i_src;
            for (; i_src != v_src.end();++i_src)
                fout <<","<<(*i_src);
        }
        static void writeVectors(ofstream & fout,const vector< vector<float> > & v_src, const vector<string> & v_names , const vector<string> & node_names )
        {
            unsigned int index=0;
            for ( vector<string>::const_iterator i_nname = node_names.begin(); i_nname != node_names.end(); ++i_nname,++index)
            {
//                cout<<"Writing vector : "<<(*i_nname)<<" "<<v_names.size()<<" "<<node_names.size()<<" "<<v_src.size()<<endl;
                fout<<(*i_nname);
                vector< vector<float> >::const_iterator i = v_src.begin();
                for ( vector<string>::const_iterator i_name = v_names.begin(); i_name != v_names.end(); ++i_name,++i)
                {
                	//cout<<"isize "<<i->size()<<endl;
                    if (i->empty())
                    	fout<<","<<0;
                    else
                    	fout<<","<<(i->at(index));
                }
                fout<<endl;
            }
        }
        static void addToVector(vector<float> & v_dest, const vector<float> & v_src)
        {
			vector<float>::const_iterator i_src = v_src.begin();
			for (std::vector<float>::iterator i_d = v_dest.begin(); i_d != v_dest.end();++i_d,++i_src) 
				(*i_d) += (*i_src);
		}
        static void subFromVector(vector<float> & v_dest, const vector<float> & v_src)
        {
            vector<float>::const_iterator i_src = v_src.begin();
            for (std::vector<float>::iterator i_d = v_dest.begin(); i_d != v_dest.end();++i_d,++i_src)
                (*i_d) -= (*i_src);
        }
		static void divideVector(vector<float> & v_dest, const float a)
		{
			for (std::vector<float>::iterator i_d = v_dest.begin(); i_d != v_dest.end();++i_d) 
				(*i_d) /= a;
		}
		network_metrics& operator +=(const network_metrics& a)
		{
			Nnodes+=a.Nnodes;
			Nedges+=a.Nedges;
			
			avg_degree+=a.avg_degree ;
            avg_degree_in+=a.avg_degree_in ;
            avg_degree_out+=a.avg_degree_out ;

			avg_degree_inter+=a.avg_degree_inter ;
			avg_degree_intra+=a.avg_degree_intra ;
            
			all_avgconn+=a.all_avgconn;
            all_avgconn_intra+=a.all_avgconn_intra;
            all_avgconn_inter+=a.all_avgconn_inter;

            powerLawGamma+=a.powerLawGamma;
            
			charPathLength+=a.charPathLength ;
			cost+=a.cost ;
			cost_bin+=a.cost_bin ;
			systemSeg+=a.systemSeg ;
			Q+=a.Q ;
			assort+=a.assort ;
			efficiency+=a.efficiency ;
			mean_closeness+=a.mean_closeness ;
			mean_p_coef+=a.mean_p_coef ;
			
//            cout<<"do vector adds "<<endl;
//ignore this variable			clust_index_2_name ;
			{
				map<int, vector<float> >::const_iterator j = a.p_coef_per_clust.begin();
				for ( map<int, vector<float> >::iterator i = p_coef_per_clust.begin(); i != p_coef_per_clust.end(); ++i,++j)
					addToVector(i->second,j->second);
			}
//            {
//                map<int, vector<float> >::const_iterator j = a.intra_conn_per_clust.begin();
//                for ( map<int, vector<float> >::iterator i = intra_conn_per_clust.begin(); i != intra_conn_per_clust.end(); ++i,++j)
//                    addToVector(i->second,j->second);
//            }
//            {
//                map<int, vector<float> >::const_iterator j = a.inter_conn_per_clust.begin();
//                for ( map<int, vector<float> >::iterator i = inter_conn_per_clust.begin(); i != inter_conn_per_clust.end(); ++i,++j)
//                    addToVector(i->second,j->second);
//            }
//
//            
//            
            
//            cout<<"do degeree "<<endl;
			{
				map<int, float >::const_iterator j = a.degree_inter_per_clust.begin();
				for ( map<int, float >::iterator i = degree_inter_per_clust.begin(); i != degree_inter_per_clust.end(); ++i,++j)
					i->second += j->second;
			}
			{
				map<int, float >::const_iterator j = a.degree_intra_per_clust.begin();
				for ( map<int, float >::iterator i = degree_intra_per_clust.begin(); i != degree_intra_per_clust.end(); ++i,++j)
					i->second += j->second;
			}

            {
                map<int, float >::const_iterator j = a.avgconn_inter_per_clust.begin();
                for ( map<int, float >::iterator i = avgconn_inter_per_clust.begin(); i != avgconn_inter_per_clust.end(); ++i,++j)
                i->second += j->second;
            }

            {
                map<int, float >::const_iterator j = a.avgconn_intra_per_clust.begin();
                for ( map<int, float >::iterator i = avgconn_intra_per_clust.begin(); i != avgconn_intra_per_clust.end(); ++i,++j)
                i->second += j->second;
            }
            
			addToVector(closeness,a.closeness) ; 
			addToVector(degree,a.degree) ;
            addToVector(degree_in,a.degree_in) ;
            addToVector(degree_out,a.degree_out) ;

			addToVector(degree_inter,a.degree_inter) ;
			addToVector(degree_intra,a.degree_intra) ;
            addToVector(avgconn_inter,a.avgconn_inter) ;
            addToVector(avgconn_intra,a.avgconn_intra) ;
			addToVector(p_coef,a.p_coef) ; 
			addToVector(withinMod_z,a.withinMod_z) ; 
			addToVector(centrality_rel,a.centrality_rel) ; 
            addToVector(Q_eii,a.Q_eii) ;
            addToVector(Q_ai,a.Q_ai) ;

			
			return *this;
		}
        
        network_metrics& operator -=(const network_metrics& a)
        {
            Nnodes-=a.Nnodes;
            Nedges-=a.Nedges;
            
            avg_degree-=a.avg_degree ;
            avg_degree_in-=a.avg_degree_in ;
            avg_degree_out-=a.avg_degree_out ;

            avg_degree_inter-=a.avg_degree_inter ;
            avg_degree_intra-=a.avg_degree_intra ;
            
			all_avgconn-=a.all_avgconn;
            all_avgconn_intra-=a.all_avgconn_intra;
            all_avgconn_inter-=a.all_avgconn_inter;
            
            powerLawGamma-=a.powerLawGamma;
            
            charPathLength-=a.charPathLength ;
            cost-=a.cost ;
            cost_bin-=a.cost_bin ;
            systemSeg-=a.systemSeg ;
            Q-=a.Q ;
            assort-=a.assort ;
            efficiency-=a.efficiency ;
            mean_closeness-=a.mean_closeness ;
            mean_p_coef-=a.mean_p_coef ;
            
            //ignore this variable			clust_index_2_name ;
            {
                map<int, vector<float> >::const_iterator j = a.p_coef_per_clust.begin();
                for ( map<int, vector<float> >::iterator i = p_coef_per_clust.begin(); i != p_coef_per_clust.end(); ++i,++j)
                    subFromVector(i->second,j->second);
            }
//            {
//                map<int, vector<float> >::const_iterator j = a.intra_conn_per_clust.begin();
//                for ( map<int, vector<float> >::iterator i = intra_conn_per_clust.begin(); i != intra_conn_per_clust.end(); ++i,++j)
//                    subFromVector(i->second,j->second);
//            }
//            {
//                map<int, vector<float> >::const_iterator j = a.inter_conn_per_clust.begin();
//                for ( map<int, vector<float> >::iterator i = inter_conn_per_clust.begin(); i != inter_conn_per_clust.end(); ++i,++j)
//                    subFromVector(i->second,j->second);
//            }
//            
//            
            
            {
                map<int, float >::const_iterator j = a.degree_inter_per_clust.begin();
                for ( map<int, float >::iterator i = degree_inter_per_clust.begin(); i != degree_inter_per_clust.end(); ++i,++j)
                    i->second -= j->second;
            }
            {
                map<int, float >::const_iterator j = a.degree_intra_per_clust.begin();
                for ( map<int, float >::iterator i = degree_intra_per_clust.begin(); i != degree_intra_per_clust.end(); ++i,++j)
                    i->second -= j->second;
            }
            
            {
                map<int, float >::const_iterator j = a.avgconn_inter_per_clust.begin();
                for ( map<int, float >::iterator i = avgconn_inter_per_clust.begin(); i != avgconn_inter_per_clust.end(); ++i,++j)
                i->second -= j->second;
            }
            {
                map<int, float >::const_iterator j = a.avgconn_intra_per_clust.begin();
                for ( map<int, float >::iterator i = avgconn_intra_per_clust.begin(); i != avgconn_intra_per_clust.end(); ++i,++j)
                i->second -= j->second;
            }
            
            
            
            subFromVector(closeness,a.closeness) ;
            subFromVector(degree,a.degree) ;

            subFromVector(degree_in,a.degree_in) ;
            subFromVector(degree_out,a.degree_out) ;

            subFromVector(degree_inter,a.degree_inter) ;
            subFromVector(degree_intra,a.degree_intra) ;
            subFromVector(avgconn_inter,a.avgconn_inter) ;
            subFromVector(avgconn_intra,a.avgconn_intra) ;
            subFromVector(p_coef,a.p_coef) ;
            subFromVector(withinMod_z,a.withinMod_z) ;
            subFromVector(centrality_rel,a.centrality_rel) ; 
            subFromVector(Q_eii,a.centrality_rel) ;
            subFromVector(Q_ai,a.centrality_rel) ;

            
            return *this;
        }
        
        
		
		network_metrics& operator /=(const float& a)
		{
			Nnodes/=a;
			Nedges/=a;
			
			avg_degree/=a ;
            avg_degree_in/=a ;
            avg_degree_out/=a ;

			avg_degree_inter/=a ;
			avg_degree_intra/=a ;
            
			all_avgconn/=a;
            all_avgconn_intra/=a;
            all_avgconn_inter/=a;
            
            powerLawGamma/=a;
            
			charPathLength/=a ;
			cost/=a ;
			cost_bin/=a ;
			systemSeg/=a ;
			Q/=a ;
			assort/=a ;
			efficiency/=a ;
			mean_closeness/=a ;
			mean_p_coef/=a ;
			
			//ignore this variable			clust_index_2_name ;
			{
				for ( map<int, vector<float> >::iterator i = p_coef_per_clust.begin(); i != p_coef_per_clust.end(); ++i)
					divideVector(i->second,a);
			}
            
//            {
//                for ( map<int, vector<float> >::iterator i = intra_conn_per_clust.begin(); i != intra_conn_per_clust.end(); ++i)
//                    divideVector(i->second,a);
//
//            }
//            {
//                for ( map<int, vector<float> >::iterator i = inter_conn_per_clust.begin(); i != inter_conn_per_clust.end(); ++i)
//                    divideVector(i->second,a);
//            }
//            
//            
            
            
			{
				for ( map<int, float >::iterator i = degree_inter_per_clust.begin(); i != degree_inter_per_clust.end(); ++i)
					i->second /=a;
			}
			{
				for ( map<int, float >::iterator i = degree_intra_per_clust.begin(); i != degree_intra_per_clust.end(); ++i)
					i->second /=a;
			}
			
            {
                for ( map<int, float >::iterator i = avgconn_inter_per_clust.begin(); i != avgconn_inter_per_clust.end(); ++i)
                i->second /=a;
            }
            {
                for ( map<int, float >::iterator i = avgconn_intra_per_clust.begin(); i != avgconn_intra_per_clust.end(); ++i)
                i->second /=a;
            }
            
            
            
			divideVector(closeness,a) ; 
			divideVector(degree,a) ;
            divideVector(degree_out,a) ;
            divideVector(degree_in,a) ;

			divideVector(degree_inter,a) ;
			divideVector(degree_intra,a) ;
            divideVector(avgconn_inter,a) ;
            divideVector(avgconn_intra,a) ;
			divideVector(p_coef,a) ; 
			divideVector(withinMod_z,a) ; 
			divideVector(centrality_rel,a) ; 
            divideVector(Q_eii,a) ;
            divideVector(Q_ai,a) ;

			
			return *this;
		}
		
		
		
		
        
        int Nnodes,Nedges;
        float avg_degree, avg_degree_in,avg_degree_out,avg_degree_inter, avg_degree_intra,all_avgconn,all_avgconn_intra,all_avgconn_inter, charPathLength, cost, cost_bin, systemSeg, Q, assort, efficiency,powerLawGamma;
        
        float mean_closeness, mean_p_coef;
        map<int, string > clust_index_2_name;
        map<int, vector<float> > p_coef_per_clust;//, intra_conn_per_clust, inter_conn_per_clust;
         map<int, float >degree_inter_per_clust, degree_intra_per_clust,avgconn_inter_per_clust, avgconn_intra_per_clust;
        vector<float> closeness, degree,degree_in,degree_out,degree_inter,avgconn_inter, degree_intra, avgconn_intra, p_coef, withinMod_z, centrality_rel,Q_eii,Q_ai;
        vector<string> node_names;
    };
    
    
    
    
    
	typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::undirectedS,
    boost::property < boost::vertex_name_t, std::string >, boost::property < boost::edge_weight_t, float > > UndirectedGraph;
    typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS,
    boost::property < boost::vertex_name_t, std::string >, boost::property < boost::edge_weight_t, float > > DirectedGraph;
    typedef boost::graph_traits < UndirectedGraph >::edge_descriptor Edge;
    typedef boost::graph_traits < DirectedGraph >::edge_descriptor DirectedEdge;
    typedef boost::graph_traits < UndirectedGraph >::vertex_descriptor vertex_descriptor;
    typedef boost::graph_traits< UndirectedGraph >::vertex_iterator vertex_iterator;
    typedef boost::property_map< UndirectedGraph, boost::vertex_index_t>::type VertexIndexMap;
//typedef boost::graph_traits < diGraph >::edge_descriptor diEdge;
//
//typedef boost::graph_traits<Graph>::edge_iterator edge_iterator;
//
//
//typedef boost::graph_traits < Graph >::vertex_descriptor Vertex;
//
//    typedef boost::property_map< Graph, boost::edge_index_t>::type EdgeIndexMap;

//typedef std::pair<int, int> E;
//    typedef boost::property_map< float, boost::vertex_index_t>::type CentralityMap;
//    typedef typename boost::property_traits<float>::key_type centrality;
//    typedef typename boost::property_traits<vertex_descriptor> key_vert;
//        typedef boost::property_map<centrality, key_vert> CentralityMap;
//  // typedef boost::shared_array_property_map<double, boost::property_map<Graph, vertex_index_t>::const_type> CentralityMap();
//    num_vertices(g), get(boost::vertex_index, g)
}
#endif
