function y = correlation_sim()
%This is design to simulate correaltion matrices from timeseries data
%the purpose is to determine the effect of noise on r-matrices 

%Define simulation properties
NumSignals=7
low_f=0.008
high_f=0.1
ScanDuration=8*60; %8 minute scan
TR=2  
NT=ScanDuration/TR+1
  msize=[500 NT];
%evenly spaced, could switch later to random 
  df=(high_f - low_f)/(NumSignals-1)
  freqs=low_f + df*(0:(NumSignals-1))
  time=0:TR:ScanDuration;

timeseries=[];
close all
				     figure
				     subplot(NumSignals,1,1)

%create base signals these will be mixed to form time series
%will know true correlations
base_sigs=[];
				     for sig = 1: NumSignals
						 subplot(NumSignals,1,sig)
						 
						 base_sigs=[base_sigs ; cos(time * freqs(sig)*2*pi) ];

plot(time,base_sigs(sig,:))
xlabel('time')						 

end
size(base_sigs)

%create timeseries data 
%save weights ,  but may not need
all_weights=[];
      figure
      subplot(10,1,1)

for row = 1:msize(1)
	  
			w=rand(NumSignals,1)*2-1;
all_weights=[ all_weights ; w'];

timeseries= [ timeseries ; sum(base_sigs .* repmat(w,1,NT)) ] ;
if (row <= 10)
 subplot(10,1,row)
plot(time,timeseries(row,:))
xlabel('time')
end
end


%correalation coefficient for the data 
canonR=corrcoef(timeseries');
  canonZ=r_to_z(canonR);
mean(triu(canonZ));
figure 
[N,X]=hist(triu(canonZ,1),20);
    N=N/sum(N)
      plot(X,N)
      all_mean=[];
all_var=[];
      for amp = 1:20

  %let's add noise to the system
timeseries = timeseries + 0.1*randn(size(timeseries));
canonR=corrcoef(timeseries');
canonZ=r_to_z(canonR);
%triu(canonZ(find(canonZ))))
all_mean=[all_mean mean(canonZ(find(triu(canonZ,1)))) ]
			      all_var=[all_var std(canonZ(find(triu(canonZ,1)))) ]

[N,X]=hist(canonZ,20);
N=N/sum(N);

hold on 
plot(X,N,'r*')
end
%  xlabel()
figure 
subplot(211)
plot(all_mean)
ylabel('mean')
subplot(212)

plot(all_var)
ylabel('Standard Deviation')



  function z = r_to_z(r)
  z=0.5*log((1+r)./(1-r));
z(eye(msize(1))==1)=0;
end
end



