//
//  main.cpp
//  interpret_motion_parameters
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include <string>
#include <set>
//FSL INCLUDE
#include "newimage/newimageall.h"
#include "utils/options.h"
#include "boost/filesystem.hpp"

using namespace NEWIMAGE;
using namespace Utilities;

using namespace std;

namespace fs = boost::filesystem;



string title="etkin_matrix_roi_dist Stanford University (Brian Patenaude) \n \t This function is used calculate the pairwise distance between ROIS. Output format will be dteermined by the extension.";
string examples="etkin_graph_roi_dist -i <connection_matrix.nii.gz> -o <output_base>";




Option<bool> verbose(string("-v,--verbose"), false,
                     string("switch on diagnostic messages"),
                     false, no_argument);
Option<bool> help(string("-h,--help"), false,
                  string("display this message"),
                  false, no_argument);

//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
					  string("Filename of text file. 3 Colums: x y z "),
					  true, requires_argument);

Option<string> outname(string("-o,--output"), string(""),
					   string("Output name"),
					   true, requires_argument);
int nonoptarg;

struct float3{
    float3(float xin,float yin,float zin)
    {
        x=xin;
        y=yin;
        z=zin;
    }
    float dist( float3 vin){
        return sqrtf((vin.x -x)*(vin.x -x) + (vin.y -y)*(vin.y -y)  + (vin.z -z)*(vin.z -z));
        
    }
    
    
    float x,y,z;
};

int do_work( const string & inname , const string & outname){
   
    if ( ! fs::exists(inname))
    {
        cerr<<"File does not exists : "<<inname<<endl;
        exit (EXIT_FAILURE);
    }
    
    vector<float3> v_cog;
    
    ifstream fcog(inname.c_str());
    if (fcog.is_open())
    {
        string line;
        while (getline(fcog,line)) {
            stringstream ss;
            ss<<line;
            float x,y,z;
            ss>>x>>y>>z;
            v_cog.push_back(float3(x,y,z));
        }
        
        
        fcog.close();
    }else{
        cerr<<"Failed to open file for reading : "<<inname<<endl;
        exit (EXIT_FAILURE);
    }
    
    unsigned int N = v_cog.size();
    fs::path p_out(outname);
    bool is_im(0);
    bool invalid_ext=false;
    string ext;
    for (; !p_out.extension().empty(); p_out = p_out.stem())
        ext = p_out.extension().string() +ext;
    
    
    if (( ext == ".nii.gz" ) ||  ( ext == ".nii" ))
    {
        is_im=1;
    }else if ( ( ext == ".txt" ) || ( ext == ".mat" ) )
    {
        is_im=0;
    }else{
        cerr<<"Invalid extension : "<<ext<<endl;
        exit (EXIT_FAILURE);
    }
    
    volume<float>* im_out = NULL;
    ofstream fout;

    if (is_im)
    {
        im_out = new volume<float>(N,N,1);
        *im_out=0;
        
    }else
    {
        fout.open(outname);
        if (! fout.is_open())
        {
            cerr<<"Failed to open file for reading"<<endl;
            exit (EXIT_FAILURE);
            
        }
    }
    
    
    vector<float3>::iterator i_v = v_cog.begin();
    for (int y = 0 ; y < N ; ++y, ++i_v )
    {
        vector<float3>::iterator i_v_dest = v_cog.begin();
        for (int x = 0 ; x < N ; ++x,++i_v_dest )
        {
            float val = 0 ;
            if ( x != y )
            val=i_v->dist(*i_v_dest);
//            cout<<"-------"<<endl;
//            cout<<i_v->x<<" "<<i_v->y<<" "<<i_v->z<<endl;
//            cout<<i_v_dest->x<<" "<<i_v_dest->y<<" "<<i_v_dest->z<<endl;
//            cout<<"dist "<<val<<endl;
//            cout<<"-------"<<endl;
            if (is_im  )
            {
                im_out->value(x,y,0)=val;
            }else{
                if (x>0)
                    fout<<" ";
                fout<<val;
            }
        
        }
        if (! is_im  )
            fout<<endl;

    }

    
    if ( is_im  )
    save_volume(*im_out,outname);
    
    
    if (fout.is_open())
        fout.close();
    
    if (im_out != NULL)
    delete im_out;

    
    return 0;
}
int main (int argc,  char * argv[])
{
    //    if (argc < 2)
    //    {
    //        usage();
    //        return 0;
    //    }
    //    string inname(argv[1]);
    //
    
	Tracer tr("main");
	OptionParser options(title, examples);
	
	try {
		// must include all wanted options here (the order determines how
		//  the help message is printed)
		options.add(inname);

		options.add(outname);
		options.add(verbose);
		options.add(help);
        
    	nonoptarg = options.parse_command_line(argc, argv);
        if (  (!options.check_compulsory_arguments(true) ))
		{
			options.usage();
			exit(EXIT_FAILURE);
		}
        
        do_work(inname.value(),outname.value());

    } catch(X_OptionError& e) {
		options.usage();
		cerr << endl << e.what() << endl;
		exit(EXIT_FAILURE);
	} catch(std::exception &e) {
		cerr << e.what() << endl;
	}
    return 0;
}

