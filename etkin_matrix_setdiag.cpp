//
//  main.cpp
//  interpret_motion_parameters
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include <string>
#include <set>
//FSL INCLUDE
#include "newimage/newimageall.h"
#include "utils/options.h"

//BOOST INCLUDES (MOSTLY BGL)
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

using namespace NEWIMAGE;
using namespace Utilities;

using namespace std;
using namespace boost;
#include "etkin_graph_typedefs.h"
#include "network_metrics.h"
using namespace etkin_graph;


string title="etkin_matrix_setdiag Stanford University (Brian Patenaude) \n \t This function is used to create masks for the connectviity matrices inorder to facility netowrk specific analysis";
string examples="etkin_matrix_setdiag [--zero] -i <connection_matrix.nii.gz> -l <val> -o <output_base>";




Option<bool> verbose(string("-v,--verbose"), false,
                     string("switch on diagnostic messages"),
                     false, no_argument);
Option<bool> help(string("-h,--help"), false,
                  string("display this message"),
                  false, no_argument);

Option<bool> zero(string("-z,--zero"), false,
                  string("Omit zero lines"),
                  false, no_argument);

//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
					  string("Filename of list of groups"),
					  true, requires_argument);
					Option<float> val(string("-l,--l"), 1,
										  string("Value to set diagonal"),
										  true, requires_argument);
Option<string> outname(string("-o,--output"), string(""),
					   string("Output name"),
					   true, requires_argument);
int nonoptarg;

template<class T>
string print( const vector<T> & vec )
{
	for (typename vector<T>::const_iterator i = vec.begin(); i != vec.end(); ++i)
		cout<<*i<<" ";
	cout<<endl;
}

string int2str( const int & val)
{
    stringstream ss;
    ss<<val;
    string sval;
    ss >> sval;
    return sval;
    
}

int do_work( const string & inname , const float & val , const string & outname , const bool & doZeros){
   
    
    volume<float> matrix;
    read_volume(matrix,inname);
    
    int xsize=matrix.xsize();
    
    
    for (int x = 0 ; x < xsize ; ++x)
    {
        if (doZeros)
        {
            //lets search if all is zero
//            if (matrix.value(x,x,0) != 0 )
            {
//                cout<<"search for zeros "<<x<<endl;
                bool allZero=true;
                for (int y = 0 ; y< xsize; ++y)
                {
//                    cout<<"test y "<<y<<" "<<matrix.value(x,y,0)<<endl;
                    if ((y != x ) && (matrix.value(x,y,0) != 0 ))
                    {
                        allZero=false;
                        break;
                    }
                }
//                cout<<"found nonzero "<<allZero<<endl;
                if (allZero)
                    matrix.value(x,x,0)=0;
                else
                    matrix.value(x,x,0)=val;

            }
            
        }else{
            matrix.value(x,x,0)=val;
        }
        
    }
    
    save_volume(matrix,outname);
  
    return 0;
}
int main (int argc,  char * argv[])
{
    //    if (argc < 2)
    //    {
    //        usage();
    //        return 0;
    //    }
    //    string inname(argv[1]);
    //
    
	Tracer tr("main");
	OptionParser options(title, examples);
	
	try {
		// must include all wanted options here (the order determines how
		//  the help message is printed)
		options.add(inname);
		options.add(val);
        options.add(zero);
		options.add(outname);
		options.add(verbose);
		options.add(help);
        
    	nonoptarg = options.parse_command_line(argc, argv);
        if (  (!options.check_compulsory_arguments(true) ))
		{
			options.usage();
			exit(EXIT_FAILURE);
		}
        
        do_work(inname.value(),val.value(),outname.value(),zero.value());

    } catch(X_OptionError& e) {
		options.usage();
		cerr << endl << e.what() << endl;
		exit(EXIT_FAILURE);
	} catch(std::exception &e) {
		cerr << e.what() << endl;
	}
    return 0;
}

