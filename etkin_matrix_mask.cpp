//
//  main.cpp
//  interpret_motion_parameters
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include <string>
#include <set>
//FSL INCLUDE
#include "newimage/newimageall.h"
#include "utils/options.h"

//BOOST INCLUDES (MOSTLY BGL)
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

using namespace NEWIMAGE;
using namespace Utilities;

using namespace std;
using namespace boost;
#include "etkin_graph_typedefs.h"
#include "network_metrics.h"
using namespace etkin_graph;


string title="etkin_matrix_mask Stanford University (Brian Patenaude) \n \t This function is used to create masks for the connectviity matrices inorder to facility netowrk specific analysis";
string examples="etkin_matrix_mask -i <connection_matrix.nii.gz> -o <output_base>";




Option<bool> verbose(string("-v,--verbose"), false,
                     string("switch on diagnostic messages"),
                     false, no_argument);
Option<bool> help(string("-h,--help"), false,
                  string("display this message"),
                  false, no_argument);

//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
					  string("Filename of list of groups"),
					  true, requires_argument);
Option<string> groupinc(string("-g,--g"), string(""),
					string("Groups to include (comma seperate integers)"),
					true, requires_argument);
Option<string> outname(string("-o,--output"), string(""),
					   string("Output name"),
					   true, requires_argument);
int nonoptarg;

template<class T>
string print( const vector<T> & vec )
{
	for (typename vector<T>::const_iterator i = vec.begin(); i != vec.end(); ++i)
		cout<<*i<<" ";
	cout<<endl;
}

string int2str( const int & val)
{
    stringstream ss;
    ss<<val;
    string sval;
    ss >> sval;
    return sval;
    
}

//stoi should be implemented in c++11, centos 7 has some compatibility issues
int stoi(const std::string& str)
{
  stringstream ss;
  ss<<str;
  int num;
  ss>>num;
  return num;
}

int do_work( const string & inname , const string & groupinc , const string & outname ){
    cout<<"inname "<<inname<<endl;
	ifstream fin(inname.c_str());
	if ( ! fin.is_open())
		return 1; 
	

	//read in groups to include
	set<int> s_groups_inc; 
	string group;
	stringstream ssgroup(groupinc);
	while(std::getline(ssgroup, group, ',')) 
	{
		s_groups_inc.insert(atoi(group.c_str()));
	}
	
	
	//mapping index to group
	list< pair<int,int> > l_indices_inc_2_group;
	
	string line; 
	int index =0 ; 
	while ( getline(fin,line))
	{
		stringstream ss;
		ss<<line;
		int group; 
		ss>>group;
		if (s_groups_inc.find(group) != s_groups_inc.end() )
			l_indices_inc_2_group.push_back(pair<int,int>(index,group));
		++index;
	}
	fin.close();	
	
	//let's do intranetwrok 

	volume<short> mask(index,index,1);//create mask of just within networks
	mask=0;
	for (list< pair<int,int> >::iterator i = l_indices_inc_2_group.begin(); i != l_indices_inc_2_group.end(); ++i )
		{
//			cout<<"include "<<*i<<endl;
			list< pair<int,int> >::iterator ii = i;
//			++ii;
			
			int group=i->second;
			for ( ; ii != l_indices_inc_2_group.end(); ++ii ) 
				if (group == ii->second )
				mask.value(i->first,ii->first ,0)= mask.value(ii->first,i->first,0)=group;
		}
		//zero main diagonal
//		for (int i =0 ; i < index; ++i)
//			mask.value(i,i,0)=0;
				
	save_volume(mask,outname);
	
	//let's do internetwork 
	volume<short> mask_inter(index,index,1);//create mask of just within networks
	mask=0;
	for (list< pair<int,int> >::iterator i = l_indices_inc_2_group.begin(); i != l_indices_inc_2_group.end(); ++i )
		{
//			cout<<"include "<<*i<<endl;
			list< pair<int,int> >::iterator ii = l_indices_inc_2_group.begin();		
			int group=i->second;
			for ( ; ii != l_indices_inc_2_group.end(); ++ii ) 
			{
				int group2 = ii->second;
				int val=group;
				if (group != group2  )
					{
						val=group*1000+ group2;
					}
				if (ii->first > i->first)
				mask_inter.value(i->first,ii->first ,0)= mask_inter.value(i->first,i->first,0)=val;
			}
		}
		//zero main diagonal
//		for (int i =0 ; i < index; ++i)
//			mask_inter.value(i,i,0)=0;
				
	save_volume(mask_inter,outname+"_inter");
	

    return 0;
}
int main (int argc,  char * argv[])
{
    //    if (argc < 2)
    //    {
    //        usage();
    //        return 0;
    //    }
    //    string inname(argv[1]);
    //
    
	Tracer tr("main");
	OptionParser options(title, examples);
	
	try {
		// must include all wanted options here (the order determines how
		//  the help message is printed)
		options.add(inname);
		options.add(groupinc);

		options.add(outname);
		options.add(verbose);
		options.add(help);
        
    	nonoptarg = options.parse_command_line(argc, argv);
        if (  (!options.check_compulsory_arguments(true) ))
		{
			options.usage();
			exit(EXIT_FAILURE);
		}
        
        do_work(inname.value(),groupinc.value(),outname.value());

    } catch(X_OptionError& e) {
		options.usage();
		cerr << endl << e.what() << endl;
		exit(EXIT_FAILURE);
	} catch(std::exception &e) {
		cerr << e.what() << endl;
	}
    return 0;
}

