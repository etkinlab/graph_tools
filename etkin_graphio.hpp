#include <fstream>
#include <sstream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/property_map/property_map.hpp>
#include <etkin_graph_typedefs.h>
#include "newimage/newimageall.h"
//using namespace NEWIMAGE;

//string COLOR_SCHEME[] = { "#800000" ,"#004000" , "#0040ff" , "#ff4000" , "#8040ff"  };
string COLOR_SCHEME[] = { "lightskyblue1" ,"orange" , "olivedrab4" , "firebrick3" , "lemonchiffon1"  };
string COLOR_INTER = "red";
string COLOR_INTRA = "blue";
//
//template<class T> 
//void writeGraphAsNIFTI( const string & filename , const T & g , const bool & directed )
//{
//	int Nfeatures =num_vertices(g);
//
//	volume<float> im_graphs(Nfeatures,Nfeatures,1);
//		im_graphs=0;
//	//		cout<<"----Graph "<<im_graph_index<<endl;
//	        typename property_map<T, edge_weight_t>::type weightmap = get(edge_weight, g);
//		    typename graph_traits<T>::edge_iterator ei, ei_end;
//		    for (boost::tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) 
//		    {	
//		    	int vsrc = source(*ei, g);
//		    	int vtarg = target(*ei, g);
//		    	
//	//	    	cout<<"vsrc/vtarg/wight" <<vsrc<<" "<<vtarg<<" "<<weightmap[*ei]<<endl;
//		    	im_graphs.value(vsrc, vtarg,0) = weightmap[*ei];
//	    		//add both sides if not directed
//	    		if (!directed)
//		    	{
//	    			im_graphs.value(vtarg, vsrc,0) = weightmap[*ei];
//
//		    	}
//		    		
//		    }
//
//	 
//	 
//}

template<class T> 
void writeGraphAsNIFTI( const string & filename , const vector< T > & g , const bool & directed )
{
	//return NULL case
	if (g.empty() ) return;
	int Nfeatures =num_vertices(g[0]);
	int Ngraphs = g.size();
	volume4D<float> im_graphs(Nfeatures,Nfeatures,1,Ngraphs);
	
	
	int im_graph_index = 0; 
	for (auto i_graph : g  )
	{		
		im_graphs[im_graph_index]=0;
//		cout<<"----Graph "<<im_graph_index<<endl;
        typename property_map<T, edge_weight_t>::type weightmap = get(edge_weight, i_graph);
	    typename graph_traits<T>::edge_iterator ei, ei_end;
	    for (boost::tie(ei, ei_end) = edges(i_graph); ei != ei_end; ++ei) 
	    {	
	    	int vsrc = source(*ei, i_graph);
	    	int vtarg = target(*ei, i_graph);
	    	
//	    	cout<<"vsrc/vtarg/wight" <<vsrc<<" "<<vtarg<<" "<<weightmap[*ei]<<endl;
    		im_graphs[im_graph_index].value(vsrc, vtarg,0) = weightmap[*ei];
    		//add both sides if not directed
    		if (!directed)
	    	{
        		im_graphs[im_graph_index].value(vtarg, vsrc,0) = weightmap[*ei];

	    	}
	    		
	    }

	    
	    ++im_graph_index;
	}
	save_volume4D(im_graphs,filename);
}
//template void writeGraphAsNIFTI<etkin_graph::UndirectedGraph>( const string & filename , const vector< T >& g , const bool & directed );
//template void writeGraphAsNIFTI<etkin_graph::UndirectedGraph>( const string & filename , const vector< T >& g , const bool & directed );

void writeBriviewGraph()
{
	
}


void writeGraphVizCirco( const string & filename , etkin_graph::UndirectedGraph & G, const vector<string> & v_labels, const vector<int> &  cluster_labels, bool digraph = 0, bool doSubgraph = 1  )
{
    ofstream fout(filename.c_str());
    if (fout.is_open())
    {
        if (digraph) fout << "digraph G {"<<endl;
        else fout << "graph G {"<<endl;
        
		fout<<"layout=neato"<<endl;
		fout<<"splines=true"<<endl;
		fout<<"overlap=scale"<<endl;

		fout<<"node [ fontsize=16,style=filled,penwidth=4]"<<endl;

//		fout<<"node [shape=circle,fontsize=14,penwidth=2]"<<endl;
		
	float radius = 1000;
	int Nverts = num_vertices(G);
			
        set<int> uniqueLabels;
        map<int,int> vert2cluster;
        map<int,int> cluster2index;
	
{
        int index=0;
        for (vector<int>::const_iterator i = cluster_labels.begin(); i != cluster_labels.end(); ++i, ++index)
        {
            vert2cluster[index]=*i;
            
            uniqueLabels.insert(*i);
        }
} 

{
	int index=0;
		for (set<int>::iterator i = uniqueLabels.begin(); i != uniqueLabels.end(); ++i, ++index)
		{
			cout<<"clust2index "<<*i<<" "<<index<<endl;
			cluster2index[*i]=index;
		}
}
       vector<bool> e_used(num_edges(G),0);
        vector<bool> v_used(num_vertices(G),0);


     
        
        {//parent graph
//            fout<<"Edge [color=red];"<<endl;
            property_map<etkin_graph::UndirectedGraph, vertex_name_t>::type vnamemap = get(vertex_name, G);
            //main graph edges
            graph_traits<etkin_graph::UndirectedGraph>::vertex_iterator vi, vi_end;
            int index = 0;
            vector<bool>::iterator iv_used = v_used.begin();
            for (boost::tie(vi, vi_end) = vertices(G); vi != vi_end; ++vi,++index,++iv_used)
            {
                if (*iv_used == 0 )
                {
                cout<<"ind "<<index<<" "<<vnamemap[index]<<endl;
				float PI=3.1456;
				  float x = cos(PI*2.0/Nverts *index)*radius;
				  float y = sin(PI*2.0/Nverts *index)*radius;
//				fout<<i<<" -> "<<i+1<<" [style=invis]"<<endl;
		 		//fout<<i<<"[pos=\"x,y\", shape = \"square\"]";
		string color=COLOR_SCHEME[cluster2index[vert2cluster[*vi]]];
//		cout<<"color "<<*vi<<" "<<vert2cluster[*vi]<<" "<<cluster2index[vert2cluster[*vi]]<<endl;
//                fout<<*vi<<"[label=\""<<vnamemap[*vi]<<"\",pos=\""<<x<<","<<y<<"!\", shape = \"circle\", color=\""<<color<<"\"];"<<endl;
                fout<<*vi<<"[label=\""<<vnamemap[*vi]<<"\",pos=\""<<x<<","<<y<<"!\", shape = \"circle\", color="<<color<<"];"<<endl;
            
                }
            }
            vector<bool>::iterator i_used = e_used.begin();
            property_map<etkin_graph::UndirectedGraph, edge_weight_t>::type weightmap = get(edge_weight, G);
            graph_traits<etkin_graph::UndirectedGraph>::edge_iterator ei, ei_end;
            for (boost::tie(ei, ei_end) = edges(G); ei != ei_end; ++ei,++i_used) {
                if ( ! *i_used )
{
	int gr0 = vert2cluster[source(*ei, G)];
	int gr1 = vert2cluster[target(*ei, G)];
string edgeColor;
if (gr0 == gr1 )
edgeColor=COLOR_INTRA;
else 
{
//edgeColor=COLOR_INTER + ":yellow";
edgeColor=COLOR_SCHEME[cluster2index[gr0]] + ":" + COLOR_SCHEME[cluster2index[gr1]] ;
}
                if (digraph) fout << source(*ei, G) << " -> " << target(*ei, G)<<"[ dir=both color=\""<<edgeColor<<"\" penwidth="<<2+10*powf(weightmap[*ei],1)<<"];"<<endl;
                else fout << source(*ei, G) << " -- " << target(*ei, G)<<"[ dir=both color=\""<<edgeColor<<"\" "<<2+10*powf(weightmap[*ei],1)<<"];"<<endl;
                            //		<< " with weight of " << weight[*ei]
                 //		<< std::endl;
} 
           }
        }
        

        fout<<"}"<<endl;
        fout.close();
    }
    
    return;
}

void writeGraphVizWithCluster( const string & filename , etkin_graph::UndirectedGraph & G, const vector<string> & v_labels, const vector<int> &  cluster_labels, bool digraph = 0, bool doSubgraph = 1  )
{
    ofstream fout(filename.c_str());
    if (fout.is_open())
    {
        if (digraph) fout << "digraph G {"<<endl;
        else fout << "graph G {"<<endl;
        
        set<int> uniqueLabels;
        map<int,int> vert2cluster;
        int index=0;
        for (vector<int>::const_iterator i = cluster_labels.begin(); i != cluster_labels.end(); ++i, ++index)
        {
            vert2cluster[index]=*i;
            
            uniqueLabels.insert(*i);
        }
        vector<bool> e_used(num_edges(G),0);
        vector<bool> v_used(num_vertices(G),0);

 //DO CLUSTERS
        for ( set<int>::iterator i_c = uniqueLabels.begin(); i_c != uniqueLabels.end(); ++i_c)
        {
			if (doSubgraph)
			{
            string s_c;
            stringstream ss;
            ss<<*i_c;
            ss>>s_c;
            string cname = "cluster_"+s_c;
            fout<<"subgraph "<<cname<<"{"<<endl;
            fout<<"Edge [color=blue];"<<endl;
			}
            {//write vertices
                property_map<etkin_graph::UndirectedGraph, vertex_name_t>::type vnamemap = get(vertex_name, G);
                //main graph edges
                graph_traits<etkin_graph::UndirectedGraph>::vertex_iterator vi, vi_end;
                int index = 0;
                vector<bool>::iterator i_used = v_used.begin();
                vector<int>::const_iterator i_lb = cluster_labels.begin();
                for (boost::tie(vi, vi_end) = vertices(G); vi != vi_end; ++vi,++index,++i_used,++i_lb)
                {
                    if ( *i_c == *i_lb){
//                    cout<<"ind "<<index<<" "<<vnamemap[index]<<endl;
                    fout<<*vi<<"[label=\""<<vnamemap[*vi]<<"\"];"<<endl;
                        *i_used=1;
                    }
                }
                
            }
            {//write edges
                graph_traits<etkin_graph::UndirectedGraph>::edge_iterator ei, ei_end;
                vector<bool>::iterator i_used = e_used.begin();
                for (boost::tie(ei, ei_end) = edges(G); ei != ei_end; ++ei,++i_used) {
                    if ( ( (*i_c) == vert2cluster[source(*ei, G)] ) && ( *i_c == vert2cluster[target(*ei, G)] ) )
                    {
                        
                        if (digraph) fout << source(*ei, G) << " -> " << target(*ei, G)<<";"<<endl;
                        else fout << source(*ei, G) << " -- " << target(*ei, G)<<";"<<endl;
                        *i_used = 1;
                        
                    }
                    //		<< " with weight of " << weight[*ei]
                    //		<< std::endl;
                }
            }
            if (doSubgraph)
            fout<<"}"<<endl;
        }
        
        
        {//parent graph
            fout<<"Edge [color=red];"<<endl;
            property_map<etkin_graph::UndirectedGraph, vertex_name_t>::type vnamemap = get(vertex_name, G);
            //main graph edges
            graph_traits<etkin_graph::UndirectedGraph>::vertex_iterator vi, vi_end;
            int index = 0;
            vector<bool>::iterator iv_used = v_used.begin();
            for (boost::tie(vi, vi_end) = vertices(G); vi != vi_end; ++vi,++index,++iv_used)
            {
                if (*iv_used == 0 )
                {
//                cout<<"ind "<<index<<" "<<vnamemap[index]<<endl;
                fout<<*vi<<"[label=\""<<vnamemap[*vi]<<"\"];"<<endl;
            
                }
            }
            vector<bool>::iterator i_used = e_used.begin();
            graph_traits<etkin_graph::UndirectedGraph>::edge_iterator ei, ei_end;
            for (boost::tie(ei, ei_end) = edges(G); ei != ei_end; ++ei,++i_used) {
            	if ( ! *i_used )
            	{
            		if (digraph){
            			fout << source(*ei, G) << " -> " << target(*ei, G)<<";"<<endl;
            		}else{
            			fout << source(*ei, G) << " -- " << target(*ei, G)<<";"<<endl;
            		}
            	}
            	//		<< " with weight of " << weight[*ei]
            	//		<< std::endl;
            }
        }
        

        fout<<"}"<<endl;
        fout.close();
    }
    
    return;
}
