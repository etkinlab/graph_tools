//
//  main.cpp
//  interpret_motion_parameters
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include <string>
#include <set>
//FSL INCLUDE
#include "newimage/newimageall.h"
#include "utils/options.h"

//BOOST INCLUDES (MOSTLY BGL)
//#include <boost/graph/properties.hpp>
//#include <boost/graph/graphviz.hpp>

//#include <boost/config.hpp>
//#include  <boost/filesystem.hpp>
#include "etkin_graph_typedefs.h"

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
//#include <boost/property_map/property_map.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
//#include <boost/graph/betweenness_centrality.hpp>

//#include <boost/graph/graphviz.hpp>

using namespace NEWIMAGE;
using namespace Utilities;

using namespace std;
using namespace boost;
#include "etkin_graphio.hpp"
#include "network_metrics.h"
using namespace etkin_graph;


string title="etkin_graph_cohend Stanford University (Brian Patenaude)";
string examples="etkin_graph_cohend -i <connection_matrix_graph.nii.gz> -o <output_base>";




Option<bool> verbose(string("-v,--verbose"), false,
                     string("switch on diagnostic messages"),
                     false, no_argument);
Option<bool> help(string("-h,--help"), false,
                  string("display this message"),
                  false, no_argument);
//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
                      string("Filename of input image correlation matrix"),
                      true, requires_argument);
Option<string> groupsname(string("-g,--groups"), string(""),				  string("Filename of subject's group"),				  false, requires_argument);
Option<string> outname(string("-o,--output"), string(""),
                       string("Output name"),
                       true, requires_argument);
int nonoptarg;

enum g_thresh_method  { ABS_THRESH, MST_MIN_W, MST_W_ABS_THRESH,MST, MST_W_PERC,  PERC , MST_U_PERC, COMPLETE };


void usage(){
    
    cout<<"\n Usage : "<<endl;
    cout<<"etkin_graph_cohen <correlations.nii.gz> \n"<<endl;
    
}


string int2str( const int & val)
{
    stringstream ss;
    ss<<val;
    string sval;
    ss >> sval;
    return sval;
    
}
int string2int( const string & val)
{
    stringstream ss;
    ss<<val;
    int ival;
    ss >> ival;
    return ival;
    
}


int do_work( const string & inname , const string & sub_groupname, const string & outname  ){
    cout<<"Reading input connectivity matrices... "<<inname<<endl;
    //    g_thresh_method g_meth = MST_MIN_W;
  
    //--------READ IN Data including masks, labels, subject names etc.....-------
    volume4D<float> connectivity_matrix;
    read_volume4D(connectivity_matrix,inname);
    int n = connectivity_matrix.xsize();
    int Nsubjects =connectivity_matrix.tsize();

	
	
	vector<int> v_group(Nsubjects,0);
	{
		if (! sub_groupname.empty())
		{
			ifstream f_gr(sub_groupname.c_str());
			if (f_gr.is_open())
			{
				int gr;
				string sgr;
				vector<int>::iterator i_s = v_group.begin();
				while (getline(f_gr,sgr)) {
					*i_s = string2int(sgr);
					++i_s;
				}
				f_gr.close();
			}
		}
    }
    
    //t-test between groups
    map<int,int> groups2count;
    for (vector<int>::iterator i_group = v_group.begin(); i_group != v_group.end(); ++i_group)
        groups2count[*i_group]++;

    map<int,int> group2index;
    int index=0;
    for (map<int,int>::iterator i = groups2count.begin(); i != groups2count.end(); ++i,++index)
    {
        cout<<"group2index "<<i->first<<" "<<i->second<<endl;
        group2index[i->first]=index;
    }
    


    volume<float> im_out(n,n,1);
    im_out=0;
    
    for (int x = 0 ; x < n ; ++x )
    {
        for ( int y = 0 ; y < n ; ++y)
        {
//            float sx(0),sxx(0),sy(0),syy(0);
            float sx[] = {0,0};
            float sxx[] = {0,0};
            int n[] = {0,0};
            vector<int>::iterator i_gr = v_group.begin();
            for (int t = 0 ; t < Nsubjects; ++t , ++i_gr)
            {
//                cout<<"gr "<<(*i_gr)<<endl;
                float val = connectivity_matrix[t].value(x,y,0);
                sx[group2index[*i_gr]] += val;
                sxx[group2index[*i_gr]] += val*val;
                n[group2index[*i_gr]] ++;
               
            }
            float cohen = sx[0]/n[0] - sx[1]/n[1];
            float s0_sqr = (sxx[0] - sx[0]*sx[0]/n[0]);//note did not normalize these
            float s1_sqr = (sxx[1] - sx[1]*sx[1]/n[1]);
            float s = sqrt( (s0_sqr+s1_sqr)/(n[0]+n[1]-2));
            cohen/=s;
//            cout<<"n "<<n[0]<<" "<<n[1]<<endl;
            im_out.value(x,y,0) = cohen;
            
            

        }
    }
    save_volume(im_out, outname);
    
    
    return 0;
}
int main (int argc,  char * argv[])
{
    //    if (argc < 2)
    //    {
    //        usage();
    //        return 0;
    //    }
    //    string inname(argv[1]);
    //
    
    /* initialize random seed: */
    srand (time(NULL));

    
    Tracer tr("main");
    OptionParser options(title, examples);
    
    try {
        // must include all wanted options here (the order determines how
        //  the help message is printed)
        options.add(inname);
               options.add(outname);
        options.add(groupsname);
        options.add(verbose);
        options.add(help);
        nonoptarg = options.parse_command_line(argc, argv);
        if (  (!options.check_compulsory_arguments(true) ))
        {
            options.usage();
            exit(EXIT_FAILURE);
        }

        
        cout<<"groupsname "<<groupsname.value()<<endl;
        do_work(inname.value(),groupsname.value(),outname.value());

        
    } catch(X_OptionError& e) {
        options.usage();
        cerr << endl << e.what() << endl;
        exit(EXIT_FAILURE);
    } catch(std::exception &e) {
        cerr << e.what() << endl;
    }
    return 0;
}

