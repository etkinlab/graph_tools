//
//  main.cpp
//  interpret_motion_parameters
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include <string>
#include <set>
//FSL INCLUDE
#include "newimage/newimageall.h"
#include "utils/options.h"
#include "boost/filesystem.hpp"

using namespace NEWIMAGE;
using namespace Utilities;

using namespace std;

namespace fs = boost::filesystem;



string title="etkin_matrix_roi_dist Stanford University (Brian Patenaude) \n \t This function is used calculate the pairwise distance between ROIS. Output format will be dteermined by the extension.";
string examples="etkin_matrix_roi_dist -i <connection_matrix.nii.gz> -c <cog.txt> -o <output_base>";




Option<bool> verbose(string("-v,--verbose"), false,
                     string("switch on diagnostic messages"),
                     false, no_argument);
Option<bool> help(string("-h,--help"), false,
                  string("display this message"),
                  false, no_argument);

//Standard Inputs
Option<string> inname(string("-i,--in"), string(""),
					  string("Image name of atlas (4D)"),
					  true, requires_argument);
Option<string> cinname(string("-c,--cin"), string(""),
                      string("Filename of text file. 3 Colums: x y z "),
                      true, requires_argument);
Option<string> outname(string("-o,--output"), string(""),
					   string("Output name"),
					   true, requires_argument);
int nonoptarg;

struct float3{
    float3(float xin,float yin,float zin)
    {
        x=xin;
        y=yin;
        z=zin;
    }
    float dist( float3 vin){
        return sqrtf((vin.x -x)*(vin.x -x) + (vin.y -y)*(vin.y -y)  + (vin.z -z)*(vin.z -z));
        
    }
    
    
    float x,y,z;
};

int do_work( const string & inname ,const string & cinname , const string & outname){
   
    if ( ! fs::exists(inname))
    {
        cerr<<"File does not exists : "<<inname<<endl;
        exit (EXIT_FAILURE);
    }
    
    vector<float3> v_cog;
    
    ifstream fcog(cinname.c_str());
    if (fcog.is_open())
    {
        string line;
        while (getline(fcog,line)) {
            stringstream ss;
            ss<<line;
            float x,y,z;
            ss>>x>>y>>z;
            v_cog.push_back(float3(x,y,z));
        }
        
        
        fcog.close();
    }else{
        cerr<<"Failed to open file for reading : "<<inname<<endl;
        exit (EXIT_FAILURE);
    }
    
    unsigned int N = v_cog.size();
    
    volume4D<float> image;
    int Nt = image.tsize();
    
    vector< vector<int> > cluster_mem(N);
    {
    vector< vector<int> >::iterator i_cm = cluster_mem.begin();
    for (vector<float3>::iterator i_c = v_cog.begin(); i_c != v_cog.end(); ++i_c,++i_cm)
    {
        for (int t = 0; t< Nt ; ++t)
            if ( image[t].value(i_c->x,i_c->y,i_c->z) > 0 )
                i_cm->push_back(t);
        
    }
    }
    
    ofstream fout(outname.c_str());
    if (! fout.is_open())
    {
        cerr<<"Failed to open file for reading"<<endl;
        exit (EXIT_FAILURE);
        
    }
    {//write output
        for (vector< vector<int> >::iterator i_cm = cluster_mem.begin(); i_cm != cluster_mem.end(); ++i_cm )
        {
            
            for (vector<int>::iterator ii_cm = i_cm->begin(); ii_cm != i_cm->end(); ++ii_cm)
            {
                if (ii_cm != i_cm->begin())
                    fout<<" ";
                fout<<(*i_cm);
            }
            fout<<endl;
        }
    }

    
    return 0;
}
int main (int argc,  char * argv[])
{
    //    if (argc < 2)
    //    {
    //        usage();
    //        return 0;
    //    }
    //    string inname(argv[1]);
    //
    
	Tracer tr("main");
	OptionParser options(title, examples);
	
	try {
		// must include all wanted options here (the order determines how
		//  the help message is printed)
		options.add(inname);
        options.add(cinname);
		options.add(outname);
		options.add(verbose);
		options.add(help);
        
    	nonoptarg = options.parse_command_line(argc, argv);
        if (  (!options.check_compulsory_arguments(true) ))
		{
			options.usage();
			exit(EXIT_FAILURE);
		}
        
        do_work(inname.value(),cinname.value(),outname.value());

    } catch(X_OptionError& e) {
		options.usage();
		cerr << endl << e.what() << endl;
		exit(EXIT_FAILURE);
	} catch(std::exception &e) {
		cerr << e.what() << endl;
	}
    return 0;
}

