function y = etkin_graph_metrics(con_matrices, subject_list_name,output)
%This script is written by Brian Patenaude. It has been
%adapapted from Petra Vertes of Ed Bullmore's lab
%----------Input------------
%
% con_matrices is a 4D NIFTI, where each timepoint is a subject's correlation (ie.
% connectivity) matrix
% maskname provides a selection mask for features, should be symmetric
% across the diagonal
%
%---------------------------
%

disp(sprintf('\n%s\n','Hello!'));
disp(sprintf('Lets run some brain connectivity analysis!\n'))
disp(sprintf('Reading connectivity matrices from : %s \n',con_matrices))

%Reading in data.
%going to use FSL data readers to read in NIFTI images
addpath /usr/local/fsl/etc/matlab/
addpath  /Volumes/Smurf-Village/SoftwareRepository/graph_theory/BCT/2014_04_05' BCT'/

subject_list=textread(subject_list_name, '%s');
size(subject_list)

[img, dims,scales,bpp,endian] = read_avw(con_matrices);

disp(sprintf('I have detected %i subjects and %i features. \n',dims(4),dims(1)))



%let's remove the NaN regions. These correspond to ROIs that fell outside
%the field of view of the scan. This resulted in a divide by zero in the
%calculation of r
%nan_mask=sum(img,4); %add NaN to anything results in NaN, so by adding across subjects, will give a 2 dimensional matrix with NaN whereever any subject had an NaN
%nan_mask=isnan(nan_mask); % transform a matrix with NaNs into a binary matrix indicating where NaN's exist

img=squeeze(img);
N=dims(1)
maxCons=0.5*(N^2-N);
%%%%%%%%DONT FORGET TO USE ABOSOLUTE VALUE WHEN NECESSARY
btw_wei=[];
edge_btw=[];
all_data=[];
for subject = 1:dims(4)
    %    for subject = 1:1
    
    disp(sprintf('Processing subject %i... \n',subject))
    close all
    
    imsubject=img(:,:,subject);
    imsubject(eye(size(imsubject))==1) = 0 ;
imsubject=abs(imsubject);

    %%%%%%%%----------------Weighted-------------------%%%%%%%

    [Ci_louv_w Mod_louv_w ]=modularity_louvain_und(imsubject);


    clust_w=clustering_coef_wu(imsubject);
    [clust_w_means,clust_w_format,clust_w_names] = multipleMeans(clust_w, Ci_louv_w, Ci_louv_w,'clust_w');


    % centrality
  %  btw_w=betweenness_wei(imsubject);
   % [btw_w_means,btw_w_format,btw_w_names] = multipleMeans(btw_w, Ci_louv_w, Ci_louv_w,'btw_w');

Ci_louv_w
    pcoef_w=participation_coef(imsubject,Ci_louv_w )
    
    
    [all_pcoef_w,all_pcoef_w_format, all_pcoef_w_names] =  multipleMeans(pcoef_w, Ci_louv_w, Ci_louv_w,'pcoef_w');
    
data_one=[ Mod_louv_w max(Ci_louv_w)];
data_one=[ data_one clust_w_means(1) ];
data_one=[ data_one all_pcoef_w(1) ];

    if (subject == 1)
all_data=data_one;
        %all_data=[ cost deg assort Mod_bin Mod_louv_bin  mean_PathLength_bin Eff_bin CostEff_bin  Mod_w Mod_louv_w mean_PathLength_w Eff_w  all_pcoef_w ];
        
    else


all_data=[all_data ; data_one ];
    end
end
%write data
%save(strcat(output,'.ssv'),'all_data','-ascii');

%write header
fid=fopen(strcat(output,'.csv'),'w');
%fwrite(fid,'cost mean_degrees assort modularity_und modularity_louvain_und mean_cluster_coef mean_PathLength Efficieny Cost-Efficiency\n')
%fprintf(fid,'%s \n','subject,cost,mean_degrees,assort,modularity_und,modularity_louvain_und,mean_cluster_coef,mean_PathLength,Efficieny,Cost-Efficiency');
%fprintf(fid,'%s \n',strcat('subject,cost,deg,assort,Mod_bin,Mod_louv_bin,clust_bin,mean_PathLength_bin,Eff_bin,CostEff_bin,Mod_w,Mod_louv_w,clust_w,mean_PathLength_w,Eff_w',all_pcoef_w_names));

header='subject,Mod_louv_w,Ngr'
header=strcat(header,',Mean_ClustCoef');
header=strcat(header,',Mean_P_Coef');

format='%6.6f,%6.6f';
format=strcat(format,',%6.6f')
format=strcat(format,',%6.6f')


%format=strcat(format,clust_w_format);
%format=strcat(format,all_pcoef_w_format);


fprintf(fid,'%s\n',header);


[R,C]=size(all_data);
for row = 1:R
    disp(sprintf('Write row %i... \n',row))
    
    fprintf(fid,'%s,',subject_list{row,1})
    %fprintf(fid,strcat(strcat('%6.6f,%6.6f,%6.6f,%6.6f,%6.6f,%6.6f,%6.6f,%6.6f,%6.6f,%6.6f,%6.6f,%6.6f,%6.6f,%6.6f',all_pcoef_w_format),'\n'),all_data(row,:));
    fprintf(fid,strcat(format,'\n'),all_data(row,:));
    
end
fclose(fid);

function [means,format,names] = multipleMeans(measure, Ci_predef, Ci_gr,basename)
%Ci_predef
Ngr=max(Ci_predef);
means=mean(measure);
names=strcat(',',strcat(basename,'_Mean'));
format=',%6.6f';
for i = 1:Ngr
means=[means mean(measure(Ci_predef==i)) ];
    real_group=Ci_gr(Ci_predef==i);
    real_group=real_group(1);
    names = strcat(strcat(strcat(names,','), strcat(basename,'_Mean_Gr_')),num2str(real_group));
    format=strcat(format,',%6.6f');
    
end

