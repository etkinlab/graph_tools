//
//  network_metric.hpp
//  this code implements various network metrics for BGL Grpahs, in particular it implments thiouse found in BCT tool box
//
//  Created by Brian Patenaude on 10/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//using namespace std;
//using namespace boost;
#ifndef NETWORK_METRICS_H
#define NETWORK_METRICS_H

#include "etkin_graph_typedefs.h"
#include <vector>
//#include <pair>
namespace etkin_graph {

    //utilities
    float corr_coef( const std::vector< std::pair<float,float> > & data );
    void multiplyWeights(UndirectedGraph & g,  const float & factor);

    template< class T >
    float getCostBin(const T & g);
    template< class T >
    void getCostBin(const T & g, network_metrics & nmets );


	//graph metrics functions
    template< class T >
	int getNumberOfNodes(const T & g );
    template< class T >
    void getNumberOfNodes(const T & g, network_metrics & nmets  );
//    int getNumberOfNodes(const diGraph & g );

//    void getNumberOfNodes(const diGraph & g, network_metrics & nmets  );
//
    template< class T >
	int getNumberOfEdges(const T & g );
    
    template< class T >
    void getNumberOfEdges(const T & g , network_metrics & nmets );
//    int getNumberOfEdges(const diGraph & g );
//    void getNumberOfEdges(const diGraph & g , network_metrics & nmets );

    
    template< class T >
    std::vector<float> degree_und(const T & g);
    
    template< class T >
    void degree_und(const T & g, network_metrics & nmets);
//    std::vector<float> degree_und(const diGraph & g);
    template< class T >
    std::vector<float> degree_in(const T & g);

    template< class T >
    std::vector<float> degree_out(const T & g);


    template< class T >
    void degree_in(const T & g, network_metrics & nmets);
    template< class T >
    void degree_out(const T & g, network_metrics & nmets);

    
    template<class T>
    void degree_intra_inter(const T & g, const  std::vector<int> & clusters, vector<float> & degree_intra,vector<float> & degree_inter);
    
//    template<class T>
    void avgconn_intra_inter(const UndirectedGraph & g, const  std::vector<int> & clusters, vector<float> & avgconn_intra,vector<float> & avgconn_inter, std::vector<unsigned int>  & n_intra, std::vector<unsigned int> & n_inter);

    template<class T>
    void degree_intra_inter(const T & g, const  std::vector<int> & clusters,network_metrics & nmets);
    //return overall average connectivity
//    template<class T>
    float avgconn_intra_inter(const UndirectedGraph & g, const  std::vector<int> & clusters,network_metrics & nmets);

    float powerLawGamma(const UndirectedGraph & g,const unsigned int & Nbins);
    void powerLawGamma(const UndirectedGraph & g,network_metrics & nmets, const  unsigned int &  Nbins);
    
    template<class T>
    float efficiency_wd( const T & g  , const vector<int> & excl_inds );
    template<class T>
    void  efficiency_wd( const T & g , const vector<int> & excl_inds , network_metrics & nmets );

    template<class T>
    float characteristicPathLength_wd( const T & g, const vector<int> & excl_inds );

    template<class T>
    void characteristicPathLength_wd( const T & g , const vector<int> & excl_inds, network_metrics & nmets );

    
    float systemSegregation( const UndirectedGraph & g, const  std::vector<int> & clusters );
    void systemSegregation( const UndirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets );

    
	float modularity(const UndirectedGraph & g, const  std::vector<int> & clusters );
    float modularity(const UndirectedGraph & g, const  std::vector<int> & clusters , std::vector<float> & eii, std::vector<float> & ai);
    void modularity(const UndirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets  );
    
    std::vector<float> within_module_degree_z( const UndirectedGraph & g, const  std::vector<int> & clusters );
    void within_module_degree_z( const UndirectedGraph & g, const  std::vector<int> & clusters, network_metrics & nmets);

    std::vector<float> closeness_centrality( const UndirectedGraph & g, const vector<int> & excl_inds );//for connected graphs
    void closeness_centrality( const UndirectedGraph & g, const vector<int> & excl_inds , network_metrics & nmets);//for connected graphs

    
    std::vector<float> participation_coefficient( const UndirectedGraph & g, const  std::vector<int> & clusters ,map<int, vector<float> > & p_coef_per_clust);
    void participation_coefficient( const UndirectedGraph & g, const  std::vector<int> & clusters , network_metrics & nmets );

    
    //    std::vector<float> rel_betweenness_centrality(const Graph & g );
//    void  printGraph( const Graph & g );
    void  centrality_brandes( const UndirectedGraph & g ,  std::vector<double> & centrality,  bool rel);

	//float assortativity_und_bin( const Graph & g , const std::vector< Edge >  & edges );
   float assortativity( const UndirectedGraph & g);
    void assortativity( const UndirectedGraph & g, network_metrics & nmets );

    
    //    std::vector<float>  clustering_coef_bu( const Graph & g );
}

#endif
