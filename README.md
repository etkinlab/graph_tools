# README #

This set of tools is used to estimate graph properties from correlation matrices. It also contains some tools for processing correlation matrices. It also provide a tool to create circle plot configuration files to be used with circus (www.circos.ca). A tool is also provide to generate surfaces of the graphs (spherical nodes with cylindrical links).

### Input Data 


*Currently only supports NIFTI format correlation matrices. 

*Tool supports 4D NIFTIs with the forth dimesion being "dim4" of the nifti files. 

*Support for directed vs non-directed graph. If a directed graph is processed as a unidirectional graph. It will ignore all edge weights in the lower triangular portion of the network matrix (i.e. include edge for feature x and y, where y>x and indexes the matrix at (x,y) ).

###Notes
* For percenitile, the actual number of edges of node may differ slightly if there is equal edge weights.
### How do I get set up? ###

* Configuration

* Dependencies
Some of the tools require FSL. It requires the image class, and fslsurface class. The fslsurface class requires an update version found in my bitbucket.

### Contents ###

* etkin_graph : Creates graphs from 4D NIFTI file of Correlations and outputs network metrics. Dimension of input are Nfeatures X Nfeatures X 1 X Nsubjects